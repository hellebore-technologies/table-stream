import {
  StringFilter,
  NumberFilter,
  SelectFilter,
  DateTimeFilter,
} from '@helleboretech/table-stream/src/types/valueFilter';
import { ComparisonType, ColumnType } from '@helleboretech/table-stream/src/types/enum';

import { parseFilterValue } from '@helleboretech/table-stream/src/filter';

import getDate from './utils';

describe.each([
  undefined,
  null,
  '',
])('parseFilterValue with empty filter', (input: string) => {
  const possibleColumnTypes = [
    ColumnType.DateTime,
    ColumnType.Select,
    ColumnType.Number,
    ColumnType.String,
  ];
  test.each(possibleColumnTypes)(`paseFilterValue for "${input}"`, (columType: ColumnType) => {
    const filter = parseFilterValue(input, columType);
    expect(filter.isActive).toBeFalsy();
  });
});

describe.each([
  ColumnType.DateTime,
  ColumnType.Number,
  ColumnType.String,
])('paseFilterValue set null search', (columnType: ColumnType) => {
  test(`test column type ${columnType}`, () => {
    const filter = parseFilterValue('//', columnType);
    expect((filter as unknown as NumberFilter).looksForNullValues).toBeTruthy();
  });
});

describe.each([
  ColumnType.Select,
])('paseFilterValue set null search', (columnType: ColumnType) => {
  test(`test column type ${columnType}`, () => {
    const filter = parseFilterValue('//', columnType);
    expect((filter as unknown as NumberFilter).looksForNullValues).toBeFalsy();
  });
});

describe.each([
  ['foobar', { value: 'foobar', begin: false, end: false }],
  ['Foobar', { value: 'foobar', begin: false, end: false }],
  ['foo ', { value: 'foo ', begin: false, end: false }],
  [' FOO', { value: ' foo', begin: false, end: false }],
  ['some Foo thing', { value: 'some foo thing', begin: false, end: false }],
  ['^foobar', { value: 'foobar', begin: true, end: false }],
  ['^ foobar', { value: ' foobar', begin: true, end: false }],
  ['^foobar$', { value: 'foobar', begin: true, end: true }],
  ['^ foobar $', { value: ' foobar ', begin: true, end: true }],
  [' ^ foobar $', { value: ' ^ foobar ', begin: false, end: true }],
  ['foobar $', { value: 'foobar ', begin: false, end: true }],
  ['foobar$$', { value: 'foobar$', begin: false, end: true }],
  ['foobar/$', { value: 'foobar$', begin: false, end: false }],
  ['foobar$$$', { value: 'foobar$$', begin: false, end: true }],
  ['foobar$$$$', { value: 'foobar$$$', begin: false, end: true }],
  ['^^foobar', { value: '^foobar', begin: true, end: false }],
  ['/^foobar', { value: '^foobar', begin: false, end: false }],
  ['^^^foobar', { value: '^^foobar', begin: true, end: false }],
  ['^^^^foobar', { value: '^^^foobar', begin: true, end: false }],
  ['^', { value: '', begin: true, end: false }],
  ['/^', { value: '^', begin: false, end: false }],
  ['^^', { value: '^', begin: true, end: false }],
  ['^^^', { value: '^^', begin: true, end: false }],
  ['^^^^', { value: '^^^', begin: true, end: false }],
  ['$', { value: '', begin: false, end: true }],
  ['/$', { value: '$', begin: false, end: false }],
  ['$$', { value: '$', begin: false, end: true }],
  ['$$$', { value: '$$', begin: false, end: true }],
  ['$$$$', { value: '$$$', begin: false, end: true }],
  ['^$', { value: '', begin: true, end: true }],
  ['/^/$', { value: '^$', begin: false, end: false }],
  ['//^//$', { value: '//^/$', begin: false, end: false }],
  [' ', { value: ' ', begin: false, end: false }],
])('parseFilterValue for StringFilter', (
  input: string,
  output: { value: string; begin: boolean; end: boolean; },
) => {
  test(`parsing is correct for ${input}`, () => {
    const filter = parseFilterValue(input, ColumnType.String);
    expect(filter).toEqual<StringFilter>({
      kind: ColumnType.String,
      isActive: true,
      looksForNullValues: false,
      value: output.value,
      begin: output.begin,
      end: output.end,
    });
  });
});

describe.each([
  ['12', 12, ComparisonType.Equal, undefined, undefined, true],
  ['12K', 12_000, ComparisonType.Equal, undefined, undefined, true],
  ['12k', 12_000, ComparisonType.Equal, undefined, undefined, true],
  ['12m', 12_000_000, ComparisonType.Equal, undefined, undefined, true],
  ['12M', 12_000_000, ComparisonType.Equal, undefined, undefined, true],
  ['=12', 12, ComparisonType.Equal, undefined, undefined, true],
  ['>12', 12, ComparisonType.Greater, undefined, undefined, true],
  ['>12k', 12_000, ComparisonType.Greater, undefined, undefined, true],
  ['>12K', 12_000, ComparisonType.Greater, undefined, undefined, true],
  ['<12', 12, ComparisonType.Lesser, undefined, undefined, true],
  ['>=12', 12, ComparisonType.GreaterEqual, undefined, undefined, true],
  ['<=12', 12, ComparisonType.LesserEqual, undefined, undefined, true],
  ['>-3.12', -3.12, ComparisonType.Greater, undefined, undefined, true],
  ['>=-3.12', -3.12, ComparisonType.GreaterEqual, undefined, undefined, true],
  ['<=-3.12', -3.12, ComparisonType.LesserEqual, undefined, undefined, true],
  ['12&13', 12, ComparisonType.Equal, 13, ComparisonType.Equal, true],
  ['>12&<13', 12, ComparisonType.Greater, 13, ComparisonType.Lesser, true],
  ['>=-3.12&<20', -3.12, ComparisonType.GreaterEqual, 20, ComparisonType.Lesser, true],
  ['>= -3.12 & < 20', -3.12, ComparisonType.GreaterEqual, 20, ComparisonType.Lesser, true],
  ['>= -.12 & < 20', -0.12, ComparisonType.GreaterEqual, 20, ComparisonType.Lesser, true],
  ['&', undefined, undefined, undefined, undefined, false],
  ['<-&>', undefined, undefined, undefined, undefined, false],
  ['>=', undefined, undefined, undefined, undefined, false],
  ['>', undefined, undefined, undefined, undefined, false],
  ['12 k', undefined, undefined, undefined, undefined, false],
  ['12 K', undefined, undefined, undefined, undefined, false],
  ['12 M', undefined, undefined, undefined, undefined, false],
  ['12 m', undefined, undefined, undefined, undefined, false],
  ['>&<12', undefined, undefined, 12, ComparisonType.Lesser, true],
])('parseFilterValue for NumericFilter', (
  input: string,
  value1: number,
  comparison1: ComparisonType,
  value2: number,
  comparison2: ComparisonType,
  isActive: boolean,
) => {
  test(`parsing is correct for ${input}`, () => {
    const filter = parseFilterValue(input, ColumnType.Number);
    expect(filter).toEqual<NumberFilter>({
      kind: ColumnType.Number,
      isActive,
      looksForNullValues: false,
      value1,
      comparison1,
      value2,
      comparison2,
    });
  });
});

describe.each([
  ['1', [1], true],
  ['1, 2', [1, 2], true],
  [',', [], false],
  [' 1 , 23, ', [1, 23], true],
])('parseFilterValue for SelectFilter', (
  input: string,
  values: number[],
  isActive: boolean,
) => {
  test(`parsing is correct for ${input}`, () => {
    const filter = parseFilterValue(input, ColumnType.Select);
    expect(filter).toEqual<SelectFilter>({
      kind: ColumnType.Select,
      isActive,
      values,
    });
  });
});

describe.each([
  ['2020-05-12T20:13:12', '2020-05-12T20:13:12', ComparisonType.Equal, undefined, undefined, true],
  ['= 2020/05/12', '2020-05-12T00:00:00', ComparisonType.Equal, undefined, undefined, true],
  ['> 2020/04/11 12:2', '2020-04-11T12:02:00', ComparisonType.Greater, undefined, undefined, true],
  ['< 02/1', `${getDate(2, 1)}T00:00:00`, ComparisonType.Lesser, undefined, undefined, true],
  ['>= 12:20', `${getDate()}T12:20:00`, ComparisonType.GreaterEqual, undefined, undefined, true],
  ['<=12:20:53', `${getDate()}T12:20:53`, ComparisonType.LesserEqual, undefined, undefined, true],
  ['<=12:20:53:', undefined, undefined, undefined, undefined, false],
  ['<=24:20:53', undefined, undefined, undefined, undefined, false],
  ['<=12:60:53', undefined, undefined, undefined, undefined, false],
  ['<=12:34:60', undefined, undefined, undefined, undefined, false],
  ['<=12:34:-10', undefined, undefined, undefined, undefined, false],
  ['<=12:34:', undefined, undefined, undefined, undefined, false],
  ['<=12:', undefined, undefined, undefined, undefined, false],
  ['<= 13/12', undefined, undefined, undefined, undefined, false],
  ['<= 13/', undefined, undefined, undefined, undefined, false],
  ['<= 12/32', undefined, undefined, undefined, undefined, false],
  ['<= 3/31', `${getDate(3, 31)}T00:00:00`, ComparisonType.LesserEqual, undefined, undefined, true],
  ['<= 3/32', undefined, undefined, undefined, undefined, false],
  ['>ccc', undefined, undefined, undefined, undefined, false],
  ['>=ccc', undefined, undefined, undefined, undefined, false],
  ['ccc', undefined, undefined, undefined, undefined, false],
  ['12/1 & 12:23', `${getDate(12, 1)}T00:00:00`, ComparisonType.Equal, `${getDate()}T12:23:00`, ComparisonType.Equal, true],
  ['>2020-05-12T20:13:12 &< 2020/5/13 12:23:53', '2020-05-12T20:13:12', ComparisonType.Greater, '2020-05-13T12:23:53', ComparisonType.Lesser, true],
  ['>=12/1 & < 05/15 12:23', `${getDate(12, 1)}T00:00:00`, ComparisonType.GreaterEqual, `${getDate(5, 15)}T12:23:00`, ComparisonType.Lesser, true],
  [`>= 12/1 01:2:3 & ${(new Date()).getFullYear()}/03/1`, `${getDate(12, 1)}T01:02:03`, ComparisonType.GreaterEqual, `${getDate(3, 1)}T00:00:00`, ComparisonType.Equal, true],
  ['&', undefined, undefined, undefined, undefined, false],
  ['>=', undefined, undefined, undefined, undefined, false],
  ['>', undefined, undefined, undefined, undefined, false],
  ['>&< 1/23', undefined, undefined, `${getDate(1, 23)}T00:00:00`, ComparisonType.Lesser, true],
])('parseFilterValue for DateTimeFilter', (
  input: string,
  value1: string,
  comparison1: ComparisonType,
  value2: string,
  comparison2: ComparisonType,
  isActive: boolean,
) => {
  test(`parsing is correct for ${input}`, () => {
    const filter = parseFilterValue(input, ColumnType.DateTime);
    expect(filter).toEqual<DateTimeFilter>({
      kind: ColumnType.DateTime,
      isActive,
      looksForNullValues: false,
      value1,
      comparison1,
      value2,
      comparison2,
    });
  });
});
