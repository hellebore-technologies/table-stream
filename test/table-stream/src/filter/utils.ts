import { createDateString } from '@helleboretech/table-stream/src/filter/parseFilterValue';

export default function getDateString(month?: number, day?: number) : string {
  const date = new Date();
  return createDateString(date.getFullYear(), month ?? date.getMonth() + 1, day ?? date.getDate());
}
