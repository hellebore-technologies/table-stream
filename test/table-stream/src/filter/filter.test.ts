import { ColumnType } from '@helleboretech/table-stream/src/types/enum';
import { ValueFilter } from '@helleboretech/table-stream/src/types/valueFilter';

import { applyValueFilter, parseFilterValue } from '@helleboretech/table-stream/src/filter';

describe.each<ValueFilter>([
  undefined,
  null,
  { kind: ColumnType.DateTime, isActive: false, looksForNullValues: false },
  { kind: ColumnType.Number, isActive: false, looksForNullValues: false },
  { kind: ColumnType.DateTime, isActive: false, looksForNullValues: false },
  {
    kind: ColumnType.String,
    isActive: false,
    looksForNullValues: false,
    value: 'ff',
    begin: false,
    end: false,
  },
])('applyFilter return true when given an invalid filter', (filter) => {
  const testData = [
    '',
    null,
    undefined,
    ' ',
    'foo',
    '12,\n',
  ];
  test.each(testData)(`paseFilterValue for "${filter}", %p`, (input) => {
    expect(applyValueFilter(filter, input)).toBeTruthy();
  });
});

describe.each([
  ['foo', ['foobar', 'Foobar', 'foo', 'foo bar', 'FOO', '^foobar', 'barfoo', 'some Foo thing'], true],
  ['foo', ['oobar', 'fo', 'fo bar', 'wyz'], false],
  ['^foo', ['foobar', 'Foobar', 'foo', 'foo bar', 'FOO'], true],
  ['^foo', ['^foobar', 'oobar', 'fo', 'fo bar', 'wyz', 'barfoo'], false],
  ['^ foo', [' foobar', ' Foobar', ' foo', ' foo bar', ' FOO'], true],
  ['^ foo', ['^foobar', 'oobar', 'fo', 'fo bar', 'wyz', 'barfoo', 'foobar', 'Foobar', 'foo', 'foo bar', 'FOO'], false],
  ['bar$', ['foobar', 'Foobar', 'fooBAR', 'foo bar', 'BAR'], true],
  ['bar$', ['foobar$', 'fooba', 'fo', 'foo br', 'wyz', 'barfoo'], false],
  ['^foo$', ['foo', 'FOO', 'Foo', 'fOo'], true],
  ['^foo$', ['foobar$', '^foo$', ' foo', 'foob'], false],
  ['/^foo', ['^foobar', '^Foobar', '^foo', '^foo bar', '^FOO', 'bar^foo', 'some ^Foo thing'], true],
  ['/^foo', ['foobar', 'oobar', 'fo', 'fo bar', 'wyz', 'barfoo'], false],
  ['bar/$', ['foobar$', 'Foobar$', 'fooBAR$', 'foo bar$ toto', 'BAR$n', 'bar$'], true],
  ['bar/$', ['fooba', 'fo', 'foo br', 'wyz', 'barfoo'], false],
  ['//', [undefined, null], true],
  ['//', ['', 'fo', ' '], false],
])('applyFilter for StringFilter', (filterValue, inputs, result) => {
  test.each(inputs)(`applyFilter with: "${filterValue}", %p => ${result}`, (input) => {
    const filter = parseFilterValue(filterValue, ColumnType.String);
    expect(applyValueFilter(filter, input)).toBe(result);
  });
});

describe.each([
  ['12', [12, 12.0], true],
  ['12', [13], false],
  ['=12', [12, 12.0], true],
  ['=12', [13], false],
  ['>12', [12.1, 13.0, 1020], true],
  ['>12', [12, 10, -12, 0], false],
  ['<12', [11.9, 10.0, -1], true],
  ['<12', [12, 12.1, 13.0, 1034], false],
  ['>=12', [12, 12.1, 13.0, 1020], true],
  ['>=12', [10, -12, 0], false],
  ['<=12', [12, 11.9, 10.0, -1], true],
  ['<=12', [12.1, 13.0, 1034], false],
  ['>-3.12', [-3.11, 1, 0], true],
  ['>-3.12', [-3.12, -3.13, -4], false],
  ['<-3.12', [-3.13, -4], true],
  ['<-3.12', [-3.12, -3.11, 0], false],
  ['>=-3.12', [-3.12, -3.11, 1, 0], true],
  ['>=-3.12', [-3.13, -4], false],
  ['<=-3.12', [-3.12, -3.13, -4], true],
  ['<=-3.12', [-3.11, 0], false],
  ['12&13', [12, 13], false],
  ['&>=13', [13, 15], true],
  ['&>=13', [1, 12], false],
  ['>=12k', [12_000, 234_000], true],
  ['>=12K', [12_000, 234_000], true],
  ['>=12M', [12_000_000, 234_000_000], true],
  ['>=12m', [12_000_000, 234_000_000], true],
  ['>=12&13', [13], true],
  ['>=12&13', [12, 12.5], false],
  ['<=12&>=10', [12, 10, 11, 10.5], true],
  ['<=12&>=10', [12.1, 9.9, -1, 0, 14], false],
  ['>-3.12&<=5', [-3.11, 5, 0, 1.1], true],
  ['>-3.12&<=5', [-3.12, -3.13, 5.1], false],
  ['>= -3.12 & < 20', [-3.11, -3.12, 5, 0, 1.1, 19.9], true],
  ['>= -3.12 & < 20', [-3.13, 20, 23, -3.15], false],
  ['<=-3.12&>5', [-3.13, 20, 23, -3.15, -3.11, -3.12, 5, 0, 1.1, 19.9], false],
  ['//', [undefined, null], true],
  ['//', [-3.13, 20, 23, -3.15, -3.11, -3.12, 5, 0, 1.1, 19.9], false],
])('applyFilter for NumberFilter', (filterValue, inputs, result) => {
  test.each(inputs)(`applyFilter with: "${filterValue}", %p => ${result}`, (input) => {
    const filter = parseFilterValue(filterValue, ColumnType.Number);
    expect(applyValueFilter(filter, input)).toBe(result);
  });
});

describe.each([
  ['2020-05-12T12:20:23', ['2020-05-12T12:20:23'], true],
  ['2020-05-12T12:20:23', ['2020-05-12T12:20:22'], false],
  ['=2020/05/12 12:20:23', ['2020-05-12T12:20:23'], true],
  ['=2020-05-12T12:20:23', ['2020-05-12T12:20:24'], false],
  ['>2020-05-12T12:20:23', ['2020-05-12T12:20:24', '2020-06-12', '2020-05-12T18:00:00'], true],
  ['>2020-05-12T12:20:23', ['2020-05-12T12:20:22', '2020-05-12', '2020-05-12T10:00:00', '2020-05-12T12:20:23'], false],
  ['<2020-05-12T12:20:23', ['2020-05-12T12:20:22', '2020-05-12', '2020-05-12T10:00:00'], true],
  ['<2020-05-12T12:20:23', ['2020-05-12T12:20:24', '2020-06-12', '2020-05-12T18:00:00', '2020-05-12T12:20:23'], false],
  ['>=2020-05-12T12:20:23', ['2020-05-12T12:20:24', '2020-06-12', '2020-05-12T18:00:00', '2020-05-12T12:20:23'], true],
  ['>=2020-05-12T12:20:23', ['2020-05-12T12:20:22', '2020-05-12', '2020-05-12T10:00:00'], false],
  ['<=2020-05-12T12:20:23', ['2020-05-12T12:20:22', '2020-05-12', '2020-05-12T10:00:00', '2020-05-12T12:20:23'], true],
  ['<=2020-05-12T12:20:23', ['2020-05-12T12:20:24', '2020-06-12', '2020-05-12T18:00:00'], false],
  ['2020-05-12T12:20:23&2020-05-12T18:20:23', ['2020-05-12T12:20:23', '2020-05-12T18:20:23'], false],
  ['>=2020-05-12T12:20:23&2020-05-12T18:20:23', ['2020-05-12T18:20:23'], true],
  ['>=2020-05-12T12:20:23&<2020-05-12T18:20:23', ['2020-05-12T12:20:23', '2020-05-12T16:20:23', '2020-05-12T18:20:22'], true],
  ['>=2020-05-12T12:20:23&<2020-05-12T18:20:23', ['2020-06-12T16:19:22', '2020-05-12T18:20:24'], false],
  ['>= 2020/05/12 12:20:23 &< 2020/05/12 18:20:23', ['2020-05-12T12:20:23', '2020-05-12T16:20:23', '2020-05-12T18:20:22'], true],
  ['>= 2020/05/12 12:20:23 & < 2020/05/12 18:20:23', ['2020-06-12T16:19:22', '2020-05-12T18:20:24'], false],
  ['//', [undefined, null], true],
  ['//', ['2020-06-12T16:19:22', '2020-05-12T18:20:24'], false],
])('applyFilter for DateTimeFilter', (filterValue, inputs, result) => {
  test.each(inputs)(`applyFilter with: "${filterValue}", %p => ${result}`, (input) => {
    const filter = parseFilterValue(filterValue, ColumnType.DateTime);
    expect(applyValueFilter(filter, input)).toBe(result);
  });
});
