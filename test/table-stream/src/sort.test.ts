import { ColumnType } from '@helleboretech/table-stream/src/types/enum';
import { Column } from '@helleboretech/table-stream/src/types/column';
import { Order, Ordering } from '@helleboretech/table-stream/src/types/filter';
import sortData from '@helleboretech/table-stream/src/sort';

interface TestData extends Record<string, unknown> {
  col1: number;
  col2: string;
  col3: string;
}

const columns: Column<TestData>[] = [
  {
    id: 'col1',
    type: ColumnType.Number,
    displayName: '',
    flex: '',
    hasFilter: false,
    isSortable: true,
    getData: (d) => d.col1,
  },
  {
    id: 'col2',
    type: ColumnType.String,
    displayName: '',
    flex: '',
    hasFilter: false,
    isSortable: true,
    getData: (d) => d.col2,
  },
  {
    id: 'col3',
    type: ColumnType.DateTime,
    displayName: '',
    flex: '',
    hasFilter: false,
    isSortable: true,
    isLocalTime: false,
    getData: (d) => d.col3,
  },
];

describe.each([
  [[12, -213, null, 0, 120, null], [-213, 0, 12, 120, null, null]],
  [[-3.15, -1, null, null, 11, 1010], [-3.15, -1, 11, 1010, null, null]],
  [[null, null, 5, 1], [1, 5, null, null]],
  [
    [null, 99, 1230, null, null, 450, -5000, null, -1],
    [-5000, -1, 99, 450, 1230, null, null, null, null],
  ],
])('sort correctly for Number column ascending', (input, result) => {
  const ordering: Ordering = { fieldName: 'col1', order: Order.Ascending };

  test(`sort array: ${input}`, () => {
    const data = input.map((d) => ({ col1: d, col2: 'foo', col3: '' }));

    const actual = sortData(data, columns, ordering).map((d) => d.col1);

    expect(actual).toStrictEqual(result);
  });
});

describe.each([
  [[12, -213, null, 0, 120, null], [120, 12, 0, -213, null, null]],
  [[-3.15, -1, null, null, 11, 1010], [1010, 11, -1, -3.15, null, null]],
  [[null, null, 5, 1], [5, 1, null, null]],
  [
    [null, 99, 1230, null, null, 450, -5000, null, -1],
    [1230, 450, 99, -1, -5000, null, null, null, null],
  ],
])('sort correctly for Number column descending', (input, result) => {
  const ordering: Ordering = { fieldName: 'col1', order: Order.Descending };

  test(`sort array: ${input}`, () => {
    const data = input.map((d) => ({ col1: d, col2: 'foo', col3: '' }));

    const actual = sortData(data, columns, ordering).map((d) => d.col1);

    expect(actual).toStrictEqual(result);
  });
});

describe.each([
  [['a', 'b', null, 'A', 'c', 'd', null], ['A', 'a', 'b', 'c', 'd', null, null]],
  [['a', 'b', null, null, 'c', 'd'], ['a', 'b', 'c', 'd', null, null]],
  [[null, null, 'c', 'd'], ['c', 'd', null, null]],
  [
    [null, '99', '1230', null, null, 'c', 'FooBar', null, 'd'],
    ['1230', '99', 'FooBar', 'c', 'd', null, null, null, null],
  ],
])('sort correctly for String column ascending', (input, result) => {
  const ordering: Ordering = { fieldName: 'col2', order: Order.Ascending };

  test(`sort array: ${input}`, () => {
    const data = input.map((d) => ({ col1: 1, col2: d, col3: '' }));

    const actual = sortData(data, columns, ordering).map((d) => d.col2);

    expect(actual).toStrictEqual(result);
  });
});

describe.each([
  [['a', 'b', null, 'c', 'd', null], ['d', 'c', 'b', 'a', null, null]],
  [['a', 'b', null, null, 'c', 'd'], ['d', 'c', 'b', 'a', null, null]],
  [[null, null, 'c', 'd'], ['d', 'c', null, null]],
  [
    [null, '99', '1230', null, null, 'c', 'FooBar', null, 'd'],
    ['d', 'c', 'FooBar', '99', '1230', null, null, null, null],
  ],
])('sort correctly for String column descending', (input, result) => {
  const ordering: Ordering = { fieldName: 'col2', order: Order.Descending };

  test(`sort array: ${input}`, () => {
    const data = input.map((d) => ({ col1: 1, col2: d, col3: '' }));

    const actual = sortData(data, columns, ordering).map((d) => d.col2);

    expect(actual).toStrictEqual(result);
  });
});

describe.each([
  [
    ['2020-05-12T12:20:23', null, '2020-05-12T12:20:22', '2020-06-12T12:20:23', null],
    ['2020-05-12T12:20:22', '2020-05-12T12:20:23', '2020-06-12T12:20:23', null, null]],
  [
    ['2024-05-12T12:20:23', '2021-05-12T12:20:23', null, null, '2020-05-12T12:20:23'],
    ['2020-05-12T12:20:23', '2021-05-12T12:20:23', '2024-05-12T12:20:23', null, null],
  ],
  [
    [null, null, '2020-05-12T12:28:23', '2020-05-12T12:20:23'],
    ['2020-05-12T12:20:23', '2020-05-12T12:28:23', null, null]],
  [
    [null, '2022-05-12T12:20:23', null, null, '2024-05-12T12:20:23', null, '2020-05-12T12:20:23'],
    ['2020-05-12T12:20:23', '2022-05-12T12:20:23', '2024-05-12T12:20:23', null, null, null, null],
  ],
])('sort correctly for DateTime column ascending', (input, result) => {
  const ordering: Ordering = { fieldName: 'col3', order: Order.Ascending };

  test(`sort array: ${input}`, () => {
    const data = input.map((d) => ({ col1: 1, col2: 'foo', col3: d }));

    const actual = sortData(data, columns, ordering).map((d) => d.col3);

    expect(actual).toStrictEqual(result);
  });
});

describe.each([
  [
    ['2020-05-12T12:20:23', null, '2020-05-12T12:20:22', '2020-06-12T12:20:23', null],
    ['2020-06-12T12:20:23', '2020-05-12T12:20:23', '2020-05-12T12:20:22', null, null]],
  [
    ['2024-05-12T12:20:23', '2021-05-12T12:20:23', null, null, '2020-05-12T12:20:23'],
    ['2024-05-12T12:20:23', '2021-05-12T12:20:23', '2020-05-12T12:20:23', null, null],
  ],
  [
    [null, null, '2020-05-12T12:28:23', '2020-05-12T12:20:23'],
    ['2020-05-12T12:28:23', '2020-05-12T12:20:23', null, null]],
  [
    [null, '2022-05-12T12:20:23', null, null, '2024-05-12T12:20:23', null, '2020-05-12T12:20:23'],
    ['2024-05-12T12:20:23', '2022-05-12T12:20:23', '2020-05-12T12:20:23', null, null, null, null],
  ],
])('sort correctly for DateTime column descending', (input, result) => {
  const ordering: Ordering = { fieldName: 'col3', order: Order.Descending };

  test(`sort array: ${input}`, () => {
    const data = input.map((d) => ({ col1: 1, col2: 'foo', col3: d }));

    const actual = sortData(data, columns, ordering).map((d) => d.col3);

    expect(actual).toStrictEqual(result);
  });
});
