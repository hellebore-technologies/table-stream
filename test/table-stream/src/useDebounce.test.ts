import { act, renderHook } from '@testing-library/react-hooks';
import useDebounce from '@helleboretech/table-stream/src/useDebounce';

jest.useFakeTimers();

test('useDebounce should update value passed only after delay', () => {
  let value = 'foo';
  const { result, rerender } = renderHook(() => useDebounce(value, 250));

  expect(result.current).toBe('foo');

  value = 'bar';
  rerender();
  expect(result.current).toBe('foo');

  value = 'fooBar';
  rerender();
  expect(result.current).toBe('foo');

  act(() => {
    jest.runAllTimers();
  });
  expect(result.current).toBe('fooBar');
});
