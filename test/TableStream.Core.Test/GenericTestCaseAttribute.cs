﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;
using NUnit.Framework.Internal.Builders;

namespace HelleboreTech.TableStream.Core.Test
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class GenericTestCaseAttribute : TestCaseAttribute, ITestBuilder
    {
        public GenericTestCaseAttribute(params object[] arguments) : base(arguments)
        {
        }

        public Type[] TypeArguments { get; set; }

        IEnumerable<TestMethod> ITestBuilder.BuildFrom(IMethodInfo method, NUnit.Framework.Internal.Test test)
        {
            if (!method.IsGenericMethodDefinition)
                return base.BuildFrom(method, test);

            if (TypeArguments == null || TypeArguments.Length != method.GetGenericArguments().Length)
            {
                var testParams = new TestCaseParameters { RunState = RunState.NotRunnable };
                testParams.Properties.Set(
                    "_SKIPREASON",
                    $"{nameof(TypeArguments)} should have {method.GetGenericArguments().Length} elements");
                return new[] { new NUnitTestCaseBuilder().BuildTestMethod(method, test, testParams) };
            }

            var gm = method.MakeGenericMethod(TypeArguments);
            return BuildFrom(gm, test);
        }
    }
}
