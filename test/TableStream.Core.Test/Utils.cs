﻿using System.Linq;

namespace HelleboreTech.TableStream.Core.Test
{
    public static class Utils
    {
        public static bool AreEquivalent<TData>(Ordering ordering, OrderingWithProperty<TData> orderingWithProperty)
            where TData : FilteredItem
        {
            if (ordering == null && orderingWithProperty == null)
                return true;
            
            return orderingWithProperty.Order == ordering.Order
                && orderingWithProperty.DataPropertyName == ordering.FieldName;
        }
        
        public static bool AreEqual<TData, U>(PropertyValueFilter<TData, U> prop1, PropertyValueFilter<TData, U> prop2)
            where TData : FilteredItem
        {
            return prop1.DataPropertyName == prop2.DataPropertyName 
                   && (
                       (prop1.ValueFilter == null && prop2.ValueFilter == null)
                       || prop1.ValueFilter.Equals(prop2.ValueFilter)
                   );
        }

        public static bool ComparePropertyValueFilters<T>(PropertyValueFilters<T> propVF1, PropertyValueFilters<T> propVF2)
            where T : FilteredItem
        {
            {
                var list1 = propVF1.NumberFilters.OrderBy(p => p.DataPropertyName).ToList();
                var list2 = propVF2.NumberFilters.OrderBy(p => p.DataPropertyName).ToList();
                if (list1.Count != list2.Count)
                    return false;

                if (
                    list1
                    .Where((t, i) => !AreEqual(t, list2[i]))
                    .Any()
                )
                    return false;
            }
            {
                var list1 = propVF1.StringFilters.OrderBy(p => p.DataPropertyName).ToList();
                var list2 = propVF2.StringFilters.OrderBy(p => p.DataPropertyName).ToList();
                if (list1.Count != list2.Count)
                    return false;

                if (
                    list1
                    .Where((t, i) => !AreEqual(t, list2[i]))
                    .Any()
                )
                    return false;
            }
            {
                var list1 = propVF1.SelectFilters.OrderBy(p => p.DataPropertyName).ToList();
                var list2 = propVF2.SelectFilters.OrderBy(p => p.DataPropertyName).ToList();
                if (list1.Count != list2.Count)
                    return false;

                if (
                    list1
                    .Where((t, i) => !AreEqual(t, list2[i]))
                    .Any()
                )
                    return false;
            }
            {
                var list1 = propVF1.DateTimeFilters.OrderBy(p => p.DataPropertyName).ToList();
                var list2 = propVF2.DateTimeFilters.OrderBy(p => p.DataPropertyName).ToList();
                if (list1.Count != list2.Count)
                    return false;

                if (
                    list1
                    .Where((t, i) => !AreEqual(t, list2[i]))
                    .Any()
                )
                    return false;
            }
            return true;
        } 
    }
}
