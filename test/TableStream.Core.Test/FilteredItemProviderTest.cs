using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HelleboreTech.TableStream.Core.Exceptions;
using Moq;
using NUnit.Framework;

namespace HelleboreTech.TableStream.Core.Test
{
    public class FilteredItemProviderTest
    {
        [GenericTestCase(
            TypeArguments = new[] { typeof(InvalidAliasItem), typeof(SortAliasCreationException) },
            TestName = nameof(InvalidAliasItem))]
        [GenericTestCase(
            TypeArguments = new[] { typeof(InvalidPropsItem), typeof(InvalidPropertyException) },
            TestName = nameof(InvalidPropsItem))]
        public void FilteredItemProvider_ShouldThrowOnInvalidObject<TData, TError>()
            where TData : FilteredItem
            where TError : Exception
        {
            var mock = new Mock<IFilteredItemProvider<TData>>();

            Assert.Throws<TError>(
                () =>
                {
                    var itemProvider = new FilteredItemProvider<TData, ItemFilter>(mock.Object);
                });
        }
        
        [Test]
        public void FilteredItemProvider_ShouldAllowItemWithoutAllFilter()
        {
            var mock = new Mock<IFilteredItemProvider<MissingPropsItem>>();

            var itemProvider = new FilteredItemProvider<MissingPropsItem, ItemFilter>(mock.Object);

            Assert.NotNull(itemProvider);
        }
        
        #region GetDataByIds

        [Test]
        public void GetDataByIds_ShouldAllowNullOrdering()
        {
            var mock = new Mock<IFilteredItemProvider<Item>>();
            var itemProvider = new FilteredItemProvider<Item, ItemFilter>(mock.Object);

            Ordering ordering = null;
            var ids = new List<Item>
            {
                new() { Column1 = "1" },
                new() { Column1 = "2" },
                new() { Column1 = "3" },
                new() { Column1 = "4" },
            };

            itemProvider.GetDataByIds(ids, ordering);

            mock.Verify(
                f => f.GetDataById(
                    It.Is<List<Item>>(i => i == ids),
                    It.Is<OrderingWithProperty<Item>>(o => o == null))
            );
        }
        
        [Test]
        public void GetDataByIds_ShouldThrowOnInvalidOrderingField()
        {
            var mock = new Mock<IFilteredItemProvider<Item>>();
            var itemProvider = new FilteredItemProvider<Item, ItemFilter>(mock.Object);

            var ordering = new Ordering
            {
                Order = Order.Ascending,
                FieldName = "foobar",
            };
            var ids = new List<Item>
            {
                new() { Column1 = "1" },
                new() { Column1 = "2" },
                new() { Column1 = "3" },
                new() { Column1 = "4" },
            };

            Assert.Throws<SortAliasNotFoundException>(
                () => { itemProvider.GetDataByIds(ids, ordering); });
        }

        [Test]
        public void GetDataByIds_ShouldGetSortColumnName()
        {
            var mock = new Mock<IFilteredItemProvider<Item>>();
            var itemProvider = new FilteredItemProvider<Item, ItemFilter>(mock.Object);

            var ordering = new Ordering { Order = Order.Descending, FieldName = nameof(Item.Column2) };
            var ids = new List<Item>
            {
                new() { Column1 = "1" },
                new() { Column1 = "2" },
                new() { Column1 = "3" },
                new() { Column1 = "4" },
            };

            itemProvider.GetDataByIds(ids, ordering);

            mock.Verify(
                f => f.GetDataById(
                    It.Is<List<Item>>(i => i == ids),
                    It.Is<OrderingWithProperty<Item>>(
                        o => o.Order == ordering.Order && o.DataPropertyName == nameof(Item.Column2)))
            );
        }

        [Test]
        public void GetDataByIds_ShouldAliasSortColumnName()
        {
            var mock = new Mock<IFilteredItemProvider<Item>>();
            var itemProvider = new FilteredItemProvider<Item, ItemFilter>(mock.Object);

            var ordering = new Ordering { Order = Order.Ascending, FieldName = nameof(Item.Column3) };
            var ids = new List<Item>
            {
                new() { Column1 = "1" },
                new() { Column1 = "2" },
                new() { Column1 = "3" },
                new() { Column1 = "4" },
            };

            itemProvider.GetDataByIds(ids, ordering);

            mock.Verify(
                f => f.GetDataById(
                    It.Is<List<Item>>(i => i == ids),
                    It.Is<OrderingWithProperty<Item>>(
                        o => o.Order == ordering.Order && o.DataPropertyName == nameof(Item.Column3Sort)))
            );
        }
        
        #endregion

        #region GetDataByIdsAsync 

        [Test]
        public async Task GetDataByIdAsync_ShouldAllowNullOrdering()
        {
            var mock = new Mock<IFilteredItemProvider<Item>>();
            var itemProvider = new FilteredItemProvider<Item, ItemFilter>(mock.Object);

            Ordering ordering = null;
            var ids = new List<Item>
            {
                new() { Column1 = "1" },
                new() { Column1 = "2" },
                new() { Column1 = "3" },
                new() { Column1 = "4" },
            };

            await itemProvider.GetDataByIdsAsync(ids, ordering);

            mock.Verify(
                f => f.GetDataByIdAsync(
                    It.Is<List<Item>>(i => i == ids),
                    It.Is<OrderingWithProperty<Item>>(o => o == null))
            );
        }

        [Test]
        public void GetDataByIdAsync_ShouldThrowOnInvalidOrderingField()
        {
            var mock = new Mock<IFilteredItemProvider<Item>>();
            var itemProvider = new FilteredItemProvider<Item, ItemFilter>(mock.Object);

            var ordering = new Ordering
            {
                Order = Order.Ascending,
                FieldName = "foobar",
            };
            var ids = new List<Item>
            {
                new() { Column1 = "1" },
                new() { Column1 = "2" },
                new() { Column1 = "3" },
                new() { Column1 = "4" },
            };

            Assert.Throws<SortAliasNotFoundException>(
                () => { itemProvider.GetDataByIdsAsync(ids, ordering).GetAwaiter().GetResult(); });
        }

        [Test]
        public async Task GetDataByIdAsync_ShouldGetSortColumnName()
        {
            var mock = new Mock<IFilteredItemProvider<Item>>();
            var itemProvider = new FilteredItemProvider<Item, ItemFilter>(mock.Object);

            var ordering = new Ordering { Order = Order.Descending, FieldName = nameof(Item.Column2) };
            var ids = new List<Item>
            {
                new() { Column1 = "1" },
                new() { Column1 = "2" },
                new() { Column1 = "3" },
                new() { Column1 = "4" },
            };

            await itemProvider.GetDataByIdsAsync(ids, ordering);

            mock.Verify(
                f => f.GetDataByIdAsync(
                    It.Is<List<Item>>(i => i == ids),
                    It.Is<OrderingWithProperty<Item>>(
                        o => o.Order == ordering.Order && o.DataPropertyName == nameof(Item.Column2)))
            );
        }

        [Test]
        public async Task GetDataByIdAsync_ShouldAliasSortColumnName()
        {
            var mock = new Mock<IFilteredItemProvider<Item>>();
            var itemProvider = new FilteredItemProvider<Item, ItemFilter>(mock.Object);

            var ordering = new Ordering { Order = Order.Ascending, FieldName = nameof(Item.Column3) };
            var ids = new List<Item>
            {
                new() { Column1 = "1" },
                new() { Column1 = "2" },
                new() { Column1 = "3" },
                new() { Column1 = "4" },
            };

            await itemProvider.GetDataByIdsAsync(ids, ordering);

            mock.Verify(
                f => f.GetDataByIdAsync(
                    It.Is<List<Item>>(i => i == ids),
                    It.Is<OrderingWithProperty<Item>>(
                        o => o.Order == ordering.Order && o.DataPropertyName == nameof(Item.Column3Sort)))
            );
        }

        #endregion

        #region GetData

        [Test]
        public void GetData_ShouldWorkProperly()
        {
            var mock = new Mock<IFilteredItemProvider<Item>>();
            var itemProvider = new FilteredItemProvider<Item, ItemFilter>(mock.Object);
            var (filter, propValueFilters) = GenerateFilters();

            itemProvider.GetData(filter);

            mock.Verify(
                f => f.GetData(
                    It.Is<PropertyValueFilters<Item>>(
                        p => Utils.ComparePropertyValueFilters(p, propValueFilters)),
                    It.Is<OrderingWithProperty<Item>>(o => Utils.AreEquivalent(filter.Ordering, o)),
                    It.Is<List<object>>(o => o == filter.SearchAfter)
                    )
            ); 
        }
        
        [Test]
        public void GetData_ShouldAllowNullOrdering()
        {
            var mock = new Mock<IFilteredItemProvider<Item>>();
            var itemProvider = new FilteredItemProvider<Item, ItemFilter>(mock.Object);
            var (filter, propValueFilters) = GenerateFilters();
            
            Ordering ordering = null;
            filter.Ordering = ordering;

            itemProvider.GetData(filter);

            mock.Verify(
                f => f.GetData(
                    It.Is<PropertyValueFilters<Item>>(
                        p => Utils.ComparePropertyValueFilters(p, propValueFilters)),
                    It.Is<OrderingWithProperty<Item>>(o => Utils.AreEquivalent(filter.Ordering, o)),
                    It.Is<List<object>>(o => o == filter.SearchAfter)
                    )
            ); 
        }

        [Test]
        public void GetData_ShouldThrowOnInvalidOrderingField()
        {
            var mock = new Mock<IFilteredItemProvider<Item>>();
            var itemProvider = new FilteredItemProvider<Item, ItemFilter>(mock.Object);
            var (filter, propValueFilters) = GenerateFilters();
            
            var ordering = new Ordering
            {
                Order = Order.Ascending,
                FieldName = "foobar",
            };
            filter.Ordering = ordering;

            Assert.Throws<SortAliasNotFoundException>(
                () => { itemProvider.GetData(filter); });
        }

        [Test]
        public void GetData_ShouldGetSortColumnName()
        {
            var mock = new Mock<IFilteredItemProvider<Item>>();
            var itemProvider = new FilteredItemProvider<Item, ItemFilter>(mock.Object);
            var (filter, propValueFilters) = GenerateFilters();
            
            var ordering = new Ordering { Order = Order.Descending, FieldName = nameof(Item.Column2) };
            filter.Ordering = ordering;

            itemProvider.GetData(filter);

            mock.Verify(
                f => f.GetData(
                    It.Is<PropertyValueFilters<Item>>(
                        p => Utils.ComparePropertyValueFilters(p, propValueFilters)),
                    It.Is<OrderingWithProperty<Item>>(o => 
                        o.Order == ordering.Order && o.DataPropertyName == nameof(Item.Column2)),
                    It.Is<List<object>>(o => o == filter.SearchAfter)
                    )
            ); 
        }

        [Test]
        public void GetData_ShouldAliasSortColumnName()
        {
            var mock = new Mock<IFilteredItemProvider<Item>>();
            var itemProvider = new FilteredItemProvider<Item, ItemFilter>(mock.Object);
            var (filter, propValueFilters) = GenerateFilters();
            
            var ordering = new Ordering { Order = Order.Ascending, FieldName = nameof(Item.Column3) };
            filter.Ordering = ordering;

            itemProvider.GetData(filter);

            mock.Verify(
                f => f.GetData(
                    It.Is<PropertyValueFilters<Item>>(
                        p => Utils.ComparePropertyValueFilters(p, propValueFilters)),
                    It.Is<OrderingWithProperty<Item>>(o => 
                        o.Order == ordering.Order && o.DataPropertyName == nameof(Item.Column3Sort)),
                    It.Is<List<object>>(o => o == filter.SearchAfter)
                    )
            ); 
        }
        
        #endregion

        #region GetDataAsync

        [Test]
        public async Task GetDataAsync_ShouldWorkProperly()
        {
            var mock = new Mock<IFilteredItemProvider<Item>>();
            var itemProvider = new FilteredItemProvider<Item, ItemFilter>(mock.Object);
            var (filter, propValueFilters) = GenerateFilters();

            await itemProvider.GetDataAsync(filter);

            mock.Verify(
                f => f.GetDataAsync(
                    It.Is<PropertyValueFilters<Item>>(
                        p => Utils.ComparePropertyValueFilters(p, propValueFilters)),
                    It.Is<OrderingWithProperty<Item>>(o => Utils.AreEquivalent(filter.Ordering, o)),
                    It.Is<List<object>>(o => o == filter.SearchAfter)
                    )
            ); 
        }
        
        [Test]
        public async Task GetDataAsync_ShouldAllowNullOrdering()
        {
            var mock = new Mock<IFilteredItemProvider<Item>>();
            var itemProvider = new FilteredItemProvider<Item, ItemFilter>(mock.Object);
            var (filter, propValueFilters) = GenerateFilters();
            
            Ordering ordering = null;
            filter.Ordering = ordering;

            await itemProvider.GetDataAsync(filter);

            mock.Verify(
                f => f.GetDataAsync(
                    It.Is<PropertyValueFilters<Item>>(
                        p => Utils.ComparePropertyValueFilters(p, propValueFilters)),
                    It.Is<OrderingWithProperty<Item>>(o => Utils.AreEquivalent(filter.Ordering, o)),
                    It.Is<List<object>>(o => o == filter.SearchAfter)
                    )
            ); 
        }

        [Test]
        public void GetDataAsync_ShouldThrowOnInvalidOrderingField()
        {
            var mock = new Mock<IFilteredItemProvider<Item>>();
            var itemProvider = new FilteredItemProvider<Item, ItemFilter>(mock.Object);
            var (filter, propValueFilters) = GenerateFilters();
            
            var ordering = new Ordering
            {
                Order = Order.Ascending,
                FieldName = "foobar",
            };
            filter.Ordering = ordering;

            Assert.Throws<SortAliasNotFoundException>(
                () => { itemProvider.GetDataAsync(filter).GetAwaiter().GetResult(); });
        }

        [Test]
        public async Task GetDataAsync_ShouldGetSortColumnName()
        {
            var mock = new Mock<IFilteredItemProvider<Item>>();
            var itemProvider = new FilteredItemProvider<Item, ItemFilter>(mock.Object);
            var (filter, propValueFilters) = GenerateFilters();
            
            var ordering = new Ordering { Order = Order.Descending, FieldName = nameof(Item.Column2) };
            filter.Ordering = ordering;

            await itemProvider.GetDataAsync(filter);

            mock.Verify(
                f => f.GetDataAsync(
                    It.Is<PropertyValueFilters<Item>>(
                        p => Utils.ComparePropertyValueFilters(p, propValueFilters)),
                    It.Is<OrderingWithProperty<Item>>(o => o.Order == ordering.Order && o.DataPropertyName == nameof(Item.Column2)),
                    It.Is<List<object>>(o => o == filter.SearchAfter)
                    )
            ); 
        }

        [Test]
        public async Task GetDataAsync_ShouldAliasSortColumnName()
        {
            var mock = new Mock<IFilteredItemProvider<Item>>();
            var itemProvider = new FilteredItemProvider<Item, ItemFilter>(mock.Object);
            var (filter, propValueFilters) = GenerateFilters();
            
            var ordering = new Ordering { Order = Order.Ascending, FieldName = nameof(Item.Column3) };
            filter.Ordering = ordering;

            await itemProvider.GetDataAsync(filter);

            mock.Verify(
                f => f.GetDataAsync(
                    It.Is<PropertyValueFilters<Item>>(
                        p => Utils.ComparePropertyValueFilters(p, propValueFilters)),
                    It.Is<OrderingWithProperty<Item>>(o => o.Order == ordering.Order && o.DataPropertyName == nameof(Item.Column3Sort)),
                    It.Is<List<object>>(o => o == filter.SearchAfter)
                    )
            ); 
        }

        #endregion

        private (Filter<ItemFilter> filter, PropertyValueFilters<Item> props) GenerateFilters()
        {
            var itemFilter = new ItemFilter
            {
                Column1 = new (),
                Column2 = new (),
                Column3 = null,
            };
            var filter = new Filter<ItemFilter>
            {
                Ordering = new Ordering
                {
                    FieldName = nameof(Item.Column1),
                    Order = Order.Ascending,
                },
                SearchAfter = null,
                ValueFilters = itemFilter,
            };
            var propValueFilters = new PropertyValueFilters<Item>
            {
                NumberFilters = new()
                {
                    new PropertyValueFilter<Item, NumberFilter>
                    {
                        DataPropertyName = nameof(itemFilter.Column3), 
                        ValueFilter = itemFilter.Column3
                    }
                },
                StringFilters = new()
                {
                    new PropertyValueFilter<Item, StringFilter>
                    {
                        DataPropertyName = nameof(itemFilter.Column1), 
                        ValueFilter = itemFilter.Column1
                    }
                },
                SelectFilters = new(),
                DateTimeFilters = new()
                {
                    new PropertyValueFilter<Item, DateTimeFilter>
                    {
                        DataPropertyName = nameof(itemFilter.Column2), 
                        ValueFilter = itemFilter.Column2
                    }
                },
            };

            return (filter, propValueFilters);
        }
    }
}
