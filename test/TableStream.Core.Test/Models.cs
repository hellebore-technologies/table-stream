﻿using System;

namespace HelleboreTech.TableStream.Core.Test
{
    public class Item : FilteredItem
    {
        public string Column1 { get; set; }
        public DateTime Column2 { get; set; }
        public double Column3 { get; set; }

        [AdditionalField(AdditionalFieldType.String)]
        public string Column3Display { get; set; }

        [SortFieldFor(nameof(Column3))]
        [AdditionalField(AdditionalFieldType.Double)]
        public double Column3Sort { get; set; }
    }

    public class InvalidAliasItem : FilteredItem
    {
        public string Column1 { get; set; }
        public DateTime Column2 { get; set; }
        public double Column3 { get; set; }

        [AdditionalField(AdditionalFieldType.String)]
        public string Column3Display { get; set; }

        [SortFieldFor("unknown prop")]
        [AdditionalField(AdditionalFieldType.Double)]
        public double Column3Sort { get; set; }
    }

    public class InvalidPropsItem : FilteredItem
    {
        public string Column1 { get; set; }
        public DateTime Column2 { get; set; }
        public string Column3Display { get; set; }
    }
        
    public class MissingPropsItem : FilteredItem
    {
        public string Column1 { get; set; }
        public DateTime Column2 { get; set; }
    }

    public class ItemFilter
    {
        public StringFilter Column1 { get; set; }
        public DateTimeFilter Column2 { get; set; }
        public NumberFilter Column3 { get; set; }
    }
}
