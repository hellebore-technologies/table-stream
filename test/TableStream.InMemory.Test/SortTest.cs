using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using HelleboreTech.TableStream.Core;
using NUnit.Framework;

namespace HelleboreTech.TableStream.InMemory.Test;

public class SortTest
{
    [TestCase(@"[-12, -7, 12, 100, 134, 250, null, null]", Order.Ascending)]
    [TestCase(@"[250, 134, 100, 12, -7, -12, null, null]", Order.Descending)]
    public void InMemoryProvider_ShouldSortNumberWhenAsked(string rawExpectedStr, Order order)
    {
        var rawData = new List<double?> { 12, null, 134, -7, 250, -12, null, 100, };
        var rawExpected = JsonSerializer.Deserialize<List<double?>>(rawExpectedStr)!;
        var data = rawData.Select(v => new TestData { Double = v }).ToList();
        var expected = rawExpected.Select(v => new TestData { Double = v }).ToList();

        var itemProvider = new FilteredItemProvider<TestData, TestDataFilter>(
            new InMemoryFilteredItemProvider<TestData>(data, TestData.FilterFactory));

        var filteredData = itemProvider.GetData(
            new Filter<TestDataFilter>
            {
                ValueFilters = new TestDataFilter(),
                Ordering = new Ordering
                {
                    FieldName = nameof(TestData.Double),
                    Order = order,
                },
                SearchAfter = new List<object>(),
            });
        
        Assert.That(filteredData.NumberOfResults, Is.EqualTo(expected.Count));
        Assert.That(JsonSerializer.Serialize(filteredData.Data), Is.EqualTo(JsonSerializer.Serialize(expected)));
    }
    
    [TestCase(@"[""1929-01-01"", ""1999-01-01"", ""2021-01-01"", ""2030-01-01"", ""2050-01-01"", null, null]", Order.Ascending)]
    [TestCase(@"[""2050-01-01"", ""2030-01-01"", ""2021-01-01"", ""1999-01-01"", ""1929-01-01"", null, null]", Order.Descending)]
    public void InMemoryProvider_ShouldSortDateWhenAsked(string rawExpectedStr, Order order)
    {
        var rawData = new List<DateTime?>
        {
            new DateTime(2021, 1, 1), 
            null, 
            new DateTime(2030, 1, 1), 
            new DateTime(2050, 1, 1), 
            new DateTime(1999, 1, 1), 
            null, 
            new DateTime(1929, 1, 1),
        };
        var rawExpected = JsonSerializer.Deserialize<List<DateTime?>>(rawExpectedStr)!;
        var data = rawData.Select(v => new TestData { Date = v }).ToList();
        var expected = rawExpected.Select(v => new TestData { Date = v }).ToList();

        var itemProvider = new FilteredItemProvider<TestData, TestDataFilter>(
            new InMemoryFilteredItemProvider<TestData>(data, TestData.FilterFactory));

        var filteredData = itemProvider.GetData(
            new Filter<TestDataFilter>
            {
                ValueFilters = new TestDataFilter(),
                Ordering = new Ordering
                {
                    FieldName = nameof(TestData.Date),
                    Order = order,
                },
                SearchAfter = new List<object>(),
            });
        
        Assert.That(filteredData.NumberOfResults, Is.EqualTo(expected.Count));
        Assert.That(JsonSerializer.Serialize(filteredData.Data), Is.EqualTo(JsonSerializer.Serialize(expected)));
    }
    
    [TestCase(@"[""01"", ""ABC"", ""abc"", ""dbc"", ""efd"", null, null]", Order.Ascending)]
    [TestCase(@"[""efd"", ""dbc"", ""abc"", ""ABC"", ""01"", null, null]", Order.Descending)]
    public void InMemoryProvider_ShouldSortStringWhenAsked(string rawExpectedStr, Order order)
    {
        var rawData = new List<string?> { "abc", null, "01", "dbc", "ABC", null, "efd", };
        var rawExpected = JsonSerializer.Deserialize<List<string?>>(rawExpectedStr)!;
        var data = rawData.Select(v => new TestData { Text = v }).ToList();
        var expected = rawExpected.Select(v => new TestData { Text = v }).ToList();

        var itemProvider = new FilteredItemProvider<TestData, TestDataFilter>(
            new InMemoryFilteredItemProvider<TestData>(data, TestData.FilterFactory));

        var filteredData = itemProvider.GetData(
            new Filter<TestDataFilter>
            {
                ValueFilters = new TestDataFilter(),
                Ordering = new Ordering
                {
                    FieldName = nameof(TestData.Text),
                    Order = order,
                },
                SearchAfter = new List<object>(),
            });
        
        Assert.That(filteredData.NumberOfResults, Is.EqualTo(expected.Count));
        Assert.That(JsonSerializer.Serialize(filteredData.Data), Is.EqualTo(JsonSerializer.Serialize(expected)));
    }
    
    [TestCase(@"[0, 0, 1, 1, 2, null, null]", Order.Ascending)]
    [TestCase(@"[2, 1, 1, 0, 0, null, null]", Order.Descending)]
    public void InMemoryProvider_ShouldSortEnumWhenAsked(string rawExpectedStr, Order order)
    {
        var rawData = new List<TestEnum?> { TestEnum.First, null, TestEnum.Third, TestEnum.Second, TestEnum.First, null, TestEnum.Second, };
        var rawExpected = JsonSerializer.Deserialize<List<TestEnum?>>(rawExpectedStr)!;
        var data = rawData.Select(v => new TestData { Enum = v }).ToList();
        var expected = rawExpected.Select(v => new TestData { Enum = v }).ToList();

        var itemProvider = new FilteredItemProvider<TestData, TestDataFilter>(
            new InMemoryFilteredItemProvider<TestData>(data, TestData.FilterFactory));

        var filteredData = itemProvider.GetData(
            new Filter<TestDataFilter>
            {
                ValueFilters = new TestDataFilter(),
                Ordering = new Ordering
                {
                    FieldName = nameof(TestData.Enum),
                    Order = order,
                },
                SearchAfter = new List<object>(),
            });
        
        Assert.That(filteredData.NumberOfResults, Is.EqualTo(expected.Count));
        Assert.That(JsonSerializer.Serialize(filteredData.Data), Is.EqualTo(JsonSerializer.Serialize(expected)));
    }
}
