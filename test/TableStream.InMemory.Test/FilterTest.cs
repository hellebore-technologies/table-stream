using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using HelleboreTech.TableStream.Core;
using NUnit.Framework;

namespace HelleboreTech.TableStream.InMemory.Test;

public class FilterTest
{
    [Test]
    public void InMemoryProvider_ShouldDoNothingWithEmptyFilter()
    {
        var data = new List<TestData>
        {
            new() { Double = 12, Date = DateTime.UtcNow, Enum = TestEnum.First, Text = "foobar" },
            new() { Date = DateTime.UtcNow, Enum = TestEnum.First, Text = "bar" },
            new() { Double = 12, Date = DateTime.UtcNow, Text = "foo" },
        };

        var itemProvider = new FilteredItemProvider<TestData, TestDataFilter>(
            new InMemoryFilteredItemProvider<TestData>(data, TestData.FilterFactory));

        var filteredData = itemProvider.GetData(
            new Filter<TestDataFilter>
            {
                ValueFilters = new(),
                SearchAfter = new List<object>(),
            });

        Assert.That(filteredData.NumberOfResults, Is.EqualTo(data.Count));
        Assert.That(JsonSerializer.Serialize(filteredData.Data), Is.EqualTo(JsonSerializer.Serialize(data)));
    }

    [Test]
    public void InMemoryProvider_ShouldReturnOnlyNumberAsked()
    {
        var data = new List<TestData>
        {
            new() { Double = 12, Date = DateTime.UtcNow, Enum = TestEnum.First, Text = "foobar" },
            new() { Date = DateTime.UtcNow, Enum = TestEnum.First, Text = "foobar" },
            new() { Double = 12, Date = DateTime.UtcNow, Text = "foobar" },
        };

        var itemProvider = new FilteredItemProvider<TestData, TestDataFilter>(
            new InMemoryFilteredItemProvider<TestData>(data, TestData.FilterFactory, batchSize: 1));

        var filteredData = itemProvider.GetData(
            new Filter<TestDataFilter>
            {
                ValueFilters = new(),
                SearchAfter = new List<object>(),
            });

        Assert.That(filteredData.NumberOfResults, Is.EqualTo(data.Count));
        Assert.That(filteredData.Data.Count, Is.EqualTo(1));
        Assert.That(JsonSerializer.Serialize(filteredData.Data), Is.EqualTo(JsonSerializer.Serialize(data.Take(1))));
    }

    [Test]
    public void InMemoryProvider_ShouldWorkWithComplexFilter()
    {
        var data = new List<TestData>
        {
            new() { Double = 12, Date = new DateTime(2023, 1, 1), Enum = TestEnum.First, Text = "foobar" },
            new() { Double = 12, Date = new DateTime(2023, 1, 1), Enum = TestEnum.Second, Text = "foobar" },
            new() { Double = null, Date = new DateTime(2023, 1, 1), Enum = TestEnum.First, Text = "foo" },
            new() { Double = 13, Date = new DateTime(2023, 1, 1), Enum = null, Text = "bar" },
            new() { Double = 14, Date = new DateTime(2023, 1, 1), Enum = TestEnum.Third, Text = "foobar" },
            new() { Double = 25, Date = new DateTime(2023, 3, 1), Enum = TestEnum.First, Text = "foo" },
            new() { Double = 13, Date = new DateTime(1923, 1, 1), Enum = TestEnum.First, Text = "bar" },
            new() { Double = 100, Date = new DateTime(2023, 1, 1), Enum = TestEnum.First, Text = "foobar" },
            new() { Double = 100, Date = null, Enum = TestEnum.First, Text = "foobar" },
        };
        var expected = new List<TestData>
        {
            data[7],
            data[5],
            data[4],
            data[0],
            data[2],
        };

        var itemProvider = new FilteredItemProvider<TestData, TestDataFilter>(
            new InMemoryFilteredItemProvider<TestData>(data, TestData.FilterFactory));

        var filteredData = itemProvider.GetData(
            new Filter<TestDataFilter>
            {
                ValueFilters = new TestDataFilter
                {
                    Date = new DateTimeFilter
                    {
                        IsActive = true,
                        Value1 = new DateTime(2023, 1, 1),
                        Comparison1 = ComparisonType.GreaterEqual,
                    },
                    Enum = new SelectFilter
                    {
                        IsActive = true,
                        Values = new List<int> { 0, 2 },
                    },
                    Text = new StringFilter
                    {
                        IsActive = true,
                        Begin = true,
                        Value = "FOO",
                    },
                },
                Ordering = new Ordering
                {
                    FieldName = nameof(TestData.Double),
                    Order = Order.Descending,
                },
                SearchAfter = new List<object>(),
            });

        Assert.That(filteredData.NumberOfResults, Is.EqualTo(expected.Count));
        Assert.That(JsonSerializer.Serialize(filteredData.Data), Is.EqualTo(JsonSerializer.Serialize(expected)));
    }

    [TestCase("foo", false, false, @"[""foobar"", ""Foobar"", ""foo"", ""foo bar"", ""FOO"", ""^foobar"", ""barfoo"", ""some Foo thing""]")]
    [TestCase("foo", true, false, @"[""foobar"", ""Foobar"", ""foo"", ""foo bar"", ""FOO""]")]
    [TestCase(" foo", true, false, @"["" foobar"", "" Foobar"", "" foo"", "" foo bar"", "" FOO""]")]
    [TestCase("bar", false, true, @"[""foobar"", ""Foobar"", ""fooBAR"", ""foo bar"", ""BAR""]")]
    [TestCase("foo", true, true, @"[""foo"", ""FOO"", ""Foo"", ""fOo""]")]
    [TestCase("^foo", false, false, @"[""^foobar"", ""^Foobar"", ""^foo"", ""^foo bar"", ""^FOO"", ""bar^foo"", ""some ^Foo thing""]")]
    [TestCase("bar$", false, false, @"[""foobar$"", ""Foobar$"", ""fooBAR$"", ""foo bar$ toto"", ""BAR$n"", ""bar$""]")]
    public void StringFilter_ShouldKeepValues(
        string filter,
        bool begin,
        bool end,
        string values)
    {
        var dataValues = JsonSerializer.Deserialize<List<string>>(values)!;
        var data = dataValues.Select(
            v => new TestData
            {
                Text = v
            })
            .ToList();

        var itemProvider = new FilteredItemProvider<TestData, TestDataFilter>(
            new InMemoryFilteredItemProvider<TestData>(data, TestData.FilterFactory));

        var filteredData = itemProvider.GetData(
            new Filter<TestDataFilter>
            {
                ValueFilters = new()
                {
                    Text = new StringFilter
                    {
                        Begin = begin,
                        End = end,
                        IsActive = true,
                        LooksForNullValues = false,
                        Value = filter,
                    }
                },
                SearchAfter = new List<object>(),
            });

        Assert.That(filteredData.NumberOfResults, Is.EqualTo(data.Count));
        Assert.That(JsonSerializer.Serialize(filteredData.Data), Is.EqualTo(JsonSerializer.Serialize(data)));
    }

    [TestCase("foo", false, false, @"[""oobar"", ""fo"", ""fo bar"", ""wyz""]")]
    [TestCase("foo", true, false, @"[""^foobar"", ""oobar"", ""fo"", ""fo bar"", ""wyz"", ""barfoo""]")]
    [TestCase(" foo", true, false, @"[""^foobar"", ""oobar"", ""fo"", ""fo bar"", ""wyz"", ""barfoo"", ""foobar"", ""Foobar"", ""foo"", ""foo bar"", ""FOO""]")]
    [TestCase("bar", false, true, @"[""foobar$"", ""fooba"", ""fo"", ""foo br"", ""wyz"", ""barfoo""]")]
    [TestCase("foo", true, true, @"[""foobar$"", ""^foo$"", "" foo"", ""foob""]")]
    [TestCase("^foo", false, false, @"[""foobar"", ""oobar"", ""fo"", ""fo bar"", ""wyz"", ""barfoo""]")]
    [TestCase("bar$", false, false, @"[""fooba"", ""fo"", ""foo br"", ""wyz"", ""barfoo""]")]
    public void StringFilter_ShouldFilterOutValues(
        string filter,
        bool begin,
        bool end,
        string values)
    {
        var dataValues = JsonSerializer.Deserialize<List<string>>(values)!;
        var data = dataValues.Select(
            v => new TestData
            {
                Text = v
            })
            .ToList();

        var itemProvider = new FilteredItemProvider<TestData, TestDataFilter>(
            new InMemoryFilteredItemProvider<TestData>(data, TestData.FilterFactory));

        var filteredData = itemProvider.GetData(
            new Filter<TestDataFilter>
            {
                ValueFilters = new()
                {
                    Text = new StringFilter
                    {
                        Begin = begin,
                        End = end,
                        IsActive = true,
                        LooksForNullValues = false,
                        Value = filter,
                    }
                },
                SearchAfter = new List<object>(),
            });

        Assert.That(filteredData.NumberOfResults, Is.EqualTo(0));
        Assert.That(JsonSerializer.Serialize(filteredData.Data), Is.EqualTo(JsonSerializer.Serialize(new List<TestData>())));
    }

    [TestCase("foo", false, @"[""foobar"", ""fo"", ""fo bar"", ""wyz""]")]
    [TestCase("foobar", true, @"[null, ""foobar"", ""fo"", ""fo bar"", ""wyz""]")]
    public void StringFilter_ShouldFilterNullWhenAsked(
        string filter,
        bool filterNull,
        string values)
    {
        var dataValues = JsonSerializer.Deserialize<List<string>>(values)!;
        var data = dataValues.Select(
            v => new TestData
            {
                Text = v
            })
            .ToList();

        var itemProvider = new FilteredItemProvider<TestData, TestDataFilter>(
            new InMemoryFilteredItemProvider<TestData>(data, TestData.FilterFactory));

        var filteredData = itemProvider.GetData(
            new Filter<TestDataFilter>
            {
                ValueFilters = new TestDataFilter
                {
                    Text = new StringFilter
                    {
                        IsActive = true,
                        LooksForNullValues = filterNull,
                        Value = filter,
                    }
                },
                SearchAfter = new List<object>(),
            });

        Assert.That(filteredData.NumberOfResults, Is.EqualTo(1));
        Assert.That(JsonSerializer.Serialize(filteredData.Data), Is.EqualTo(JsonSerializer.Serialize(data.Take(1))));
    }

    [TestCase(
        12, ComparisonType.Equal, null, null,
        @"[12, 12.0, 13]",
        @"[12, 12.0]")]
    [TestCase(
        12, ComparisonType.Greater, null, null,
        @"[12, 12.1, 13, 10, -12.0, 0, 1020]",
        @"[12.1, 13, 1020]")]
    [TestCase(
        12, ComparisonType.Lesser, null, null,
        @"[12, 12.1, 13, 10, -12.0, 0, 11.9, 1020]",
        @"[10, -12.0, 0, 11.9]")]
    [TestCase(
        12, ComparisonType.GreaterEqual, null, null,
        @"[12, 12.1, 13, 10, -12.0, 0, 1020]",
        @"[12, 12.1, 13, 1020]")]
    [TestCase(
        12, ComparisonType.LesserEqual, null, null,
        @"[12, 12.1, 13, 10, -12.0, 0, 11.9, 1020]",
        @"[12, 10, -12.0, 0, 11.9]")]
    [TestCase(
    -3.12, ComparisonType.Equal, null, null,
        @"[12, -3.120, -3.12]",
        @"[-3.120, -3.12]")]
    [TestCase(
        -3.12, ComparisonType.Greater, null, null,
        @"[-3.12, -3.11, -3.13, -4, 1, 0]",
        @"[-3.11, 1, 0]")]
    [TestCase(
        -3.12, ComparisonType.Lesser, null, null,
        @"[-3.12, -3.11, -3.13, -4, 1, 0]",
        @"[-3.13, -4]")]
    [TestCase(
        -3.12, ComparisonType.GreaterEqual, null, null,
        @"[-3.12, -3.11, -3.13, -4, 1, 0]",
        @"[-3.12, -3.11, 1, 0]")]
    [TestCase(
        -3.12, ComparisonType.LesserEqual, null, null,
        @"[-3.12, -3.11, -3.13, -4, 1, 0]",
        @"[-3.12, -3.13, -4]")]
    [TestCase(
        null, null, 12, ComparisonType.Equal,
        @"[12, 12.0, 13]",
        @"[12, 12.0]")]
    [TestCase(
        null, null, 12, ComparisonType.Greater,
        @"[12, 12.1, 13, 10, -12.0, 0, 1020]",
        @"[12.1, 13, 1020]")]
    [TestCase(
        null, null, 12, ComparisonType.Lesser,
        @"[12, 12.1, 13, 10, -12.0, 0, 11.9, 1020]",
        @"[10, -12.0, 0, 11.9]")]
    [TestCase(
        null, null, 12, ComparisonType.GreaterEqual,
        @"[12, 12.1, 13, 10, -12.0, 0, 1020]",
        @"[12, 12.1, 13, 1020]")]
    [TestCase(
        null, null, 12, ComparisonType.LesserEqual,
        @"[12, 12.1, 13, 10, -12.0, 0, 11.9, 1020]",
        @"[12, 10, -12.0, 0, 11.9]")]
    [TestCase(
        13, ComparisonType.Equal, 12, ComparisonType.Equal,
        @"[12, 13, 0]",
        @"[]")]
    [TestCase(
        13, ComparisonType.Equal, 12, ComparisonType.GreaterEqual,
        @"[12, 12.5, 13, 0]",
        @"[13]")]
    [TestCase(
        12, ComparisonType.LesserEqual, 10, ComparisonType.GreaterEqual,
        @"[9.9, 12, 12.1, 10, -1, 0, 11, 10.5, 14]",
        @"[12, 10, 11, 10.5]")]
    [TestCase(
        -3.12, ComparisonType.Greater, 5, ComparisonType.LesserEqual,
        @"[-3.12, -3.13, -3.11, 5, 0, 5.1, 1.1]",
        @"[-3.11, 5, 0, 1.1]")]
    [TestCase(
        -3.12, ComparisonType.GreaterEqual, 20, ComparisonType.Lesser,
        @"[-3.11, -3.13, 20, -3.12, 5, 0, 23, -3.15, 1.1, 19.9]",
        @"[-3.11, -3.12, 5, 0, 1.1, 19.9]")]
    [TestCase(
        -3.12, ComparisonType.LesserEqual, 5, ComparisonType.Greater,
        @"[-3.13, 20, 23, -3.15, -3.11, -3.12, 5, 0, 1.1, 19.9]",
        @"[]")]
    public void NumberFilter_ShouldWorkAsExpected(
        double? filterValue1,
        ComparisonType? comparisonType1,
        double? filterValue2,
        ComparisonType? comparisonType2,
        string input,
        string expected)
    {
        var dataValues = JsonSerializer.Deserialize<List<double>>(input)!;
        var data = dataValues.Select(v => new TestData { Double = v }) .ToList();
        var expectedValues = JsonSerializer.Deserialize<List<double>>(expected)!;
        var expectedData = expectedValues.Select(v => new TestData { Double = v }) .ToList();

        var itemProvider = new FilteredItemProvider<TestData, TestDataFilter>(
            new InMemoryFilteredItemProvider<TestData>(data, TestData.FilterFactory));

        var filteredData = itemProvider.GetData(
            new Filter<TestDataFilter>
            {
                ValueFilters = new TestDataFilter
                {
                    Double = new NumberFilter
                    {
                        IsActive = true,
                        Value1 = filterValue1,
                        Comparison1 = comparisonType1,
                        Value2 = filterValue2,
                        Comparison2 = comparisonType2,
                    }
                },
                SearchAfter = new List<object>(),
            });

        Assert.That(filteredData.NumberOfResults, Is.EqualTo(expectedData.Count));
        Assert.That(JsonSerializer.Serialize(filteredData.Data), Is.EqualTo(JsonSerializer.Serialize(expectedData)));
    }

    [TestCase(false, @"[12, 13, 14, 20000]", 4)]
    [TestCase(true, @"[12, 13, 14, 20000, null]", 1)]
    public void NumberFilter_ShouldFilterNullWhenAsked(bool filterNull, string values, int expectedNumberOfItem)
    {
        var dataValues = JsonSerializer.Deserialize<List<double?>>(values)!;
        var data = dataValues.Select(v => new TestData { Double = v }).ToList();

        var itemProvider = new FilteredItemProvider<TestData, TestDataFilter>(
            new InMemoryFilteredItemProvider<TestData>(data, TestData.FilterFactory));

        var filteredData = itemProvider.GetData(
            new Filter<TestDataFilter>
            {
                ValueFilters = new TestDataFilter
                {
                    Double = new NumberFilter
                    {
                        IsActive = true,
                        LooksForNullValues = filterNull,
                        Value1 = 12,
                        Comparison1 = ComparisonType.GreaterEqual
                    }
                },
                SearchAfter = new List<object>(),
            });

        Assert.That(filteredData.NumberOfResults, Is.EqualTo(expectedNumberOfItem));
        Assert.That(
            JsonSerializer.Serialize(filteredData.Data),
            Is.EqualTo(JsonSerializer.Serialize(data.TakeLast(expectedNumberOfItem))));
    }

    [TestCase(
        "2023-03-01T23:12:52", ComparisonType.Equal, null, null,
        @"[""2023-03-01T23:12:52"", ""2023-03-01T10:12:52"", ""2023-10-01T23:12:52""]",
        @"[""2023-03-01T23:12:52""]")]
    [TestCase(
        "2022-12-23T09:10:01", ComparisonType.Greater, null, null,
        @"[""2023-03-01T23:12:52"", ""2022-12-23T09:10:01"", ""2023-03-01T10:12:52"", ""2022-10-01T23:12:52""]",
        @"[""2023-03-01T23:12:52"", ""2023-03-01T10:12:52""]")]
    [TestCase(
        "2022-12-23T09:10:01", ComparisonType.Lesser, null, null,
        @"[""2023-03-01T23:12:52"", ""2022-12-23T09:10:01"", ""2023-03-01T10:12:52"", ""2022-10-01T23:12:52""]",
        @"[""2022-10-01T23:12:52""]")]
    [TestCase(
        "2022-12-23T09:10:01", ComparisonType.GreaterEqual, null, null,
        @"[""2023-03-01T23:12:52"", ""2022-12-23T09:10:01"", ""2023-03-01T10:12:52"", ""2022-10-01T23:12:52""]",
        @"[""2023-03-01T23:12:52"", ""2022-12-23T09:10:01"", ""2023-03-01T10:12:52""]")]
    [TestCase(
        "2022-12-23T09:10:01", ComparisonType.LesserEqual, null, null,
        @"[""2023-03-01T23:12:52"", ""2022-12-23T09:10:01"", ""2023-03-01T10:12:52"", ""2022-10-01T23:12:52""]",
        @"[""2022-12-23T09:10:01"", ""2022-10-01T23:12:52""]")]
    [TestCase(
        null, null, "2023-03-01T23:12:52", ComparisonType.Equal,
        @"[""2023-03-01T23:12:52"", ""2023-03-01T10:12:52"", ""2023-10-01T23:12:52""]",
        @"[""2023-03-01T23:12:52""]")]
    [TestCase(
        null, null, "2022-12-23T09:10:01", ComparisonType.Greater,
        @"[""2023-03-01T23:12:52"", ""2022-12-23T09:10:01"", ""2023-03-01T10:12:52"", ""2022-10-01T23:12:52""]",
        @"[""2023-03-01T23:12:52"", ""2023-03-01T10:12:52""]")]
    [TestCase(
        null, null, "2022-12-23T09:10:01", ComparisonType.Lesser,
        @"[""2023-03-01T23:12:52"", ""2022-12-23T09:10:01"", ""2023-03-01T10:12:52"", ""2022-10-01T23:12:52""]",
        @"[""2022-10-01T23:12:52""]")]
    [TestCase(
        null, null, "2022-12-23T09:10:01", ComparisonType.GreaterEqual,
        @"[""2023-03-01T23:12:52"", ""2022-12-23T09:10:01"", ""2023-03-01T10:12:52"", ""2022-10-01T23:12:52""]",
        @"[""2023-03-01T23:12:52"", ""2022-12-23T09:10:01"", ""2023-03-01T10:12:52""]")]
    [TestCase(
        null, null, "2022-12-23T09:10:01", ComparisonType.LesserEqual,
        @"[""2023-03-01T23:12:52"", ""2022-12-23T09:10:01"", ""2023-03-01T10:12:52"", ""2022-10-01T23:12:52""]",
        @"[""2022-12-23T09:10:01"", ""2022-10-01T23:12:52""]")]
    [TestCase(
        "2021-03-01T23:12:52", ComparisonType.Equal, "2023-03-01T23:12:52", ComparisonType.Equal,
        @"[""2023-03-01T23:12:52"", ""2023-03-01T10:12:52"", ""2023-10-01T23:12:52""]",
        @"[]")]
    [TestCase(
        "2023-03-01T23:12:52", ComparisonType.Equal, "2023-03-01T00:00:00", ComparisonType.GreaterEqual,
        @"[""2023-03-01T23:12:52"", ""2023-03-01T10:12:52"", ""2023-10-01T23:12:52""]",
        @"[""2023-03-01T23:12:52""]")]
    [TestCase(
        "2023-03-01T23:12:52", ComparisonType.LesserEqual, "2022-10-01T23:12:52", ComparisonType.GreaterEqual,
        @"[""2023-03-01T23:12:52"", ""2022-12-23T09:10:01"", ""2023-03-01T10:12:52"", ""2022-10-01T23:12:52""]",
        @"[""2023-03-01T23:12:52"", ""2022-12-23T09:10:01"", ""2023-03-01T10:12:52"", ""2022-10-01T23:12:52""]")]
    [TestCase(
        "2022-10-01T23:12:52", ComparisonType.Greater, "2023-03-01T10:12:52", ComparisonType.LesserEqual,
        @"[""2023-03-01T23:12:52"", ""2022-12-23T09:10:01"", ""2023-03-01T10:12:52"", ""2022-10-01T23:12:52""]",
        @"[""2022-12-23T09:10:01"", ""2023-03-01T10:12:52""]")]
    [TestCase(
        "2022-12-23T09:10:01", ComparisonType.GreaterEqual, "2023-03-01T23:12:52", ComparisonType.Lesser,
        @"[""2023-03-01T23:12:52"", ""2022-12-23T09:10:01"", ""2023-03-01T10:12:52"", ""2022-10-01T23:12:52""]",
        @"[""2022-12-23T09:10:01"", ""2023-03-01T10:12:52""]")]
    [TestCase(
        "2022-12-23T09:10:01", ComparisonType.LesserEqual, "2023-01-01T01:10:01", ComparisonType.Greater,
        @"[""2023-03-01T23:12:52"", ""2022-12-23T09:10:01"", ""2023-03-01T10:12:52"", ""2022-10-01T23:12:52""]",
        @"[]")]
    public void DateTimeFilter_ShouldWorkAsExpected(
        string? filterValue1,
        ComparisonType? comparisonType1,
        string? filterValue2,
        ComparisonType? comparisonType2,
        string input,
        string expected)
    {
        var dataValues = JsonSerializer.Deserialize<List<DateTime>>(input)!;
        var data = dataValues.Select(v => new TestData { Date = v }) .ToList();
        var expectedValues = JsonSerializer.Deserialize<List<DateTime>>(expected)!;
        var expectedData = expectedValues.Select(v => new TestData { Date = v }) .ToList();

        var itemProvider = new FilteredItemProvider<TestData, TestDataFilter>(
            new InMemoryFilteredItemProvider<TestData>(data, TestData.FilterFactory));

        var filteredData = itemProvider.GetData(
            new Filter<TestDataFilter>
            {
                ValueFilters = new TestDataFilter
                {
                    Date = new DateTimeFilter()
                    {
                        IsActive = true,
                        Value1 = filterValue1 == null ? null : DateTime.Parse(filterValue1),
                        Comparison1 = comparisonType1,
                        Value2 = filterValue2 == null ? null : DateTime.Parse(filterValue2),
                        Comparison2 = comparisonType2,
                    }
                },
                SearchAfter = new List<object>(),
            });

        Assert.That(filteredData.NumberOfResults, Is.EqualTo(expectedData.Count));
        Assert.That(JsonSerializer.Serialize(filteredData.Data), Is.EqualTo(JsonSerializer.Serialize(expectedData)));
    }

    [TestCase(false, @"[""2023-03-01T23:12:52"", ""2022-12-23T09:10:01"", ""2023-03-01T10:12:52"", ""2022-10-01T23:12:52""]", 4)]
    [TestCase(true, @"[""2023-03-01T23:12:52"", ""2022-12-23T09:10:01"", ""2023-03-01T10:12:52"", ""2022-10-01T23:12:52"", null]", 1)]
    public void DateTimeFilter_ShouldFilterNullWhenAsked(bool filterNull, string values, int expectedNumberOfItem)
    {
        var dataValues = JsonSerializer.Deserialize<List<DateTime?>>(values)!;
        var data = dataValues.Select(v => new TestData { Date = v }).ToList();

        var itemProvider = new FilteredItemProvider<TestData, TestDataFilter>(
            new InMemoryFilteredItemProvider<TestData>(data, TestData.FilterFactory));

        var filteredData = itemProvider.GetData(
            new Filter<TestDataFilter>
            {
                ValueFilters = new TestDataFilter
                {
                    Date = new DateTimeFilter()
                    {
                        IsActive = true,
                        LooksForNullValues = filterNull,
                        Value1 = new DateTime(2020, 1, 1),
                        Comparison1 = ComparisonType.GreaterEqual
                    }
                },
                SearchAfter = new List<object>(),
            });

        Assert.That(filteredData.NumberOfResults, Is.EqualTo(expectedNumberOfItem));
        Assert.That(
            JsonSerializer.Serialize(filteredData.Data),
            Is.EqualTo(JsonSerializer.Serialize(data.TakeLast(expectedNumberOfItem))));
    }

    [TestCase(
        new []{ TestEnum.First, TestEnum.Second, TestEnum.Third },
        new []{ TestEnum.First },
        new []{ TestEnum.First })]
    [TestCase(
        new []{ TestEnum.First, TestEnum.Second, TestEnum.Third, TestEnum.First },
        new []{ TestEnum.First, TestEnum.Third },
        new []{ TestEnum.First, TestEnum.Third, TestEnum.First })]
    public void SelectFilter_ShouldWorkAsExpected(TestEnum[] inputData, TestEnum[] filterValue, TestEnum[] expectedData)
    {
        var data = inputData.Select(v => new TestData { Enum = v }).ToList();
        var expected = expectedData.Select(v => new TestData { Enum = v }).ToList();

        var itemProvider = new FilteredItemProvider<TestData, TestDataFilter>(
            new InMemoryFilteredItemProvider<TestData>(data, TestData.FilterFactory));

        var filteredData = itemProvider.GetData(
            new Filter<TestDataFilter>
            {
                ValueFilters = new TestDataFilter
                {
                    Enum = new SelectFilter()
                    {
                        IsActive = true,
                        Values = filterValue.Select(v => (int)v).ToList(),
                    }
                },
                SearchAfter = new List<object>(),
            });

        Assert.That(filteredData.NumberOfResults, Is.EqualTo(expected.Count));
        Assert.That(JsonSerializer.Serialize(filteredData.Data), Is.EqualTo(JsonSerializer.Serialize(expected)));
    }
}
