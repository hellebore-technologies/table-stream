﻿using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using HelleboreTech.TableStream.Core;
using NUnit.Framework;

namespace HelleboreTech.TableStream.InMemory.Test;

public class GetDataTest
{
    [TestCase(
        @"[12, 7, 12, 100, 7, 134, 999, 21]",
        @"[7, 12, 100, 134]", 
        Order.Ascending)]
    [TestCase(
        @"[250, 11, 11, 65, -4, 23, 1230]",
        @"[1230, 250, 23, 11]",
        Order.Descending)]
    public void InMemoryProvider_ShouldGetDataByIds(string rawFilter, string rawExpectedStr, Order order)
    {
        var rawData = new List<int> { 12, 134, 7, 250, 11, 100, 23, 1_230, 3_230 };
        var filter = JsonSerializer.Deserialize<List<int>>(rawFilter)!;
        var rawExpected = JsonSerializer.Deserialize<List<int>>(rawExpectedStr)!;
        var data = rawData.Select(v => new TestData { Id = v }).ToList();
        var filterData = filter.Select(v => new TestData { Id = v }).ToList();
        var expected = rawExpected.Select(v => new TestData { Id = v }).ToList();

        var itemProvider = new FilteredItemProvider<TestData, TestDataFilter>(
            new InMemoryFilteredItemProvider<TestData>(data, TestData.FilterFactory));

        var filteredData = itemProvider.GetDataByIds(
            filterData,
            new Ordering
            {
                FieldName = nameof(TestData.Id),
                Order = order,
            });
        
        Assert.That(filteredData.Count, Is.EqualTo(expected.Count));
        Assert.That(JsonSerializer.Serialize(filteredData), Is.EqualTo(JsonSerializer.Serialize(expected)));
    }
}
