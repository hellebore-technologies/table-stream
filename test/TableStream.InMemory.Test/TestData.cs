﻿using System;
using System.Collections.Generic;
using System.Linq;
using HelleboreTech.TableStream.Core;

namespace HelleboreTech.TableStream.InMemory.Test;

public enum TestEnum
{
    First,
    Second,
    Third,
}

public class TestData : FilteredItem
{
    public int Id { get; set; }
    public double? Double { get; set; }
    public string? Text { get; set; }
    public DateTime? Date { get; set; }
    public TestEnum? Enum { get; set; }

    public static Func<TestData, bool> FilterFactory(IEnumerable<TestData> data)
    {
        var ids = data.Select(d => d.Id).ToHashSet();
        bool Filter(TestData d) => ids.Contains(d.Id);
        return Filter;
    }
}

public class TestDataFilter : Filter<TestData>
{
    public NumberFilter? Id { get; set; }
    public NumberFilter? Double { get; set; }
    public StringFilter? Text { get; set; }
    public DateTimeFilter? Date { get; set; }
    public SelectFilter? Enum { get; set; }
}
