import { ComponentCSSWithTheme, Theme } from './types/styles';
import { tableHeaderCSS } from './components/TableHeader';
import { columnHeaderCSS } from './components/ColumnHeader';
import { columnHeaderInputSelectCSS } from './components/ColumnHeaderInputSelect';
import { columnHeaderInputBasicCSS } from './components/ColumnHeaderInputBasic';
import { columnHeaderSortIconCSS } from './components/ColumnHeaderSortIcon';
import { columnHeaderTitleCSS } from './components/ColumnHeaderTitle';
import { tableBodyCSS } from './components/TableBody';
import { rowCSS } from './components/Row';
import { rowCellCSS } from './components/RowCell';
import { rowCellContentCSS } from './components/RowCellContent';
import { scrollBannerCSS } from './components/ScrollBanner';

export const defaultTheme : Theme = {
  base: {
    background: 'white',
    font: 'black',
  },
  border: '#c6c6c6',
  highlight: {
    background: '#ededed',
    font: 'black',
  },
  hover: {
    background: '#dbdbdbc0',
    font: 'black',
  },
  error: 'red',
};

export const defaultComponentsCSS : ComponentCSSWithTheme<Record<string, unknown>> = {
  table: () => ({
    label: 'table',
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
    height: '100%',
    overflow: 'hidden',

    '& *': {
      boxSizing: 'border-box',
    },
  }),
  tableHeader: tableHeaderCSS,
  columnHeader: columnHeaderCSS,
  columnHeaderInputSelect: columnHeaderInputSelectCSS,
  columnHeaderInputBasic: columnHeaderInputBasicCSS,
  columnHeaderSortIcon: columnHeaderSortIconCSS,
  columnHeaderTitle: columnHeaderTitleCSS,
  tableBody: tableBodyCSS,
  row: rowCSS,
  rowCell: rowCellCSS,
  rowCellContent: rowCellContentCSS,
  scrollBanner: scrollBannerCSS,
};
