import { CSSObject } from '@emotion/react';

import {
  Theme,
  ComponentsCSS,
  ComponentCSSWithTheme,
  CSSWithTheme,
  PartialTheme,
} from './types/styles';
import { Components } from './types/component';

/**
 * Merge default styles with custom ones.
 * @private
 * @param theme Custom color scheme.
 * @param defaultCSS Default styles for the components.
 * @param customCSS Custom styles that will override the default ones.
 * @returns Object containing the styles that will be applied to the table's components.
 */
function mergeComponentCSS(
  theme: Theme,
  defaultCSS: CSSWithTheme,
  customCSS?: CSSWithTheme,
) : CSSObject {
  if (customCSS != null) {
    return { ...defaultCSS(theme), ...customCSS(theme) };
  }

  return defaultCSS(theme);
}

/**
 * Merge theme with custom theme before applying it to the merge result of the default styles
 * and custom ones.
 * @param defaultTheme Default color scheme.
 * @param defaultComponentsCSS Default styles for table's components.
 * @param customTheme Custom color scheme.
 * @param customComponentsCSS Custom styles for table's components.
 * @returns Object containing merge theme and components' styles.
 */
export function buildCSSAndTheme<T extends Record<string, unknown>>(
  defaultTheme: Theme,
  defaultComponentsCSS: ComponentCSSWithTheme<T>,
  customTheme: PartialTheme,
  customComponentsCSS: Partial<ComponentCSSWithTheme<T>>,
) : { theme: Theme; componentsCSS: ComponentsCSS<T> } {
  const theme: Theme = {
    base: { ...defaultTheme.base, ...customTheme.base },
    highlight: { ...defaultTheme.highlight, ...customTheme.highlight },
    hover: { ...defaultTheme.hover, ...customTheme.hover },
    border: customTheme.border || defaultTheme.border,
    error: customTheme.error || defaultTheme.error,
  };
  const componentsCSS = Object
    .keys(defaultComponentsCSS)
    .map(
      (k: keyof ComponentsCSS<T>) => ({
        key: k,
        value: mergeComponentCSS(theme, defaultComponentsCSS[k], customComponentsCSS[k]),
      }),
    ).reduce((acc, curr) => ({ ...acc, [curr.key]: curr.value }), {}) as ComponentsCSS<T>;

  return { theme, componentsCSS };
}

/**
 * Merge default components and custom components.
 * @param defaultComponents Default components.
 * @param customComponents Custom components that will override the default ones.
 * @returns Object containing the components that will be used to build the table.
 */
export function buildComponents<T extends Record<string, unknown>>(
  defaultComponents: Components<T>,
  customComponents: Partial<Components<T>>,
): Components<T> {
  return { ...defaultComponents, ...customComponents };
}
