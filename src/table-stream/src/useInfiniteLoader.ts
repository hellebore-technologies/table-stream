import { useState, useRef } from 'react';
import { FixedSizeList } from 'react-window';

import { ListRange } from './types/component';
import { DataResult, Filter } from './types/filter';

/**
 * Hook that will automatically fetch new data when the user is scrolling to the end of the table.
 * It helps implementing infinite loading behavior.
 * @param fetchData Function called to get new data.
 * @param threshold Number of data not displayed under which new data will be fetched.
 * @returns Array containing:
 *  - array of data
 *  - number of match
 *  - reference to the list of row
 *  - onFilterChange function
 *  - onItemRendered function
 */
export default function useInfiniteLoader<T extends Record<string, unknown>>(
  fetchData: (filter: Filter) => Promise<DataResult<T>>,
  threshold = 100,
) : [
    data: T[],
    numberOfData: number,
    refToList: React.MutableRefObject<FixedSizeList>,
    onFilterChange: (newFilter: Filter) => void,
    onItemRendered: (props: ListRange) => void,
  ] {
  const [data, setData] = useState<T[]>([]);
  const [numberOfResults, setNumberOfResults] = useState(0);
  const [searchAfter, setSearchAfter] = useState<(string | number)[]>(null);
  const [filter, setFilter] = useState<Filter>({
    valueFilters: {},
    ordering: null,
  });
  const listRef = useRef<FixedSizeList>(null);

  const onChangeFilter = (newFilter: Filter) => {
    setFilter(newFilter);
    if (listRef.current != null) {
      listRef.current.scrollToItem(0);
    }
    fetchData(newFilter)
      .then((dataResult: DataResult<T>) => {
        setData(dataResult.data);
        setSearchAfter(dataResult.nextSearchAfter);
        setNumberOfResults(dataResult.numberOfResults);
      });
  };

  const onItemRendered = ({ overscanStopIndex }: ListRange) => {
    if (filter.ordering != null) {
      if (
        data.length - overscanStopIndex < threshold
        && data.length < numberOfResults
      ) {
        const modifiedFilter: Filter = {
          ...filter,
          searchAfter,
        };

        fetchData(modifiedFilter)
          .then((dataResult: DataResult<T>) => {
            setData([...data, ...dataResult.data]);
            setSearchAfter(dataResult.nextSearchAfter);
            setNumberOfResults(dataResult.numberOfResults);
          });
      }
    }
  };

  return [
    data,
    numberOfResults,
    listRef,
    onChangeFilter,
    onItemRendered,
  ];
}
