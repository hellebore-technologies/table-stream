/** @jsx jsx */
import { jsx } from '@emotion/react';
import {
  ReactElement,
  useState,
  useMemo,
  useLayoutEffect,
  useRef,
  useEffect,
} from 'react';
import { FixedSizeList as List } from 'react-window';
import AutoSizer from 'react-virtualized-auto-sizer';
import { Options } from 'react-select';

import { Column, OptionType } from './types/column';
import { ValueFilter, ValueFilters } from './types/valueFilter';
import { ComponentCSSWithTheme, PartialTheme } from './types/styles';
import {
  Order,
  Ordering,
  Filter,
  DataResult,
} from './types/filter';
import { Components, ListRange } from './types/component';

import TableHeader from './components/TableHeader';
import ColumnHeader from './components/ColumnHeader';
import ColumnHeaderSortIcon from './components/ColumnHeaderSortIcon';
import ColumnHeaderTitle from './components/ColumnHeaderTitle';
import Row from './components/Row';
import RowCell from './components/RowCell';
import RowCellContent from './components/RowCellContent';
import ColumnHeaderInputBasic,
{ parseValue as parseInputValue } from './components/ColumnHeaderInputBasic';
import ColumnHeaderInputSelect,
{ parseValues as parseSelectValues } from './components/ColumnHeaderInputSelect';
import TableBody from './components/TableBody';
import ScrollBanner from './components/ScrollBanner';

import { ColumnType } from './types/enum';
import { buildCSSAndTheme, buildComponents } from './utils';
import { defaultComponentsCSS, defaultTheme } from './styles';
import { isValid } from './filter';
import sortData from './sort';
import useDebounce from './useDebounce';

export const defaultComponents: Components<Record<string, unknown>> = {
  tableHeader: TableHeader,
  columnHeader: ColumnHeader,
  columnHeaderInputBasic: ColumnHeaderInputBasic,
  columnHeaderInputSelect: ColumnHeaderInputSelect,
  columnHeaderSortIcon: ColumnHeaderSortIcon,
  columnHeaderTitle: ColumnHeaderTitle,
  tableBody: TableBody,
  row: Row,
  rowCell: RowCell,
  rowCellContent: RowCellContent,
  scrollBanner: ScrollBanner,
};

interface TableProps<T extends Record<string, unknown>> {
  /** List of data. */
  data: T[];
  /** List of column definitions. */
  columns: Column<T>[];
  /** Ordering of the table. */
  ordering: Ordering;
  /** Default values for the filters. */
  filter?: Map<string, string | Options<OptionType>>;
  /** Height of a row in pixel. */
  rowHeight?: number;
  /**
   * Number of row to render outside of the visible table.
   * Useful to avoid displaying blank space when a user is scrolling.
   */
  overscanCount?: number;
  /** Number of ms to wait before processing changes in filters. */
  filterDebounceDelay?: number;
  /** Whether the user is scrolling in the table. */
  isScrolling?: boolean;
  /** Custom color scheme. */
  theme?: PartialTheme;
  /** Override default styles for the components to build used to build the table. */
  componentsCSS?: Partial<ComponentCSSWithTheme<T>>;
  /** Override default components used to build the table. */
  components?: Partial<Components<T>>;
  /** Reference to the list of rows displayed by the table. */
  listRef?: React.MutableRefObject<List>;
  /** Get the unique identifier of a given row. */
  rowKeyGetter?: (index: number, data: T[]) => string | number;
  /** Handle user click on a row. */
  onRowClick?: (data: T) => void;
  /** Handle user double click on a row. */
  onRowDoubleClick?: (data: T) => void;
  /** Called when data are filtered. */
  onDataFiltered?: (result: DataResult<T>) => void;
  /** Called the values of the filters change. */
  onChangeFilter?: (filter: Filter) => void;
  /** Called when rows rendered by the table change. */
  onItemRendered?: (props: ListRange) => void;
}

export default function Table<T extends Record<string, unknown>>({
  data,
  columns,
  ordering: defaultOrdering,
  filter: defaultFilter,
  rowHeight,
  overscanCount,
  filterDebounceDelay,
  isScrolling,
  listRef,
  theme: customTheme,
  componentsCSS: customComponentsCSS,
  components: customComponents,
  rowKeyGetter,
  onRowClick,
  onRowDoubleClick,
  onDataFiltered,
  onItemRendered,
  onChangeFilter,
}: TableProps<T>) : ReactElement {
  const [scrollbarWidth, setScrollbarWidth] = useState(0);
  const [ordering, setOrdering] = useState<Ordering>(defaultOrdering);
  const [valueFilters, setValueFilters] = useState<ValueFilters>(
    defaultFilter == null
      ? {}
      : Array.from(defaultFilter.entries()).reduce((o, f) => {
        const column = columns.find((c) => c.id === f[0]);
        let filter = null;
        if (column.type === ColumnType.Select) {
          filter = parseSelectValues(f[1] as Options<OptionType>);
        } else if (
          column.type === ColumnType.String
          || column.type === ColumnType.DateTime
          || column.type === ColumnType.Number
        ) {
          filter = parseInputValue(f[1] as string, column);
        }

        if (filter != null) {
          // eslint-disable-next-line no-param-reassign
          o[column.id] = filter;
        }

        return o;
      }, {} as ValueFilters),
  );
  const [filteredData, setFilteredData] = useState(data);
  const listOuterRef = useRef(null);
  const style = useMemo(
    () => buildCSSAndTheme(defaultTheme, defaultComponentsCSS, customTheme, customComponentsCSS),
    [customTheme, customComponentsCSS],
  );
  const components = useMemo(
    () => buildComponents(defaultComponents, customComponents),
    [customComponents],
  );

  useLayoutEffect((): void => {
    if (listOuterRef.current != null) {
      const node = listOuterRef.current;
      if (node != null) {
        const clientWidth = node.clientWidth || 0;
        const offsetWidth = node.offsetWidth || 0;
        const newScrollbarWidth = offsetWidth - clientWidth;
        if (newScrollbarWidth !== scrollbarWidth) {
          setScrollbarWidth(newScrollbarWidth);
        }
      }
    }
  });

  useEffect(
    () => {
      if (ordering != null && !columns.map((c) => c.id).includes(ordering.fieldName)) {
        // remove ordering when column used to order is removed
        setOrdering(null);
      }
      const newFilter: ValueFilters = {};
      columns.forEach((c) => {
        if (valueFilters[c.id] != null) {
          newFilter[c.id] = valueFilters[c.id];
        }
      });
      setValueFilters(newFilter);
    },
    [columns],
  );

  const debouncedRealFilters = useDebounce(valueFilters, filterDebounceDelay);
  useEffect(
    () => {
      if (onChangeFilter == null) {
        const newData = data.filter((d) => isValid(d, columns, debouncedRealFilters));
        const sortedData = sortData(newData, columns, ordering);
        setFilteredData(sortedData);
        if (onDataFiltered != null) {
          onDataFiltered({ data: sortedData, numberOfResults: sortedData.length });
        }
      }
    },
    [data, debouncedRealFilters, ordering, columns],
  );

  useEffect(
    () => {
      if (onChangeFilter != null) {
        onChangeFilter({
          ordering,
          valueFilters: debouncedRealFilters,
        });
      }
    },
    [debouncedRealFilters, ordering],
  );

  useEffect(
    () => {
      if (onChangeFilter != null) {
        setFilteredData(data);
      }
    },
    [data],
  );

  const handleFilterChange = (
    filter: ValueFilter,
    columnId: string,
  ) => {
    const newRealFilters = { ...valueFilters };
    newRealFilters[columnId] = filter;
    setValueFilters(newRealFilters);
  };

  const handleSortChange = (columnId: string) => {
    if (ordering == null || ordering.fieldName !== columnId) {
      setOrdering({ fieldName: columnId, order: Order.Descending });
    } else {
      switch (ordering.order) {
        case Order.Ascending:
          setOrdering({ fieldName: columnId, order: Order.Descending });
          break;
        case Order.Descending:
          setOrdering({ fieldName: columnId, order: Order.Ascending });
          break;
        default:
          setOrdering(null);
          break;
      }
    }
  };

  return (
    <div
      css={style.componentsCSS.table}
    >
      <components.tableHeader
        cssStyle={style.componentsCSS.tableHeader}
        scrollBarSize={scrollbarWidth}
      >
        {
          columns.map((column) => (
            <components.columnHeader
              key={column.id}
              column={column}
              cssStyle={style.componentsCSS.columnHeader}
              handleSortChange={handleSortChange}
            >
              {
                column.type !== ColumnType.None && column.isSortable && (
                  <components.columnHeaderSortIcon
                    cssStyle={style.componentsCSS.columnHeaderSortIcon}
                    theme={style.theme}
                    isActive={ordering != null && column.id === ordering.fieldName}
                    order={(ordering == null) ? null : ordering.order}
                  />
                )
              }
              <components.columnHeaderTitle
                cssStyle={style.componentsCSS.columnHeaderTitle}
                title={column.displayName}
              />
              {
                column.type !== ColumnType.None && column.hasFilter && (
                  column.type === ColumnType.Select
                    ? (
                      <components.columnHeaderInputSelect
                        defaultValues={
                          (defaultFilter?.get(column.id) == null)
                            ? null
                            : (defaultFilter.get(column.id) as Options<OptionType>)
                        }
                        column={column}
                        cssStyle={style.componentsCSS.columnHeaderInputSelect}
                        theme={style.theme}
                        onChangeColumnFilter={handleFilterChange}
                      />
                    )
                    : (
                      <components.columnHeaderInputBasic
                        defaultValue={
                          defaultFilter?.get(column.id) == null
                            ? null
                            : (defaultFilter.get(column.id) as string)
                        }
                        column={column}
                        cssStyle={style.componentsCSS.columnHeaderInputBasic}
                        theme={style.theme}
                        onChangeColumnFilter={handleFilterChange}
                      />
                    )
                )
              }
            </components.columnHeader>
          ))
        }
      </components.tableHeader>
      <components.tableBody
        cssStyle={style.componentsCSS.tableBody}
      >
        <components.scrollBanner
          isScrolling={isScrolling}
          rowHeight={rowHeight}
          scrollBarSize={scrollbarWidth}
          scrollTop={() => listRef?.current?.scrollTo(0)}
          theme={style.theme}
          cssStyle={style.componentsCSS.scrollBanner}
        />
        <AutoSizer>
          {({ height, width }): ReactElement => (
            <List
              outerRef={listOuterRef}
              ref={listRef}
              height={height}
              itemKey={rowKeyGetter}
              width={width}
              itemData={filteredData}
              itemCount={filteredData.length}
              itemSize={rowHeight}
              overscanCount={overscanCount}
              onItemsRendered={onItemRendered}
            >
              {({ data: listData, index, style: listStyle }) => (
                <components.row
                  data={listData}
                  index={index}
                  style={listStyle}
                  theme={style.theme}
                  cssStyle={style.componentsCSS.row}
                  onClick={onRowClick}
                  onDoubleClick={onRowDoubleClick}
                >
                  {
                    columns.map((column) => (
                      <components.rowCell
                        key={column.id}
                        column={column}
                        cssStyle={style.componentsCSS.rowCell}
                      >
                        {
                          column.cellRenderer != null || column.type === ColumnType.None
                            ? column.cellRenderer(listData[index])
                            : (
                              <components.rowCellContent
                                data={listData[index]}
                                column={column}
                                cssStyle={style.componentsCSS.rowCellContent}
                              />
                            )
                        }
                      </components.rowCell>
                    ))
                  }
                </components.row>
              )}
            </List>
          )}
        </AutoSizer>
      </components.tableBody>
    </div>
  );
}

Table.defaultProps = {
  rowHeight: 25,
  overscanCount: 1,
  filterDebounceDelay: 300,
  filter: null,
  isScrolling: false,
  listRef: null,
  theme: {},
  componentsCSS: {},
  components: {},
  rowKeyGetter: (index: number) => index,
  onRowClick: null,
  onRowDoubleClick: null,
  onItemRendered: null,
  onChangeFilter: null,
  onDataFiltered: null,
};
