import {
  StringFilter,
  NumberFilter,
  SelectFilter,
  DateTimeFilter,
  ValueFilter,
} from '../types/valueFilter';
import { ColumnType, ComparisonType } from '../types/enum';

/**
 * Parse input value into Filter object for string filter.
 * @private
 * @param value String to parse.
 * @returns Object holding the current value of the filter.
 */
const parseStringFilter = (value: string) : StringFilter => {
  if (value == null || value === '') {
    return {
      kind: ColumnType.String,
      isActive: false,
      looksForNullValues: false,
      value: '',
      begin: false,
      end: false,
    };
  }

  if (value === '//') {
    return {
      kind: ColumnType.String,
      isActive: true,
      looksForNullValues: true,
      value: '',
      begin: false,
      end: false,
    };
  }

  const filter: StringFilter = {
    kind: ColumnType.String,
    isActive: true,
    looksForNullValues: false,
    value: value.toLowerCase(),
    begin: false,
    end: false,
  };

  // handle begin anchor detection
  const prefix = filter.value.slice(0, 2);
  if (prefix === '/^') {
    filter.value = filter.value.slice(1);
    filter.begin = false;
  } else if (prefix.startsWith('^')) {
    filter.value = filter.value.slice(1);
    filter.begin = true;
  }

  // handle end anchor detection
  const suffix = filter.value.slice(-2);
  if (suffix === '/$') {
    filter.value = `${filter.value.slice(0, filter.value.length - 2)}$`;
    filter.end = false;
  } else if (suffix.endsWith('$')) {
    filter.value = filter.value.slice(0, filter.value.length - 1);
    filter.end = true;
  }

  return filter;
};

/**
 * Helper function that parses the comparison characters: <, <=, =, =>, and >.
 * It also removes the comparison characters from the input.
 * @private
 * @param value String to parse.
 * @returns Object containing the value, without the comparison character, and the comparison
 * type.
 */
const parseComparisonInFilter = (value: string) : {
  value: string,
  comparisonType?: ComparisonType,
} => {
  if (value == null || value === '') {
    return { value };
  }

  const val = value.trim();
  let offset = 0;
  let type = ComparisonType.Equal;
  // handle first comparison character
  switch (val[0]) {
    case '<':
      type = ComparisonType.Lesser;
      offset += 1;
      break;
    case '>':
      type = ComparisonType.Greater;
      offset += 1;
      break;
    case '=':
      type = ComparisonType.Equal;
      offset += 1;
      break;
    default:
      break;
  }
  // handle second comparison character
  if (val.length > 1) {
    if (val[1] === '=') {
      if (type === ComparisonType.Lesser) {
        type = ComparisonType.LesserEqual;
        offset += 1;
      } else if (type === ComparisonType.Greater) {
        type = ComparisonType.GreaterEqual;
        offset += 1;
      }
    }
  }

  return {
    value: val.substr(offset, val.length - offset).trim(),
    comparisonType: type,
  };
};

/**
 * Helper function that parse a number with potential comparison characters.
 * @private
 * @param value String to parse.
 * @returns Object containing the number parsed and the comparison type.
 */
function parseNumberFilterValue(value: string) : {
  value?: number,
  comparisonType?: ComparisonType,
} {
  const parsedComparison = parseComparisonInFilter(value);
  const { comparisonType } = parsedComparison;
  let { value: strValue } = parsedComparison;
  let coefficient = 1;
  if (strValue != null && strValue.length > 1) {
    const lastCharPos = strValue.length - 1;
    switch (strValue[lastCharPos]) {
      case 'k':
      case 'K':
        coefficient = 1000;
        strValue = strValue.substring(0, lastCharPos);
        break;
      case 'm':
      case 'M':
        coefficient = 1000000;
        strValue = strValue.substring(0, lastCharPos);
        break;
      default:
        break;
    }
  }

  if (/^[0-9.-]+$/.test(strValue)) {
    const number = parseFloat(strValue);
    if (Number.isNaN(number)) {
      return {};
    }
    return {
      value: number * coefficient,
      comparisonType,
    };
  }

  return {};
}

/**
 * Parse input value into Filter object for number filter.
 * @private
 * @param value String to parse.
 * @returns Object holding the current value of the filter.
 */
function parseNumberFilter(value: string) : NumberFilter {
  if (value == null || value === '') {
    return {
      kind: ColumnType.Number,
      isActive: false,
      looksForNullValues: false,
    };
  }

  if (value === '//') {
    return {
      kind: ColumnType.Number,
      isActive: true,
      looksForNullValues: true,
    };
  }

  let res: NumberFilter = {
    kind: ColumnType.Number,
    isActive: false,
    looksForNullValues: false,
  };
  const substring = value.split('&');
  if (substring.length === 2) {
    const { value: value1, comparisonType: comparison1 } = parseNumberFilterValue(substring[0]);
    const { value: value2, comparisonType: comparison2 } = parseNumberFilterValue(substring[1]);
    res = {
      kind: ColumnType.Number,
      isActive: value1 != null || value2 != null,
      looksForNullValues: false,
      value1,
      comparison1,
      value2,
      comparison2,
    };
  } else if (substring.length === 1) {
    const { value: value1, comparisonType: comparison1 } = parseNumberFilterValue(substring[0]);
    res = {
      kind: ColumnType.Number,
      isActive: value1 != null,
      looksForNullValues: false,
      value1,
      comparison1,
    };
  }
  return res;
}

/**
 * Parse input value into Filter object for select filter.
 * @private
 * @param value String to parse.
 * @returns Object holding the current value of the filter.
 */
function parseSelectFilter(value: string) : SelectFilter {
  if (value == null || value === '') {
    return {
      kind: ColumnType.Select,
      isActive: false,
      values: [],
    };
  }

  const values = value.split(',')
    .map((s) => parseInt(s, 10))
    .filter((n) => !Number.isNaN(n));

  return {
    kind: ColumnType.Select,
    isActive: values.length > 0,
    values,
  };
}

/**
 * Build date string with format YYYY-MM-DD.
 * @private
 * @param year
 * @param month
 * @param day
 * @returns Date string.
 */
export function createDateString(
  year: number,
  month: number,
  day: number,
) : string {
  const yearStr = year.toString();
  const monthStr = month.toString().padStart(2, '0');
  const dayStr = day.toString().padStart(2, '0');
  return `${yearStr}-${monthStr}-${dayStr}`;
}

/**
 * Build time string with format HH:MM:SS.
 * @private
 * @param hour
 * @param minute
 * @param second
 * @returns Time string.
 */
function createTimeString(
  hour: number,
  minute: number,
  second: number,
) : string {
  const hourStr = hour.toString().padStart(2, '0');
  const minuteStr = minute.toString().padStart(2, '0');
  const secondeStr = second.toString().padStart(2, '0');
  return `${hourStr}:${minuteStr}:${secondeStr}`;
}

/**
 * Parse a 'date' or 'time' string into a normalized format, filling missing part (if any) with
 * the filler value.
 * @param value Value to parse.
 * @param type Type of value ('date' or 'time').
 * @param filler Filler value to be used in missing field.
 * @returns Date or Time string.
 */
function parseDateTime(value: string, type: 'date' | 'time', filler: number) : string {
  const separator = type === 'date' ? '/' : ':';
  const split = value.split(separator);
  if (split.length < 2 || split.length > 3) {
    return null;
  }

  const values = split.map((s) => parseInt(s, 10)).filter((i) => !Number.isNaN(i));
  if (split.length !== values.length) {
    return null;
  }
  if (values.length === 2) {
    if (type === 'date') {
      values.splice(0, 0, filler);
    } else {
      values.push(filler);
    }
  }

  if (type === 'date') {
    const dateStr = createDateString(values[0], values[1], values[2]);
    const date = new Date(dateStr);
    return Number.isNaN(date.getTime()) ? null : dateStr;
  }

  const [hours, minutes, seconds] = values;
  if (hours < 0 || hours > 23
    || minutes < 0 || minutes > 59
    || seconds < 0 || seconds > 59) {
    return null;
  }

  return createTimeString(hours, minutes, seconds);
}

/**
 * Helper function that parse a string with potential comparison characters.
 * @private
 * @param value String to parse.
 * @returns Object containing the string without any comparison characters and the
 * comparison type.
 */
function parseDateFilterValue(value: string) : {
  value?: string,
  comparisonType?: ComparisonType,
} {
  const { value: strValue, comparisonType } = parseComparisonInFilter(value);

  if (strValue == null || strValue.length === 0) {
    return {};
  }
  let inferredValue = strValue;

  const split = strValue.split(' ');
  const now = new Date();
  if (split.length === 2) {
    const date = parseDateTime(split[0], 'date', now.getFullYear());
    const time = parseDateTime(split[1], 'time', 0);

    if (time != null && date != null) {
      inferredValue = `${date}T${time}`;
    } else {
      return {};
    }
  } else if (split.length === 1 && split[0].length <= 10) {
    let date = parseDateTime(split[0], 'date', now.getFullYear());
    let time = parseDateTime(split[0], 'time', 0);

    if (date != null || time != null) {
      date = date ?? createDateString(now.getFullYear(), now.getMonth() + 1, now.getDate());
      time = time ?? createTimeString(0, 0, 0);
      inferredValue = `${date}T${time}`;
    } else {
      return {};
    }
  }

  return {
    value: inferredValue,
    comparisonType,
  };
}

/**
 * Parse input value into Filter object for datetime filter.
 * @private
 * @param value String to parse.
 * @returns Object holding the current value of the filter.
 */
function parseDateFilter(value: string) : DateTimeFilter {
  if (value == null || value === '') {
    return {
      kind: ColumnType.DateTime,
      isActive: false,
      looksForNullValues: false,
    };
  }

  if (value === '//') {
    return {
      kind: ColumnType.DateTime,
      isActive: true,
      looksForNullValues: true,
    };
  }

  let res: DateTimeFilter = {
    kind: ColumnType.DateTime,
    isActive: false,
    looksForNullValues: false,
  };
  const substring = value.split('&');
  if (substring.length === 2) {
    const { value: value1, comparisonType: comparison1 } = parseDateFilterValue(substring[0]);
    const { value: value2, comparisonType: comparison2 } = parseDateFilterValue(substring[1]);
    res = {
      kind: ColumnType.DateTime,
      isActive: value1 != null || value2 != null,
      looksForNullValues: false,
      value1,
      comparison1,
      value2,
      comparison2,
    };
  } else if (substring.length === 1) {
    const { value: value1, comparisonType: comparison1 } = parseDateFilterValue(substring[0]);
    res = {
      kind: ColumnType.DateTime,
      isActive: value1 != null,
      looksForNullValues: false,
      value1,
      comparison1,
    };
  }
  return res;
}

/**
 * Parse the string into a ValueFilter structure. The parsing method used depends on the columnType
 * parameter.
 * @param value String to parse.
 * @param columnType Type of the filter's column. Determine which parsing method is applied.
 * @returns The filter created based on the parsing input data.
 */
function parseFilterValue(value: string, columnType: ColumnType) : ValueFilter {
  switch (columnType) {
    case ColumnType.String:
      return parseStringFilter(value);
    case ColumnType.Number:
      return parseNumberFilter(value);
    case ColumnType.Select:
      return parseSelectFilter(value);
    case ColumnType.DateTime:
      return parseDateFilter(value);
    default:
      throw new Error(`Unable to parse value for unknown column type ${columnType}.`);
      break;
  }
}

export default parseFilterValue;
