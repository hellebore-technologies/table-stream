import {
  ValueFilter,
  ValueFilters,
} from '../types/valueFilter';
import { Column, NotNoneColumn } from '../types/column';
import { ColumnType, ComparisonType } from '../types/enum';

/**
 * Apply the comparison given in paramter to the data given in parameter.
 * @param datum1 Left side of the comparison.
 * @param comparison Type of comparison.
 * @param datum2 Right side of the comparison.
 * @returns The result of the comparison.
 */
function applyComparison(
  datum1: string | number,
  comparison: ComparisonType,
  datum2: string | number,
) : boolean {
  if (typeof datum1 !== typeof datum2) {
    return false;
  }
  switch (comparison) {
    case ComparisonType.Equal:
      return datum1 === datum2;
    case ComparisonType.Greater:
      return datum1 > datum2;
    case ComparisonType.GreaterEqual:
      return datum1 >= datum2;
    case ComparisonType.Lesser:
      return datum1 < datum2;
    case ComparisonType.LesserEqual:
      return datum1 <= datum2;
    default:
      return false;
  }
}

/**
 * Check that the datum is valid according to the filter.
 * @param filter Filter that should be applied.
 * @param datum Datum on which to apply the filter.
 * @returns true if the datum should be kept, false otherwise.
 */
export function applyValueFilter(filter: ValueFilter, datum: string | number) : boolean {
  if (filter == null || !filter.isActive) {
    return true;
  }

  if (filter.kind !== ColumnType.Select && filter.looksForNullValues) {
    return datum == null;
  }

  if (filter.kind === ColumnType.String && typeof datum === 'string') {
    const index = datum.toLowerCase().indexOf(filter.value);
    let isFilterValid = index !== -1;
    if (filter.begin) {
      isFilterValid = isFilterValid && index === 0;
    }
    if (filter.end) {
      isFilterValid = isFilterValid && (datum.length - index) === filter.value.length;
    }
    return isFilterValid;
  }

  if (filter.kind === ColumnType.Number && typeof datum === 'number') {
    let isFilterValid = true;
    if (filter.value1 != null && filter.comparison1 != null) {
      isFilterValid = isFilterValid && applyComparison(datum, filter.comparison1, filter.value1);
    }
    if (filter.value2 != null && filter.comparison2 != null) {
      isFilterValid = isFilterValid && applyComparison(datum, filter.comparison2, filter.value2);
    }
    return isFilterValid;
  }

  if (filter.kind === ColumnType.Select && typeof datum === 'number') {
    return filter.values.includes(datum);
  }

  if (filter.kind === ColumnType.DateTime && typeof datum === 'string') {
    let isFilterValid = true;
    if (filter.value1 != null && filter.comparison1 != null) {
      isFilterValid = isFilterValid && applyComparison(datum, filter.comparison1, filter.value1);
    }
    if (filter.value2 != null && filter.comparison2 != null) {
      isFilterValid = isFilterValid && applyComparison(datum, filter.comparison2, filter.value2);
    }
    return isFilterValid;
  }

  return false;
}

/**
 * Find the function that return the value of a given column from the data object and apply
 * it to the given object.
 * @param column Column definition.
 * @param obj Data object.
 * @returns Value extracted from the data object.
 */
function getData<T extends Record<string, unknown>>(
  column: NotNoneColumn<T>,
  obj: T,
) : number | string {
  if (column.getFilterData != null) {
    return column.getFilterData(obj);
  }
  if (column.getRawData != null) {
    return column.getRawData(obj);
  }
  return column.getData(obj);
}

/**
 * Check that an object is valid according to the columns' filters.
 * @param obj Object to check.
 * @param columns Column definitions.
 * @param filter Current filters for the columns.
 * @returns true if the object is valid according to the filters, false otherwise.
 */
export function isValid<T extends Record<string, unknown>>(
  obj: T,
  columns: Column<T>[],
  filter: ValueFilters,
) : boolean {
  return columns.reduce<boolean>(
    (p, c) => (
      p
      && (
        c.type === ColumnType.None
        || applyValueFilter(filter[c.id], getData(c, obj))
      )
    ),
    true,
  );
}
