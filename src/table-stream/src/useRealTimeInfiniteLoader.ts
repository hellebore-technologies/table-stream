import { useState, useEffect, useRef } from 'react';
import { FixedSizeList } from 'react-window';

import { ListRange } from './types/component';
import { DataResult, Filter, Ordering } from './types/filter';

/**
 * Hook that will automatically fetch new data when the user is scrolling to the end of the table.
 * It helps implementing infinite loading behavior. It will also periodically refresh the rows
 * currently displayed to the user to ensure that they are up to date.
 * @param updateInterval Number os ms between two refresh of the row displayed.
 * @param fetchData Function called to get new data.
 * @param fetchUpdateData Function called to refresh the data displayed.
 * @param threshold Number of data not displayed under which new data will be fetched.
 * @returns Array containing:
 *  - array of data
 *  - number of match
 *  - reference to the list of row
 *  - whether the user is scrolling
 *  - onFilterChange function
 *  - onItemRendered function
 */
export default function useRealTimeInfiniteLoader<T extends Record<string, unknown>>(
  updateInterval: number,
  fetchData: (filter: Filter) => Promise<DataResult<T>>,
  fetchUpdateData: (ids: T[], ordering: Ordering) => Promise<T[]>,
  threshold = 100,
) : [
    data: T[],
    numberOfData: number,
    refToList: React.MutableRefObject<FixedSizeList>,
    isScrolling: boolean,
    onFilterChange: (newFilter: Filter) => void,
    onItemRendered: (props: ListRange) => void,
  ] {
  const [data, setData] = useState<T[]>([]);
  const [numberOfResults, setNumberOfResults] = useState(0);
  const [searchAfter, setSearchAfter] = useState<(string | number)[]>(null);
  const [filter, setFilter] = useState<Filter>({
    valueFilters: {},
    ordering: null,
  });
  const [isScrolling, setIsScrolling] = useState(false);
  const [visibleRange, setVisibleRange] = useState<ListRange>(null);
  const listRef = useRef<FixedSizeList>(null);

  useEffect(
    () => {
      if (!isScrolling) {
        return () => { /* noop */ };
      }

      const ids = data.slice(visibleRange.overscanStartIndex, visibleRange.overscanStopIndex);

      const timer = setInterval(
        () => {
          fetchUpdateData(ids, filter.ordering)
            .then((results: T[]) => {
              setData([
                ...data.slice(0, visibleRange.overscanStartIndex),
                ...results,
                ...data.slice(visibleRange.overscanStopIndex),
              ]);
            });
        },
        updateInterval,
      );
      return () => clearInterval(timer);
    },
    [isScrolling, data, visibleRange, filter],
  );

  useEffect(
    () => {
      if (isScrolling) {
        return () => { /* noop */ };
      }

      const timer = setInterval(
        () => {
          fetchData(filter)
            .then((dataResult: DataResult<T>) => {
              setData(dataResult.data);
              setSearchAfter(dataResult.nextSearchAfter);
              setNumberOfResults(dataResult.numberOfResults);
            });
        },
        updateInterval,
      );
      return () => clearInterval(timer);
    },
    [isScrolling, filter],
  );

  const onChangeFilter = (newFilter: Filter) => {
    setFilter(newFilter);
    if (listRef.current != null) {
      listRef.current.scrollToItem(0);
    }
    fetchData(newFilter)
      .then((dataResult: DataResult<T>) => {
        setData(dataResult.data);
        setSearchAfter(dataResult.nextSearchAfter);
        setNumberOfResults(dataResult.numberOfResults);
      });
  };

  const onItemRendered = (props: ListRange) => {
    setVisibleRange(props);
    const { overscanStopIndex, visibleStartIndex } = props;
    if (filter.ordering != null) {
      if (
        isScrolling
        && (data.length - overscanStopIndex < threshold)
        && data.length < numberOfResults
      ) {
        const modifiedFilter: Filter = {
          ...filter,
          searchAfter,
        };

        fetchData(modifiedFilter)
          .then((dataResult: DataResult<T>) => {
            setData([...data, ...dataResult.data]);
            setSearchAfter(dataResult.nextSearchAfter);
            setNumberOfResults(dataResult.numberOfResults);
          });
      }
      setIsScrolling(visibleStartIndex !== 0);
    }
  };

  return [
    data,
    numberOfResults,
    listRef,
    isScrolling,
    onChangeFilter,
    onItemRendered,
  ];
}
