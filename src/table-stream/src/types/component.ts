import { FC } from 'react';
import { ListOnItemsRenderedProps } from 'react-window';

import { TableHeaderProps } from '../components/TableHeader';
import { ColumnHeaderProps } from '../components/ColumnHeader';
import { ColumnHeaderInputBasicProps } from '../components/ColumnHeaderInputBasic';
import { ColumnHeaderInputSelectProps } from '../components/ColumnHeaderInputSelect';
import { ColumnHeaderSortIconProps } from '../components/ColumnHeaderSortIcon';
import { ColumnHeaderTitleProps } from '../components/ColumnHeaderTitle';
import { TableBodyProps } from '../components/TableBody';
import { RowProps } from '../components/Row';
import { RowCellProps } from '../components/RowCell';
import { RowCellContentProps } from '../components/RowCellContent';
import { ScrollBannerProps } from '../components/ScrollBanner';

/**
 * Components used to build the Table.
 */
export interface Components<T extends Record<string, unknown>> {
  /** Component containing all the columns' headers. */
  tableHeader: FC<TableHeaderProps>;
  /** Component containing all the components of a column header. */
  columnHeader: FC<ColumnHeaderProps<T>>;
  /** Filter input for select type column. */
  columnHeaderInputSelect: FC<ColumnHeaderInputSelectProps<T>>;
  /** Filter input for all column except select type. */
  columnHeaderInputBasic: FC<ColumnHeaderInputBasicProps<T>>;
  /** Icons to show sort order of a column. */
  columnHeaderSortIcon: FC<ColumnHeaderSortIconProps>;
  /** Component used to render the title of a column. */
  columnHeaderTitle: FC<ColumnHeaderTitleProps>;
  /** Component containing the rows. */
  tableBody: FC<TableBodyProps>;
  /** Component representing a table row and containing the cells. */
  row: FC<RowProps<T>>;
  /** Component representing a cell in a row. */
  rowCell: FC<RowCellProps<T>>;
  /**
   * Component representing the content of cell in a row. Used by default to render cell content.
   * Can be overridden by column's cellRenderer property.
   * */
  rowCellContent: FC<RowCellContentProps<T>>;
  /** Component displayed when a user is scrolling. */
  scrollBanner: FC<ScrollBannerProps>;
}

export type ListRange = ListOnItemsRenderedProps;
