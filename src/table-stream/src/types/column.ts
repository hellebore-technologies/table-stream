import { ReactNode } from 'react';
import { Options } from 'react-select';

import { ColumnType } from './enum';

/**
 * Definition of a Select type column value.
 */
export interface OptionType {
  /** Actual value. */
  value: number;
  /** Display value. */
  label: string;
}

/**
 * Column that doesn't represent a part of data. Typically used to display buttons.
 */
export interface NoneColumn<T extends Record<string, unknown>> {
  /** Type of the column. */
  type: ColumnType.None;
  /** Column's identifier. Must be unique among all columns. */
  id: string;
  /** Column's name that will be displayed in the header. */
  displayName: string;
  /** CSS flex value defining the columns width. */
  flex: string;
  /** Render function for column's cells. */
  cellRenderer: (data: T) => ReactNode;
}

/**
 * Basic definition of a data column.
 */
interface BaseColumn<T extends Record<string, unknown>> {
  /** Column's identifier. Must be unique among all columns. */
  id: string;
  /** Column's name that will be displayed in the header. */
  displayName: string;
  /** CSS flex value defining the columns width. */
  flex: string;
  /** Whether the column can be filtered. */
  hasFilter: boolean;
  /** Whether the column can be sorted. */
  isSortable: boolean;
  /** Compute display value for the column from the row's data. */
  getData: (data: T) => number | string;
  /**
   * If define compute, from the row's data, the value used to sort and filter the column.
   * It essentially has the same purpose as getRawData and getFilterData.
   * */
  getRawData?: (data: T) => number | string;
  /**
   * If define compute, from the row's data, the value used to sort and filter the column.
   * Priority order, from most to least priority, is:
   *  - getFilterData
   *  - getRawData
   *  - getData
   * */
  getFilterData?: (data: T) => number | string;
  /**
   * If define compute, from the row's data, the value used to sort and filter the column.
   * Priority order, from most to least priority, is:
   *  - getSortData
   *  - getRawData
   *  - getData
   * */
  getSortData?: (data: T) => number | string;
  /** Render function for column's cells. */
  cellRenderer?: (data: T) => ReactNode;
}

/**
 * Column definition for string data.
 */
export interface StringColumn<T extends Record<string, unknown>> extends BaseColumn<T> {
  type: ColumnType.String;
}

/**
 * Column definition for enum data.
 */
export interface SelectColumn<T extends Record<string, unknown>> extends BaseColumn<T> {
  type: ColumnType.Select;
  /** Possible values for the column. */
  selectOptions: Options<OptionType>;
}

/**
 * Column definition for datetime data.
 */
export interface DateColumn<T extends Record<string, unknown>> extends BaseColumn<T> {
  type: ColumnType.DateTime;
  /** Whether the column display local time or UTC. */
  isLocalTime: boolean;
}

/**
 * Column definition for number data.
 */
export interface NumberColumn<T extends Record<string, unknown>> extends BaseColumn<T> {
  type: ColumnType.Number;
}

/**
 * Column displaying data. This type of column can be filtered and used for sorting the table.
 */
export type NotNoneColumn<T extends Record<string, unknown>> =
  StringColumn<T>
  | NumberColumn<T>
  | SelectColumn<T>
  | DateColumn<T>;

/**
 * Column definition.
 */
export type Column<T extends Record<string, unknown>> =
  NoneColumn<T>
  | NotNoneColumn<T>;
