import { ColumnType, ComparisonType } from './enum';

/**
 * Filter for string values.
 */
export interface StringFilter {
  /** Filter's type. */
  kind: ColumnType.String;
  /** Whether the filter should be applied. */
  isActive: boolean;
  /** Whether to display only null values. */
  looksForNullValues: boolean;
  /** String to search for. */
  value: string;
  /** Whether to anchor search at the beginning. */
  begin: boolean;
  /** Whether to anchor search at the end. */
  end: boolean;
}

/**
 * Filter for number values.
 */
export interface NumberFilter {
  /** Filter's type. */
  kind: ColumnType.Number;
  /** Whether the filter should be applied. */
  isActive: boolean;
  /** Whether to display only null values. */
  looksForNullValues: boolean;
  /** First value. */
  value1?: number;
  /** First comparison operation. */
  comparison1?: ComparisonType,
  /** Second value. */
  value2?: number;
  /** Second comparison operation. */
  comparison2?: ComparisonType,
}

/**
 * Filter for enum values.
 */
export interface SelectFilter {
  /** Filter's type. */
  kind: ColumnType.Select;
  /** Whether the filter should be applied. */
  isActive: boolean;
  /** Selected values. */
  values: number[];
}

/**
 * Filter for DateTime values.
 */
export interface DateTimeFilter {
  /** Filter's type. */
  kind: ColumnType.DateTime;
  /** Whether the filter should be applied. */
  isActive: boolean;
  /** Whether to display only null values. */
  looksForNullValues: boolean;
  /** First value. */
  value1?: string;
  /** First comparison operation. */
  comparison1?: ComparisonType,
  /** Second value. */
  value2?: string;
  /** Second comparison operation. */
  comparison2?: ComparisonType;
}

/**
 * Filter for a column.
 */
export type ValueFilter = StringFilter | NumberFilter | SelectFilter | DateTimeFilter;

/**
 * Object containing all the filters for a table.
 */
export interface ValueFilters {
  [columnId: string]: ValueFilter,
}
