import { ValueFilters } from './valueFilter';

export enum Order {
  Ascending = 1,
  Descending = 2
}

/**
 * Define the order by which the table is sorted.
 */
export interface Ordering {
  /** Column's id. */
  fieldName: string;
  /** Sort order. */
  order: Order;
}

/**
 * Define the filters and order that are applied to the table's data.
 */
export interface Filter {
  /** List of data ids used by search engine to retrieve next page. */
  searchAfter?: (string | number)[];
  /** Ordering of the data. */
  ordering: Ordering;
  /** Filters */
  valueFilters: ValueFilters,
}

/**
 * Result returned by the data provider after applying the Filter object sent.
 */
export interface DataResult<T> {
  /** List of data. */
  data: T[];
  /** List of data ids used by search engine to retrieve next page. */
  nextSearchAfter?: (string | number)[];
  /** Number of match found. */
  numberOfResults: number;
}
