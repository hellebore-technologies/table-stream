enum ComparisonType {
  Greater,
  GreaterEqual,
  Lesser,
  LesserEqual,
  Equal,
}

export default ComparisonType;
