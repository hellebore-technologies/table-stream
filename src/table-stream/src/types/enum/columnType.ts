enum ColumnType {
  None,
  String,
  Number,
  DateTime,
  Select
}

export default ColumnType;
