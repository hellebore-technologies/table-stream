import { CSSObject } from '@emotion/react';

import { Components } from './component';

/**
 * Simple object containing background and font colors.
 */
export interface Color {
  /** Background color. */
  background: string;
  /** Font color. */
  font: string;
}

/**
 * Customizable color scheme definition used by the library.
 */
export interface Theme {
  /** Accent color. */
  highlight: Color;
  /** Color used for hovered lines. */
  hover: Color;
  /** Basic color of the table. */
  base: Color;
  /** Borders' color. */
  border: string;
  /** Color used for error messages and to highlight invalid inputs. */
  error: string;
}

type RecursivePartial<T> = {
  [P in keyof T]?: Partial<T[P]>;
};

/**
 * Customizable color scheme definition used by the library.
 */
export type PartialTheme = RecursivePartial<Theme>;

/**
 * Function that apply a Theme to a given set of styles and return the merged styles.
 */
export type CSSWithTheme = (theme: Theme) => CSSObject;

interface TableCSSWithTheme {
  /** Function used to compute main component's styles. */
  table: CSSWithTheme;
}

/**
 * Functions that compute the styles of each of the components of the table.
 */
export type ComponentCSSWithTheme<T extends Record<string, unknown>> = TableCSSWithTheme & {
  [K in keyof Components<T>]: CSSWithTheme;
};

interface TableComponentsCSS {
  /** Styles to be applied to the main component. */
  table: CSSObject;
}

/**
 * Styles for each components used by the table.
 */
export type ComponentsCSS<T extends Record<string, unknown>> = TableComponentsCSS & {
  [K in keyof Components<T>]: CSSObject;
};
