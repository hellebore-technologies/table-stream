import { Column, NotNoneColumn } from './types/column';
import { ColumnType } from './types/enum';
import { Order, Ordering } from './types/filter';

/**
 * Compare values according to the ascending order with null value last.
 * @param a First values.
 * @param b Second values.
 * @returns Return :
 *  - 0 if a === b
 *  - 1 if a > b
 *  - -1 if a < b
 */
function ascendingComparer<T>(a: T, b: T): number {
  if (a === b) return 0;
  if (a == null) return 1;
  if (b == null) return -1;
  return a > b ? 1 : -1;
}

/**
 * Compare values according to the descending order with null value last.
 * @param a First values.
 * @param b Second values.
 * @returns Return :
 *  - 0 if a === b
 *  - 1 if a < b
 *  - -1 if a > b
 */
function descendingComparer<T>(a: T, b: T): number {
  if (a === b) return 0;
  if (a == null) return 1;
  if (b == null) return -1;
  return a < b ? 1 : -1;
}

/**
 * Find the function that return the value of a given column from the data object.
 * @param column Column definition.
 * @returns Getter function.
 */
function getDataGetter<T extends Record<string, unknown>>(
  column: NotNoneColumn<T>,
) : (data: T) => number | string {
  if (column.getSortData != null) {
    return column.getSortData;
  }
  if (column.getRawData != null) {
    return column.getRawData;
  }
  return column.getData;
}

/**
 * Sort the table's data according to the ordering provided.
 * @param data Array of data.
 * @param columns Column definitions.
 * @param ordering Ordering of the table.
 * @returns Sorted array of data.
 */
function sortData<T extends Record<string, unknown>>(
  data: T[],
  columns: Column<T>[],
  ordering: Ordering,
): T[] {
  if (ordering == null) {
    return data;
  }

  const sortColumn = columns.find((c) => c.id === ordering.fieldName);
  if (sortColumn == null || sortColumn.type === ColumnType.None) {
    return data;
  }

  const getter = getDataGetter(sortColumn);
  const comparer = ordering.order === Order.Ascending
    ? ascendingComparer
    : descendingComparer;
  return data.sort((a, b) => comparer(getter(a), getter(b)));
}

export default sortData;
