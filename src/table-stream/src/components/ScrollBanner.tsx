/** @jsx jsx */
import { ReactElement } from 'react';
import { CSSObject, jsx } from '@emotion/react';

import { Theme } from '../types/styles';

export interface ScrollBannerProps {
  /** Whether the user is scrolling down the table. */
  isScrolling: boolean;
  /** Table's scrollbar size. Use to define banner width to match table content's width. */
  scrollBarSize: number;
  /** Size of a table's row. */
  rowHeight: number;
  /** Styles to be applied. */
  cssStyle: CSSObject;
  /** Custom color scheme. */
  theme: Theme;
  /** Handle scroll to the top of the table. */
  scrollTop: () => void;
}

export function scrollBannerCSS() : CSSObject {
  return {
    label: 'scroll-banner',
    position: 'absolute',
    top: 0,
    zIndex: 1,
    alignItems: 'center',
    justifyContent: 'center',

    '&:hover': {
      cursor: 'pointer',
      textDecoration: 'underline',
    },
  };
}

export default function ScrollBanner({
  isScrolling,
  scrollBarSize,
  rowHeight,
  cssStyle,
  theme,
  scrollTop,
}: ScrollBannerProps) : ReactElement {
  return (
    <div
      css={cssStyle}
      style={{
        display: isScrolling ? 'flex' : 'none',
        width: `calc(100% - ${scrollBarSize}px)`,
        height: rowHeight * 2,
        fontSize: '1.25em',
        ...theme.base,
      }}
      role="button"
      tabIndex={0}
      onClick={scrollTop}
    >
      <span>New updates paused due to scroll. Click here to resume.</span>
    </div>
  );
}
