/** @jsx jsx */
import {
  PropsWithChildren,
  ReactElement,
  useState,
} from 'react';
import { CSSObject, jsx } from '@emotion/react';

import { ColumnType } from '../types/enum';
import {
  DateColumn,
  NumberColumn,
  StringColumn,
} from '../types/column';

import { parseFilterValue } from '../filter';
import { Theme } from '../types/styles';
import { ValueFilter } from '../types/valueFilter';

export function columnHeaderInputBasicCSS(theme: Theme) : CSSObject {
  return {
    label: 'column-header-input-basic',
    width: 'calc(100% - 0.5em)',
    marginTop: '0.5em',
    padding: 'calc(0.25em + 2px) calc(0.5em + 2px)',
    borderRadius: '4px',
    border: '1px solid',
    outline: 0,
    backgroundColor: theme.base.background,
    color: theme.base.font,

    '&:focus': {
      boxShadow: '0 0 0 1px var(--shadowColor)',
    },
  };
}

export interface ColumnHeaderInputBasicProps<T extends Record<string, unknown>> {
  /** Filter's default value. */
  defaultValue?: string;
  /** Column's definition. */
  column: StringColumn<T> | DateColumn<T> | NumberColumn<T>;
  /** Styles to be applied. */
  cssStyle: CSSObject;
  /** Custom color scheme. */
  theme: Theme;
  /** Fired when the user change the value of the filer. */
  onChangeColumnFilter: (value: ValueFilter, columnId: string) => void;
}

/**
 * Parse the value of the filter into a ValueFilter.
 * @param value Raw value of the filter.
 * @param column Column definition.
 * @returns ValueFilter created.
 */
export function parseValue<T extends Record<string, unknown>>(
  value: string,
  column: StringColumn<T> | DateColumn<T> | NumberColumn<T>,
) : ValueFilter {
  const filter = parseFilterValue(value, column.type);
  if (column.type === ColumnType.DateTime
    && filter.kind === ColumnType.DateTime
    && column.isLocalTime
  ) {
    if (filter.value1 != null) {
      filter.value1 = (new Date(filter.value1)).toISOString();
    }
    if (filter.value2 != null) {
      filter.value2 = (new Date(filter.value2)).toISOString();
    }
  }
  return filter;
}

export default function ColumnHeaderInputBasic<T extends Record<string, unknown>>({
  defaultValue = null,
  column,
  cssStyle,
  theme,
  onChangeColumnFilter,
}: PropsWithChildren<ColumnHeaderInputBasicProps<T>>) : ReactElement {
  const [value, setValue] = useState<string>(defaultValue ?? '');
  const [isActive, setIsActive] = useState<boolean>(defaultValue != null);

  const isValid = (value != null && value !== '' && isActive)
    || ((value == null || value === '') && !isActive);
  const borderColor = isValid ? theme.border : theme.error;
  const shadow = isValid ? null : `0 0 0 1px ${borderColor}`;

  const onChangeEventHandler = (evt: React.ChangeEvent<HTMLInputElement>) => {
    const newValue = evt.target.value;
    const filter = parseValue(newValue, column);
    setValue(newValue);
    setIsActive(filter.isActive);
    onChangeColumnFilter(filter, column.id);
  };

  return (
    <input
      css={cssStyle}
      style={{
        '--shadowColor': borderColor,
        borderColor,
        boxShadow: shadow,
      } as React.CSSProperties}
      type="text"
      value={value}
      onChange={onChangeEventHandler}
    />
  );
}
