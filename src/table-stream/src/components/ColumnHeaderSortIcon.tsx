/** @jsx jsx */
import { ReactElement } from 'react';
import { CSSObject, jsx } from '@emotion/react';

import { Order } from '../types/filter';
import { Theme } from '../types/styles';

interface CommonSortIconProps {
  /** Styles to be applied. */
  cssStyle: CSSObject,
}

function SortIconNeutral({ cssStyle }: CommonSortIconProps) : ReactElement {
  return (
    <svg css={cssStyle} enableBackground="new 0 0 64 64" viewBox="0 0 64 64" xmlns="http://www.w3.org/2000/svg">
      <path d="m31.414 15.586-7-7c-.78-.781-2.048-.781-2.828 0l-7 7c-.781.781-.781 2.047 0 2.828.78.781 2.048.781 2.828 0l3.586-3.586v39.172c0 1.104.896 2 2 2s2-.896 2-2v-39.172l3.586 3.586c.39.391.902.586 1.414.586s1.024-.195 1.414-.586c.781-.781.781-2.047 0-2.828z" />
      <path d="m49.414 45.586c-.781-.781-2.047-.781-2.828 0l-3.586 3.586v-39.172c0-1.104-.896-2-2-2s-2 .896-2 2v39.172l-3.586-3.586c-.781-.781-2.048-.781-2.828 0-.781.781-.781 2.047 0 2.828l7 7c.391.391.902.586 1.414.586s1.023-.195 1.414-.586l7-7c.781-.781.781-2.047 0-2.828z" />
    </svg>
  );
}

function SortDescendingIcon({ cssStyle }: CommonSortIconProps) : ReactElement {
  return (
    <svg css={cssStyle} xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 512 512" enableBackground="new 0 0 512 512">
      <g>
        <g>
          <g>
            <rect x="2.344" width="400" height="32" />
            <rect x="2.344" y="96" width="320" height="32" />
            <rect x="2.344" y="192" width="256" height="32" />
            <rect x="2.344" y="288" width="192" height="32" />
            <rect x="2.344" y="384" width="128" height="32" />
            <rect x="2.344" y="480" width="64" height="32" />
            <polygon points="487.032,428.688 466.344,449.376 466.344,128 434.344,128 434.344,449.376 413.656,428.688 391.032,451.312 450.344,510.624 509.656,451.312" />
            <rect x="434.344" y="64" width="32" height="32" />
            <rect x="434.344" width="32" height="32" />
          </g>
        </g>
      </g>
    </svg>
  );
}

function SortAscendingIcon({ cssStyle }: CommonSortIconProps) : ReactElement {
  return (
    <svg css={{ ...cssStyle, transform: 'rotate(180deg) scaleX(-1)' }} xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 512 512" enableBackground="new 0 0 512 512">
      <g>
        <g>
          <g>
            <rect x="2.344" width="400" height="32" />
            <rect x="2.344" y="96" width="320" height="32" />
            <rect x="2.344" y="192" width="256" height="32" />
            <rect x="2.344" y="288" width="192" height="32" />
            <rect x="2.344" y="384" width="128" height="32" />
            <rect x="2.344" y="480" width="64" height="32" />
            <polygon points="487.032,428.688 466.344,449.376 466.344,128 434.344,128 434.344,449.376 413.656,428.688 391.032,451.312 450.344,510.624 509.656,451.312" />
            <rect x="434.344" y="64" width="32" height="32" />
            <rect x="434.344" width="32" height="32" />
          </g>
        </g>
      </g>
    </svg>
  );
}

export interface ColumnHeaderSortIconProps {
  /** Whether sort is active for the column considered. */
  isActive: boolean;
  /** Column's sort order. */
  order?: Order;
  /** Styles to be applied. */
  cssStyle: CSSObject;
  /** Custom color scheme. */
  theme: Theme;
}

export function columnHeaderSortIconCSS(theme: Theme) : CSSObject {
  return {
    label: 'column-header-sort-icon',
    position: 'absolute',
    top: '2px',
    right: '2px',
    width: '1.5em',
    padding: '0.2em',
    fill: theme.border,
    stroke: theme.border,
  };
}

export default function ColumnHeaderSortIcon({
  isActive,
  order,
  cssStyle,
  theme,
}: ColumnHeaderSortIconProps) : ReactElement {
  const activeCSSStyle = {
    ...cssStyle,

    fill: theme.base.font,
    stroke: theme.base.font,
  };

  if (!isActive) {
    return <SortIconNeutral cssStyle={cssStyle} />;
  }

  switch (order) {
    case Order.Ascending:
      return <SortAscendingIcon cssStyle={activeCSSStyle} />;

    case Order.Descending:
      return <SortDescendingIcon cssStyle={activeCSSStyle} />;

    default:
      return <SortIconNeutral cssStyle={cssStyle} />;
  }
}
