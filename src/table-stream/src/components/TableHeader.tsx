/** @jsx jsx */
import { PropsWithChildren, ReactElement } from 'react';
import { CSSObject, jsx } from '@emotion/react';

import { Theme } from '../types/styles';

export interface TableHeaderProps {
  /** Table's scrollbar size. Use to pad headers to match table content's width. */
  scrollBarSize: number;
  /** Styles to be applied. */
  cssStyle: CSSObject;
}

export function tableHeaderCSS(theme: Theme) : CSSObject {
  return {
    label: 'table-header',
    display: 'flex',
    flexDirection: 'row',
    zIndex: 2,
    flex: '0 0 auto',
    backgroundColor: theme.base.background,
    color: theme.base.font,
  };
}

export default function TableHeader({
  scrollBarSize,
  cssStyle,
  children,
}: PropsWithChildren<TableHeaderProps>) : ReactElement {
  return (
    <div
      css={cssStyle}
      style={{ paddingRight: scrollBarSize }}
    >
      {children}
    </div>
  );
}
