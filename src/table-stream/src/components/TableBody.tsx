/** @jsx jsx */
import { PropsWithChildren, ReactElement } from 'react';
import { CSSObject, jsx } from '@emotion/react';

export interface TableBodyProps {
  /** Styles to be applied. */
  cssStyle: CSSObject;
}

export function tableBodyCSS() : CSSObject {
  return {
    label: 'table-body',
    flex: '1',
    position: 'relative',
  };
}

export default function TableBody({
  cssStyle,
  children,
}: PropsWithChildren<TableBodyProps>) : ReactElement {
  return (
    <div
      css={cssStyle}
    >
      {children}
    </div>
  );
}
