/** @jsx jsx */
import { ReactElement } from 'react';
import { CSSObject, jsx } from '@emotion/react';

export interface ColumnHeaderTitleProps {
  /** Column's title. */
  title: string;
  /** Styles to be applied. */
  cssStyle: CSSObject;
}

export function columnHeaderTitleCSS() : CSSObject {
  return {
    label: 'column-header-title',
    display: 'flex',
    justifyContent: 'center',
    width: 'calc(100% - 0.5em)',
    minWidth: 0,
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    whiteSpace: 'pre',
    fontWeight: 'bold',
  };
}

export default function ColumnHeaderTitle({
  title,
  cssStyle,
} : ColumnHeaderTitleProps) : ReactElement {
  return (
    <div
      css={cssStyle}
    >
      {title}
    </div>
  );
}
