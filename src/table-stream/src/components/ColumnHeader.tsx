/** @jsx jsx */
import {
  PropsWithChildren,
  ReactElement,
} from 'react';
import { jsx, CSSObject } from '@emotion/react';

import { ColumnType } from '../types/enum';
import { Column } from '../types/column';
import { Theme } from '../types/styles';

export function columnHeaderCSS(theme: Theme) : CSSObject {
  return {
    label: 'column-header',
    display: 'flex',
    flexDirection: 'column',
    minWidth: 0,
    position: 'relative',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '0.5em',
    borderRight: `1px solid ${theme.border}`,
    borderBottom: `1px solid ${theme.border}`,
    backgroundColor: theme.base.background,
    color: theme.base.font,
  };
}

export interface ColumnHeaderProps<T extends Record<string, unknown>> {
  /** Column's definition. */
  column: Column<T>;
  /** Styles to be applied. */
  cssStyle: CSSObject;
  /** Fired when use double click on header to change sort order. */
  handleSortChange: (columnId: string) => void;
}

export default function ColumnHeader<T extends Record<string, unknown>>({
  column,
  cssStyle,
  handleSortChange,
  children,
}: PropsWithChildren<ColumnHeaderProps<T>>) : ReactElement {
  return (
    <div
      key={`header-${column.id}`}
      css={cssStyle}
      style={{ flex: column.flex }}
      onDoubleClick={() => column.type !== ColumnType.None
        && column.isSortable
        && handleSortChange(column.id)}
    >
      {children}
    </div>
  );
}
