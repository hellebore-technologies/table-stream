/** @jsx jsx */
import {
  ReactElement,
} from 'react';
import Select, {
  components,
  ValueContainerProps,
  OptionProps,
  Options,
} from 'react-select';
import { CSSObject, jsx } from '@emotion/react';

import { OptionType, SelectColumn } from '../types/column';
import { Theme } from '../types/styles';
import { ValueFilter } from '../types/valueFilter';

import { parseFilterValue } from '../filter';
import { ColumnType } from '../types/enum';

function ValueContainer({
  children,
  getValue,
  ...props
}: ValueContainerProps<OptionType, boolean>) : ReactElement {
  const values = getValue() as unknown as OptionType[];
  const data = values.length === 0 ? '' : `${values.length} selected`;
  const input = (children as unknown as ReactElement[])[1];

  return (
    // eslint-disable-next-line react/jsx-props-no-spreading
    <components.ValueContainer getValue={getValue} {...props}>
      {input}
      <span
        css={{
          label: 'value-container-value',
          overflow: 'hidden',
          textOverflow: 'ellipsis',
          whiteSpace: 'nowrap',
          minWidth: 0,
        }}
      >
        {data}
      </span>
    </components.ValueContainer>
  );
}

function Option({
  data,
  isSelected,
  ...props
} : OptionProps<OptionType, boolean>) : ReactElement {
  return (
    // eslint-disable-next-line react/jsx-props-no-spreading
    <components.Option data={data} isSelected={isSelected} {...props}>
      <span
        css={{
          label: 'select-option',
          display: 'flex',
          alignItems: 'center',

          '& > input': {
            marginRight: '10px',
          },
        }}
      >
        <input type="checkbox" checked={isSelected} readOnly />
        {data.label}
      </span>
    </components.Option>
  );
}

export interface ColumnHeaderInputSelectProps<T extends Record<string, unknown>> {
  /** Filter's default value. */
  defaultValues?: Options<OptionType>;
  /** Column's definition. */
  column: SelectColumn<T>;
  /** Styles to be applied. */
  cssStyle: CSSObject;
  /** Custom color scheme. */
  theme: Theme;
  /** Fired when the user change the value of the filer. */
  onChangeColumnFilter: (value: ValueFilter, columnId: string) => void;
}

export function columnHeaderInputSelectCSS() : CSSObject {
  return {
    label: 'column-header-input-select',
    marginTop: '0.5em',
    width: 'calc(100% - 0.5em)',
  };
}

/**
 * Parse the value of the filter into a ValueFilter.
 * @param values Raw value of the filter.
 * @returns ValueFilter created.
 */
export function parseValues(values: Options<OptionType>) : ValueFilter {
  return parseFilterValue(values.map((o) => o.value).join(), ColumnType.Select);
}

export default function ColumnHeaderInputSelect<T extends Record<string, unknown>>({
  defaultValues = null,
  column,
  cssStyle,
  theme,
  onChangeColumnFilter,
}: ColumnHeaderInputSelectProps<T>): ReactElement {
  return (
    <Select
      name={column.id}
      css={cssStyle}
      defaultValue={defaultValues}
      options={
        column.selectOptions
          .map((o) => o)
          .sort((o1, o2) => o1.label.localeCompare(o2.label))
      }
      placeholder=""
      isMulti
      hideSelectedOptions={false}
      closeMenuOnSelect={false}
      tabSelectsValue={false}
      backspaceRemovesValue={false}
      onChange={
        (options: OptionType[]) => {
          if (options != null) {
            onChangeColumnFilter(
              parseValues(options),
              column.id,
            );
          }
        }
      }
      components={{
        ValueContainer,
        Option,
      }}
      theme={(dftTheme) => ({
        ...dftTheme,
        colors: {
          ...dftTheme.colors,
          text: theme.base.font,
          primary: theme.highlight.background,
          neutral0: theme.base.background,
          neutral5: theme.base.font,
          neutral10: theme.base.font,
          neutral20: theme.border,
          neutral40: theme.border,
          neutral50: theme.border,
          neutral60: theme.border,
          neutral70: theme.base.font,
          neutral80: theme.base.font,
        },
      })}
      styles={{
        indicatorSeparator: (provided) => ({
          ...provided,
          marginTop: '4px',
          marginBottom: '4px',
        }),
        clearIndicator: (provided) => ({ ...provided, padding: 0 }),
        dropdownIndicator: (provided) => ({ ...provided, padding: 0 }),
        valueContainer: (provided) => ({ ...provided, flexWrap: 'nowrap' }),
        control: (provided, state) => {
          let boxShadow = null;
          if (state.isFocused) {
            boxShadow = `0 0 0 1px ${theme.border}`;
          }

          return ({
            ...provided,
            minHeight: 0,
            borderColor: theme.border,
            boxShadow,
            '&:hover': {
              borderColor: theme.border,
            },
          });
        },
        option: (provided, state) => {
          let backgroundColor = theme.base.background;
          let color = theme.base.font;
          if (state.isFocused) {
            backgroundColor = theme.hover.background;
            color = theme.hover.font;
          } else if (state.isSelected) {
            backgroundColor = theme.highlight.background;
            color = theme.highlight.font;
          }

          return ({
            ...provided,
            backgroundColor,
            color,
          });
        },
      }}
    />
  );
}
