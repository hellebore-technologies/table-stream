/** @jsx jsx */
import {
  PropsWithChildren,
  ReactElement,
} from 'react';
import { CSSObject, jsx } from '@emotion/react';

import { NotNoneColumn } from '../types/column';
import { ColumnType } from '../types/enum';

export interface RowCellContentProps<T extends Record<string, unknown>> {
  /** Column's definition. */
  column: NotNoneColumn<T>;
  /** Row's data. */
  data: T;
  /** Styles to be applied. */
  cssStyle: CSSObject;
}

export function rowCellContentCSS() : CSSObject {
  return {
    label: 'row-cell-content',
    display: 'flex',
    alignItems: 'center',
    alignSelf: 'stretch',
    flex: 1,
    padding: '0 1em',
    minWidth: 0,
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    whiteSpace: 'pre',
  };
}

export default function RowCell<T extends Record<string, unknown>>({
  column,
  data,
  cssStyle,
}: PropsWithChildren<RowCellContentProps<T>>) : ReactElement {
  return (
    <div
      css={{
        justifyContent: column.type === ColumnType.Number ? 'flex-end' : 'initial',
        ...cssStyle,
      }}
    >
      {column.getData(data)}
    </div>
  );
}
