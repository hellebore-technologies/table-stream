/** @jsx jsx */
import {
  PropsWithChildren,
  ReactElement,
} from 'react';
import { ListChildComponentProps } from 'react-window';
import { CSSObject, jsx } from '@emotion/react';

import { Theme } from '../types/styles';

export interface RowProps<T extends Record<string, unknown>> extends ListChildComponentProps {
  /** Styles to be applied. */
  cssStyle: CSSObject;
  /** Custom color scheme. */
  theme: Theme;
  /** Handle user's click on row. */
  onClick: (data: T) => void;
  /** Handle user's double click on row. */
  onDoubleClick: (data: T) => void;
}

export function rowCSS(theme: Theme) : CSSObject {
  return {
    label: 'row',
    display: 'flex',
    flexDirection: 'row',
    overflow: 'hidden',

    '&:hover': {
      backgroundColor: theme.hover.background,
      color: theme.hover.font,
    },
  };
}

export default function Row<T extends Record<string, unknown>>({
  index,
  data,
  style,
  cssStyle,
  theme,
  onClick,
  onDoubleClick,
  children,
}: PropsWithChildren<RowProps<T>>) : ReactElement {
  return (
    <div
      role="button"
      tabIndex={0}
      css={{
        backgroundColor: index % 2 === 1 ? theme.highlight.background : theme.base.background,
        color: index % 2 === 1 ? theme.highlight.font : theme.base.font,
        cursor: onDoubleClick != null || onClick != null ? 'pointer' : 'initial',
        ...cssStyle,
      }}
      style={style}
      onClick={() => onClick != null && onClick(data[index])}
      onDoubleClick={() => onDoubleClick != null && onDoubleClick(data[index])}
    >
      {children}
    </div>
  );
}
