/** @jsx jsx */
import {
  PropsWithChildren,
  ReactElement,
} from 'react';
import { CSSObject, jsx } from '@emotion/react';

import { Theme } from '../types/styles';
import { Column } from '../types/column';

export interface RowCellProps<T extends Record<string, unknown>> {
  /** Column's definition. */
  column: Column<T>;
  /** Styles to be applied. */
  cssStyle: CSSObject;
}

export function rowCellCSS(theme: Theme) : CSSObject {
  return {
    label: 'row-cell',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'stretch',
    minWidth: 0,
    overflow: 'hidden',
    borderLeft: `1px solid ${theme.border}`,
  };
}

export default function RowCell<T extends Record<string, unknown>>({
  column,
  cssStyle,
  children,
} : PropsWithChildren<RowCellProps<T>>): ReactElement {
  return (
    <div
      css={cssStyle}
      style={{ flex: column.flex }}
    >
      {children}
    </div>
  );
}
