﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HelleboreTech.TableStream.Core;
using Nest;

namespace HelleboreTech.TableStream.Elasticsearch
{
    /// <summary>
    /// Provides functions to insert data into the storage.
    /// </summary>
    /// <typeparam name="TData">Type of data stored.</typeparam>

    public class ElasticsearchStreamProvider<TData> : IStreamProvider<TData> where TData : FilteredItem
    {
        private readonly ElasticClient _client;

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        /// <param name="uri">A single uri representing the root of the node you want to connect to.</param>
        /// <param name="index">Index's name.</param>
        /// <param name="idField">Name of the data's id property.</param>
        public ElasticsearchStreamProvider(
            string uri,
            string index,
            string idField)
        {
            var node = new Uri(uri);
            var settings = new ConnectionSettings(node)
                .ThrowExceptions()
                .DefaultIndex(index)
                .DisableDirectStreaming()
                .DefaultMappingFor<TData>(m => m.IdProperty(idField))
                .DefaultFieldNameInferrer(p => p);
            _client = new ElasticClient(settings);
        }

        /// <summary>
        /// Insert a document into the current index.
        /// </summary>
        /// <param name="item">Document to insert.</param>
        public bool Insert(TData item)
        {
            return _client.IndexDocument(item).IsValid;
        }

        /// <summary>
        /// Insert a document into the current index asynchronously.
        /// </summary>
        /// <param name="item">Document to insert.</param>
        public async Task<bool> InsertAsync(TData item)
        {
            var response = await _client.IndexDocumentAsync(item);
            return response.IsValid;
        }

        /// <summary>
        /// Insert several documents into the current index.
        /// </summary>
        /// <param name="items">Documents to insert.</param>
        /// <returns>Number of documents inserted.</returns>
        public int Insert(IEnumerable<TData> items)
        {
            var response = _client.IndexMany(items);
            return response.Items.Count;
        }

        /// <summary>
        /// Insert several documents into the current index asynchronously.
        /// </summary>
        /// <param name="items">Documents to insert.</param>
        /// <returns>Number of documents inserted.</returns>
        public async Task<int> InsertAsync(IEnumerable<TData> items)
        {
            var response = await _client.IndexManyAsync(items);
            return response.Items.Count;
        }

        /// <summary>
        /// Update a document.
        /// </summary>
        /// <param name="id">Id of the document.</param>
        /// <param name="item">Object containing the fields to update. Must be an anonymous object.</param>
        public bool Update(string id, object item)
        {
            var searchRequest = new SearchRequest<TData>
            {
                Query = new QueryContainer(
                    new IdsQuery
                    {
                        Values = new List<Id> { id },
                    }),
            };
            var data = _client.Search<TData>(searchRequest)
                .Hits
                .Select(s => s.Index)
                .ToList();
            if (data.Count != 1)
                return false;
            var query = _client.Update<TData, object>(
                id,
                descriptor => descriptor.Index(data[0]).Doc(item));
            return query.IsValid;
        }

        /// <summary>
        /// Update a document asynchronously.
        /// </summary>
        /// <param name="id">Id of the document.</param>
        /// <param name="item">Object containing the fields to update. Must be an anonymous object.</param>
        public async Task<bool> UpdateAsync(string id, object item)
        {
            var searchRequest = new SearchRequest<TData>
            {
                Query = new QueryContainer(
                    new IdsQuery
                    {
                        Values = new List<Id> { id },
                    }),
            };
            var data = (await _client.SearchAsync<TData>(searchRequest))
                .Hits
                .Select(s => s.Index)
                .ToList();
            if (data.Count != 1)
                return false;
            var query = await _client.UpdateAsync<TData, object>(
                id,
                descriptor => descriptor.Index(data[0]).Doc(item));
            return query.IsValid;
        }
    }
}
