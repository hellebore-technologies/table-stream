﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HelleboreTech.TableStream.Core;
using Nest;

namespace HelleboreTech.TableStream.Elasticsearch
{
    /// <summary>
    /// Provides functions to search for data using filter.
    /// </summary>
    /// <typeparam name="TData">Type of data stored.</typeparam>
    public class ElasticsearchFilteredItemProvider<TData> : IFilteredItemProvider<TData>
        where TData : FilteredItem
    {
        private readonly int _batchSize;
        private readonly ElasticClient _client;

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        /// <param name="uri">A single uri representing the root of the node you want to connect to.</param>
        /// <param name="index">Index's name.</param>
        /// <param name="idField">Name of the data's id property.</param>
        /// <param name="batchSize">Maximum amount of objects returned by a query.</param>
        public ElasticsearchFilteredItemProvider(
            string uri,
            string index,
            string idField,
            int batchSize = 400)
        {
            _batchSize = batchSize;
            var node = new Uri(uri);
            var settings = new ConnectionSettings(node)
                .ThrowExceptions()
                .DefaultIndex(index)
                .DisableDirectStreaming()
                .DefaultMappingFor<TData>(m => m.IdProperty(idField))
                .DefaultFieldNameInferrer(p => p);
            _client = new ElasticClient(settings);
        }

        /// <inheritdoc />
        public async Task<DataResult<TData>> GetDataAsync(
            PropertyValueFilters<TData> propertyValueFilters,
            OrderingWithProperty<TData> ordering,
            List<object> searchAfter)
        {
            var queryList = new List<QueryContainer>();
            var nullList = new List<QueryContainer>();

            foreach (var propertyFilter in propertyValueFilters.StringFilters)
                InterpretFilter(propertyFilter, queryList, nullList);
            foreach (var propertyFilter in propertyValueFilters.NumberFilters)
                InterpretFilter(propertyFilter, queryList, nullList);
            foreach (var propertyFilter in propertyValueFilters.SelectFilters)
                InterpretFilter(propertyFilter, queryList);
            foreach (var propertyFilter in propertyValueFilters.DateTimeFilters)
                InterpretFilter(propertyFilter, queryList, nullList);

            var searchRequest = new SearchRequest<TData>
            {
                Query = new QueryContainer(
                    new BoolQuery
                    {
                        Filter = queryList,
                        MustNot = nullList,
                    }),
                SearchAfter = searchAfter,
                Sort = GetSort(ordering),
                Size = _batchSize,
            };
            var countRequest = new CountRequest<TData>
            {
                Query = searchRequest.Query,
            };

            var dataTask = _client.SearchAsync<TData>(searchRequest);
            var countTask = _client.CountAsync(countRequest);
            var data = await dataTask;
            var count = await countTask;

            return new DataResult<TData>
            {
                Data = data.Documents.ToList(),
                NextSearchAfter = data.Hits.LastOrDefault()?.Sorts?.ToList(),
                NumberOfResults = count.Count,
            };
        }

        /// <inheritdoc />
        public DataResult<TData> GetData(
            PropertyValueFilters<TData> propertyValueFilters,
            OrderingWithProperty<TData> ordering,
            List<object> searchAfter)
        {
            return GetDataAsync(
                    propertyValueFilters,
                    ordering,
                    searchAfter)
                .GetAwaiter()
                .GetResult();
        }

        /// <inheritdoc />
        public async Task<List<TData>> GetDataByIdAsync(
            IEnumerable<TData> ids,
            OrderingWithProperty<TData> ordering)
        {
            var searchRequest = new SearchRequest<TData>
            {
                Query = new QueryContainer(
                    new IdsQuery
                    {
                        Values = ids.Select(d => new Id(d)),
                    }),
                Sort = GetSort(ordering),
                Size = _batchSize,
            };

            var data = await _client.SearchAsync<TData>(searchRequest);

            return data.Documents.ToList();
        }

        /// <inheritdoc />
        public List<TData> GetDataById(IEnumerable<TData> ids, OrderingWithProperty<TData> ordering)
        {
            return GetDataByIdAsync(ids, ordering).GetAwaiter().GetResult();
        }

        private List<ISort> GetSort(OrderingWithProperty<TData> ordering)
        {
            if (ordering == null)
                return null;

            var sortField = ordering.DataPropertyName;
            SortOrder order;
            switch (ordering.Order)
            {
                case Order.Ascending:
                    order = SortOrder.Ascending;
                    break;
                case Order.Descending:
                    order = SortOrder.Descending;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(ordering.Order));
            }

            var sort = new List<ISort>
            {
                new FieldSort { Field = sortField, Order = order },
                new FieldSort { Field = nameof(FilteredItem.TieBreakerId), Order = order },
            };
            return sort;
        }

        private void InterpretFilter(
            PropertyValueFilter<TData, StringFilter> filter,
            List<QueryContainer> queryList,
            List<QueryContainer> nullList)
        {
            if (filter.ValueFilter == null || !filter.ValueFilter.IsActive)
                return;

            if (filter.ValueFilter.LooksForNullValues)
            {
                nullList.Add(new ExistsQuery { Field = filter.DataPropertyName });
                return;
            }

            var wildQuery = "";
            if (!filter.ValueFilter.Begin)
                wildQuery = "*";
            wildQuery += filter.ValueFilter.Value;
            if (!filter.ValueFilter.End)
                wildQuery += "*";

            var field = filter.DataPropertyName;

            queryList.Add(
                new WildcardQuery
                {
                    Field = field,
                    Value = wildQuery,
                });
        }

        private void SingleNumericRangeProcess(
            string propertyName,
            ComparisonType? compare,
            double? value,
            List<QueryContainer> queryList)
        {
            string greaterThan = null, greaterThanOrEqualTo = null, lessThan = null, lessThanOrEqualTo = null;
            switch (compare)
            {
                case ComparisonType.Greater:
                    greaterThan = value.ToString();
                    break;
                case ComparisonType.GreaterEqual:
                    greaterThanOrEqualTo = value.ToString();
                    break;
                case ComparisonType.Lesser:
                    lessThan = value.ToString();
                    break;
                case ComparisonType.LesserEqual:
                    lessThanOrEqualTo = value.ToString();
                    break;
                case ComparisonType.Equal:
                    queryList.Add(
                        new TermQuery
                        {
                            Field = propertyName,
                            Value = value,
                        });
                    break;
                case null:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            queryList.Add(
                new TermRangeQuery
                {
                    Field = propertyName,
                    GreaterThan = greaterThan,
                    GreaterThanOrEqualTo = greaterThanOrEqualTo,
                    LessThan = lessThan,
                    LessThanOrEqualTo = lessThanOrEqualTo,
                });
        }

        private void InterpretFilter(
            PropertyValueFilter<TData, NumberFilter> filter,
            List<QueryContainer> queryList,
            List<QueryContainer> nullList)
        {
            if (filter.ValueFilter == null || !filter.ValueFilter.IsActive)
                return;

            if (filter.ValueFilter.LooksForNullValues)
            {
                nullList.Add(new ExistsQuery { Field = filter.DataPropertyName });
                return;
            }

            SingleNumericRangeProcess(
                filter.DataPropertyName,
                filter.ValueFilter.Comparison1,
                filter.ValueFilter.Value1,
                queryList);
            SingleNumericRangeProcess(
                filter.DataPropertyName,
                filter.ValueFilter.Comparison2,
                filter.ValueFilter.Value2,
                queryList);
        }

        private void InterpretFilter(
            PropertyValueFilter<TData, SelectFilter> filter,
            List<QueryContainer> queryList)
        {
            if (filter.ValueFilter == null || !filter.ValueFilter.IsActive)
                return;

            queryList.Add(
                new TermsQuery
                {
                    Field = filter.DataPropertyName,
                    Terms = filter.ValueFilter.Values.OfType<object>().ToList(),
                });
        }

        private void SingleDateRangeProcess(
            string propertyName,
            ComparisonType? compare,
            DateTime? value,
            List<QueryContainer> queryList)
        {
            DateTime? greaterThan = null, greaterThanOrEqualTo = null, lessThan = null, lessThanOrEqualTo = null;
            switch (compare)
            {
                case ComparisonType.Greater:
                    greaterThan = value;
                    break;
                case ComparisonType.GreaterEqual:
                    greaterThanOrEqualTo = value;
                    break;
                case ComparisonType.Lesser:
                    lessThan = value;
                    break;
                case ComparisonType.LesserEqual:
                    lessThanOrEqualTo = value;
                    break;
                case ComparisonType.Equal:
                    lessThanOrEqualTo = value;
                    greaterThanOrEqualTo = value;
                    break;
                case null:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            queryList.Add(
                new DateRangeQuery
                {
                    Field = propertyName,
                    GreaterThan = greaterThan,
                    GreaterThanOrEqualTo = greaterThanOrEqualTo,
                    LessThan = lessThan,
                    LessThanOrEqualTo = lessThanOrEqualTo,
                });
        }

        private void InterpretFilter(
            PropertyValueFilter<TData, DateTimeFilter> filter,
            List<QueryContainer> queryList,
            List<QueryContainer> nullList)
        {
            if (filter.ValueFilter == null || !filter.ValueFilter.IsActive)
                return;

            if (filter.ValueFilter.LooksForNullValues)
            {
                nullList.Add(new ExistsQuery { Field = filter.DataPropertyName });
                return;
            }

            SingleDateRangeProcess(
                filter.DataPropertyName,
                filter.ValueFilter.Comparison1,
                filter.ValueFilter.Value1,
                queryList);
            SingleDateRangeProcess(
                filter.DataPropertyName,
                filter.ValueFilter.Comparison2,
                filter.ValueFilter.Value2,
                queryList);
        }
    }
}
