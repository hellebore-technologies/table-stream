﻿using System;
using HelleboreTech.TableStream.Core;
using Nest;

namespace HelleboreTech.TableStream.Elasticsearch
{
    /// <summary>
    /// Provides functions to manipulate data storage structures.
    /// </summary>
    /// <typeparam name="TData">Type of data stored.</typeparam>
    /// <typeparam name="TFilter">Type of request.</typeparam>
    public class ElasticsearchDatabaseHandler<TData, TFilter> : DatabaseHandler<TData, TFilter>
        where TData : FilteredItem
    {
        private readonly ElasticClient _client;
        private readonly string _index;
        private readonly string _indexTemplateName;
        private readonly string _indexTimeout;
        private readonly string _lifeCyclePolicyName;
        private readonly int _nbReplicas;
        private readonly int _nbShards;
        private readonly string _rolloverTime;
        private readonly bool _useTemplate;

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        /// <param name="uri">A single uri representing the root of the node you want to connect to.</param>
        /// <param name="index">Index's name.</param>
        /// <param name="nbShards">Number of shards for the index.</param>
        /// <param name="nbReplicas">Number of replicas for each shards.</param>
        public ElasticsearchDatabaseHandler(
            string uri,
            string index,
            int nbShards,
            int nbReplicas = 0) :
            this(uri, index, nbShards, false, null, null, nbReplicas)
        {
        }

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        /// <param name="uri">A single uri representing the root of the node you want to connect to.</param>
        /// <param name="index">Index's name.</param>
        /// <param name="nbShards">Number of shards for the index.</param>
        /// <param name="indexTimeout">Maximum elapsed time from index creation before deleting index.</param>
        /// <param name="rolloverTime">Maximum elapsed time from index creation before rolling write index to new one.</param>
        /// <param name="nbReplicas">Number of replicas for each shards.</param>
        public ElasticsearchDatabaseHandler(
            string uri,
            string index,
            int nbShards,
            string indexTimeout,
            string rolloverTime,
            int nbReplicas = 0) :
            this(uri, index, nbShards, true, indexTimeout, rolloverTime, nbReplicas)
        {
        }

        private ElasticsearchDatabaseHandler(
            string uri,
            string index,
            int nbShards,
            bool useTemplate,
            string indexTimeout,
            string rolloverTime,
            int nbReplicas)
        {
            var node = new Uri(uri);
            var settings = new ConnectionSettings(node)
                .ThrowExceptions()
                .DefaultIndex(index)
                .DisableDirectStreaming();
            _client = new ElasticClient(settings);
            _index = index;
            _nbShards = nbShards;
            _nbReplicas = nbReplicas;
            _useTemplate = useTemplate;
            _indexTimeout = indexTimeout;
            _rolloverTime = rolloverTime;

            if (_useTemplate)
            {
                if (string.IsNullOrWhiteSpace(indexTimeout))
                    throw new ArgumentNullException(
                        nameof(indexTimeout),
                        $"{nameof(indexTimeout)} must be set to a value.");
                if (string.IsNullOrWhiteSpace(rolloverTime))
                    throw new ArgumentNullException(
                        nameof(rolloverTime),
                        $"{nameof(rolloverTime)} must be set to a value.");

                _lifeCyclePolicyName = _index + "_policy";
                _indexTemplateName = _index + "_template";
            }
        }

        /// <inheritdoc />
        public override void CreateTable()
        {
            string writeIndex;
            var aliasesDescriptor = new AliasesDescriptor();
            if (_useTemplate)
            {
                CreateTemplate();
                writeIndex = _index + "-000001";
                aliasesDescriptor = aliasesDescriptor.Alias(
                    _index,
                    ad => ad.IsWriteIndex());
            }
            else
            {
                writeIndex = _index;
                aliasesDescriptor = null;
            }

            var putMapping = MappingBuilder();
            _client
                .Indices
                .Create(
                    writeIndex,
                    c => c
                        .Settings(
                            sd => sd
                                .NumberOfShards(_nbShards)
                                .NumberOfReplicas(_nbReplicas)
                                .Analysis(SetUpAnalysis))
                        .Map(tmd => putMapping)
                        .Aliases(ad => aliasesDescriptor)
                );
        }

        /// <inheritdoc />
        public override void DeleteTable()
        {
            if (_useTemplate)
            {
                _client.Indices.DeleteTemplate(_indexTemplateName);
                _client.Indices.Delete(_index + "*");
                _client.IndexLifecycleManagement.DeleteLifecycle(_lifeCyclePolicyName);
            }
            else
            {
                _client.Indices.Delete(_index);
            }
        }

        /// <inheritdoc />
        public override void TruncateTable()
        {
            DeleteTable();
            CreateTable();
        }

        private void CreateTemplate()
        {
            _client
                .IndexLifecycleManagement
                .PutLifecycle(
                    _lifeCyclePolicyName,
                    pld => pld
                        .Policy(
                            pd => pd
                                .Phases(
                                    phd => phd
                                        .Hot(
                                            phd => phd
                                                .Actions(
                                                    lad => lad
                                                        .SetPriority(
                                                            sp => sp
                                                                .Priority(100)
                                                        )
                                                        .Rollover(
                                                            rld => rld
                                                                .MaximumAge(_rolloverTime))
                                                )
                                        )
                                        .Delete(
                                            phd => phd
                                                .MinimumAge(_indexTimeout) // Time to keep the index
                                                .Actions(
                                                    lad => lad
                                                        .Delete(d => d) // Delete action
                                                )
                                        )
                                )
                        )
                );

            var putMapping = MappingBuilder();
            _client
                .Indices
                .PutTemplate(
                    _indexTemplateName,
                    ptd => ptd
                        .IndexPatterns(_index + "*")
                        .Settings(
                            isd => isd
                                .NumberOfShards(_nbShards)
                                .NumberOfReplicas(_nbReplicas)
                                .Setting("lifecycle.name", _lifeCyclePolicyName)
                                .Setting("lifecycle.rollover_alias", _index)
                                .Analysis(SetUpAnalysis)
                        )
                        .Map(m => putMapping)
                );
        }

        private AnalysisDescriptor SetUpAnalysis(AnalysisDescriptor analysisDescriptor)
        {
            analysisDescriptor
                .Normalizers(
                    nd => nd
                        .Custom(
                            "lowercase",
                            cnd => cnd
                                .Filters("lowercase")
                        )
                )
                .Analyzers(
                    azd => azd
                        .Custom(
                            "ngram_analyzer",
                            cazd => cazd
                                .Tokenizer("ngram_tokenizer")
                                .Filters("lowercase")
                        )
                )
                .Tokenizers(
                    td => td
                        .NGram(
                            "ngram_tokenizer",
                            ntd => ntd
                                .MinGram(3)
                                .MaxGram(3)
                                .TokenChars(
                                    TokenChar.Letter,
                                    TokenChar.Digit,
                                    TokenChar.Punctuation)
                        )
                );
            return analysisDescriptor;
        }

        private PutMappingRequest MappingBuilder()
        {
            var nestProps = new Properties();
            foreach (var filterProp in _properties.Item1)
            {
                IProperty nestProp = null;
                if (filterProp.Type == typeof(StringFilter))
                    nestProp = new KeywordProperty
                    {
                        Normalizer = "lowercase",
                    };
                else if (filterProp.Type == typeof(NumberFilter))
                    nestProp = new NumberProperty(NumberType.Double);
                else if (filterProp.Type == typeof(DateTimeFilter))
                    nestProp = new DateProperty();
                else if (filterProp.Type == typeof(SelectFilter))
                    nestProp = new NumberProperty(NumberType.Integer);
                else
                    throw new ArgumentOutOfRangeException(
                        nameof(filterProp.Type),
                        filterProp.Type,
                        "Unknown filter type.");
                nestProps.Add(filterProp.Name, nestProp);
            }

            foreach (var additionalDataProp in _properties.Item2)
            {
                IProperty nestProp = additionalDataProp.Type switch
                {
                    AdditionalFieldType.Guid     => new KeywordProperty(),
                    AdditionalFieldType.SmallInt => new NumberProperty(NumberType.Byte),
                    AdditionalFieldType.Int      => new NumberProperty(NumberType.Integer),
                    AdditionalFieldType.Long     => new NumberProperty(NumberType.Long),
                    AdditionalFieldType.Float    => new NumberProperty(NumberType.Float),
                    AdditionalFieldType.Double   => new NumberProperty(NumberType.Double),
                    AdditionalFieldType.String   => new KeywordProperty { Normalizer = "lowercase" },
                    AdditionalFieldType.Date     => new DateProperty(),
                    _ => throw new ArgumentOutOfRangeException(
                        nameof(additionalDataProp.Type),
                        additionalDataProp.Type,
                        "Unknown additional field type."),
                };

                nestProps.Add(additionalDataProp.Name, nestProp);
            }

            var putMapping = new PutMappingRequest(_index)
            {
                Properties = nestProps,
            };
            return putMapping;
        }
    }
}
