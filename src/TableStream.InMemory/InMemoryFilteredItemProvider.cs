﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HelleboreTech.TableStream.Core;

namespace HelleboreTech.TableStream.InMemory;

/// <summary>
/// Provides functions to search for data using filter.
/// </summary>
/// <typeparam name="TData">Type of data stored.</typeparam>
public class InMemoryFilteredItemProvider<TData> : IFilteredItemProvider<TData>
    where TData : FilteredItem
{
    private IEnumerable<TData> _data;
    private readonly int _batchSize;
    private readonly AscendingComparer _ascendingComparer;
    private readonly DescendingComparer _descendingComparer;
    private readonly Func<IEnumerable<TData>, Func<TData, bool>> _idFilterFactory;

    private class AscendingComparer : IComparer<object?>
    {
        public int Compare(object? x, object? y)
        {
            if (x == null && y == null)
                return 0;
            if (x == null)
                return 1;
            if (y == null)
                return -1;
            
            return x switch
            {
                double xVal => xVal.CompareTo(y),
                int xVal => xVal.CompareTo(y),
                long xVal => xVal.CompareTo(y),
                float xVal => xVal.CompareTo(y),
                short xVal => xVal.CompareTo(y),
                ushort xVal => xVal.CompareTo(y),
                string xVal => string.CompareOrdinal(xVal, (string)y),
                DateTime xVal => xVal.CompareTo(y),
                Enum xVal => xVal.CompareTo(y),
                _ => -1,
            };
        }
    }
    
    private class DescendingComparer : IComparer<object?>
    {
        public int Compare(object? x, object? y)
        {
            if (x == null && y == null)
                return 0;
            if (x == null)
                return -1;
            if (y == null)
                return 1;
            
            return x switch
            {
                double xVal => xVal.CompareTo(y),
                int xVal => xVal.CompareTo(y),
                long xVal => xVal.CompareTo(y),
                float xVal => xVal.CompareTo(y),
                short xVal => xVal.CompareTo(y),
                ushort xVal => xVal.CompareTo(y),
                string xVal => string.CompareOrdinal(xVal, (string)y),
                DateTime xVal => xVal.CompareTo(y),
                Enum xVal => xVal.CompareTo(y),
                _ => 0,
            };
        }
    }

    /// <summary>
    /// Initializes a new instance of the class.
    /// </summary>
    /// <param name="data">Data on which filters should operate.</param>
    /// <param name="idFilterFactory">Factory that uses the data collection passed to it to create the filter
    /// function used by GetDataByIds to determine which objects to return.</param>
    /// <param name="batchSize">Maximum amount of objects returned by a query.</param>
    public InMemoryFilteredItemProvider(
        List<TData> data,
        Func<IEnumerable<TData>, Func<TData, bool>> idFilterFactory,
        int batchSize = 400)
    {
        _data = data;
        _batchSize = batchSize;
        _idFilterFactory = idFilterFactory;
        _ascendingComparer = new AscendingComparer();
        _descendingComparer = new DescendingComparer();
    }

    /// <summary>
    /// Change the data on which filters are executed.
    /// </summary>
    /// <param name="data">Data on which filters should operate.</param>
    public void SetData(IEnumerable<TData> data)
    {
        _data = data;
    }

    private List<TData> SortData(IEnumerable<TData> data, OrderingWithProperty<TData>? ordering)
    {
        var sortedData = data;

        if (ordering != null)
        {
            if (ordering.Order == Order.Ascending)
                sortedData = sortedData
                    .OrderBy(d => ordering.DataGetter(d), _ascendingComparer)
                    .ThenBy(d => d.TieBreakerId);
            else
                sortedData = sortedData
                    .OrderByDescending(d => ordering.DataGetter(d), _descendingComparer)
                    .ThenBy(d => d.TieBreakerId);
        }

        return sortedData.ToList();
    }

    private bool Compare(double result, ComparisonType comparisonType)
    {
        return comparisonType switch
        {
            ComparisonType.Greater => result > 0,
            ComparisonType.GreaterEqual => result >= 0,
            ComparisonType.Lesser => result < 0,
            ComparisonType.LesserEqual => result <= 0,
            ComparisonType.Equal => Math.Abs(result) < 1E-15,
            _ => throw new ArgumentOutOfRangeException(nameof(comparisonType), comparisonType, null)
        };
    }

    private bool Compare(object value, double filterValue, ComparisonType comparisonType)
    {
        return value switch
        {
            double doubleValue => Compare(doubleValue - filterValue, comparisonType),
            int intValue => Compare(intValue - filterValue, comparisonType),
            long longValue => Compare(longValue - filterValue, comparisonType),
            float floatValue => Compare(floatValue - filterValue, comparisonType),
            short shortValue => Compare(shortValue - filterValue, comparisonType),
            ushort ushortValue => Compare(ushortValue - filterValue, comparisonType),
            _ => false
        };
    }

    private IEnumerable<TData> ApplyNumberFilers(
        IEnumerable<TData> data,
        IEnumerable<PropertyValueFilter<TData, NumberFilter>>? propertyValueFilters)
    {
        if (propertyValueFilters == null)
            return data;
        
        var filters = propertyValueFilters
            .Where(f => f.ValueFilter?.IsActive ?? false);
        var filteredData = data;
        foreach (var filter in filters)
        {
            if (filter.ValueFilter.LooksForNullValues)
            {
                filteredData = filteredData.Where(d => filter.DataGetter(d) == null);
                continue;
            }

            if (filter.ValueFilter.Value1.HasValue && filter.ValueFilter.Comparison1.HasValue)
                filteredData = filteredData
                    .Where(d => Compare(
                        filter.DataGetter(d), 
                        filter.ValueFilter.Value1.Value, 
                        filter.ValueFilter.Comparison1.Value));
            
            if (filter.ValueFilter.Value2.HasValue && filter.ValueFilter.Comparison2.HasValue)
                filteredData = filteredData
                    .Where(d => Compare(
                        filter.DataGetter(d), 
                        filter.ValueFilter.Value2.Value, 
                        filter.ValueFilter.Comparison2.Value));
        }

        return filteredData;
    }
    
    private IEnumerable<TData> ApplyStringFilers(
        IEnumerable<TData> data,
        IEnumerable<PropertyValueFilter<TData, StringFilter>>? propertyValueFilters)
    {
        if (propertyValueFilters == null)
            return data;
        
        var filters = propertyValueFilters
            .Where(f => f.ValueFilter?.IsActive ?? false);
        var filteredData = data;
        foreach (var filter in filters)
        {
            if (filter.ValueFilter.LooksForNullValues)
                filteredData = filteredData.Where(d => filter.DataGetter(d) == null);

            if (!string.IsNullOrWhiteSpace(filter.ValueFilter.Value))
            {
                if (filter.ValueFilter.LooksForNullValues)
                {
                    filteredData = filteredData.Where(d => filter.DataGetter(d) == null);
                    continue;
                }

                if (filter.ValueFilter.Begin && filter.ValueFilter.End)
                {
                    filteredData = filteredData
                        .Where(d =>
                        {
                            var val = filter.DataGetter(d) as string;
                            return val != null && val.Equals(filter.ValueFilter.Value, StringComparison.OrdinalIgnoreCase);
                        });
                } else if (filter.ValueFilter.Begin)
                {
                    filteredData = filteredData
                        .Where(d =>
                        {
                            var val = filter.DataGetter(d) as string;
                            return val != null && val.StartsWith(filter.ValueFilter.Value, StringComparison.OrdinalIgnoreCase);
                        });
                }
                else if (filter.ValueFilter.End)
                {
                    filteredData = filteredData
                        .Where(d =>
                        {
                            var val = filter.DataGetter(d) as string;
                            return val != null && val.EndsWith(filter.ValueFilter.Value, StringComparison.OrdinalIgnoreCase);
                        });
                }
                else
                {
                    filteredData = filteredData
                        .Where(d =>
                        {
                            var val = filter.DataGetter(d) as string;
                            return val != null && val.IndexOf(filter.ValueFilter.Value, StringComparison.OrdinalIgnoreCase) >= 0;
                        });
                }
            }
        }

        return filteredData;
    }
    
    private IEnumerable<TData> ApplySelectFilers(
        IEnumerable<TData> data,
        IEnumerable<PropertyValueFilter<TData, SelectFilter>>? propertyValueFilters)
    {
        if (propertyValueFilters == null)
            return data;
        
        var filters = propertyValueFilters
            .Where(f => f.ValueFilter?.IsActive ?? false);
        var filteredData = data;
        foreach (var filter in filters)
        {
            filteredData = filteredData
                .Where(d =>
                {
                    var val = filter.DataGetter(d);
                    return val != null && filter.ValueFilter.Values.Contains((int)val);
                });
        }

        return filteredData;
    }

    private bool Compare(DateTime? val, DateTime? filterValue, ComparisonType comparisonType)
    {
        if (!val.HasValue || !filterValue.HasValue)
            return false;

        return comparisonType switch
        {
            ComparisonType.Greater => val.Value > filterValue.Value,
            ComparisonType.GreaterEqual => val.Value >= filterValue.Value,
            ComparisonType.Lesser => val.Value < filterValue.Value,
            ComparisonType.LesserEqual => val.Value <= filterValue.Value,
            ComparisonType.Equal => val.Value == filterValue.Value,
            _ => throw new ArgumentOutOfRangeException(nameof(comparisonType), comparisonType, null)
        };
    }
    
    private IEnumerable<TData> ApplyDateTimeFilters(
        IEnumerable<TData> data,
        IEnumerable<PropertyValueFilter<TData, DateTimeFilter>>? propertyValueFilters)
    {
        if (propertyValueFilters == null)
            return data;
        
        var filters = propertyValueFilters
            .Where(f => f.ValueFilter?.IsActive ?? false);
        var filteredData = data;
        foreach (var filter in filters)
        {
            if (filter.ValueFilter.LooksForNullValues)
            {
                filteredData = filteredData.Where(d => filter.DataGetter(d) == null);
                continue;
            }
            
            if (filter.ValueFilter.Value1.HasValue && filter.ValueFilter.Comparison1.HasValue)
                filteredData = filteredData
                    .Where(d => Compare(
                        filter.DataGetter(d) as DateTime?, 
                        filter.ValueFilter.Value1.Value, 
                        filter.ValueFilter.Comparison1.Value));
            
            if (filter.ValueFilter.Value2.HasValue && filter.ValueFilter.Comparison2.HasValue)
                filteredData = filteredData
                    .Where(d => Compare(
                        filter.DataGetter(d) as DateTime?, 
                        filter.ValueFilter.Value2.Value, 
                        filter.ValueFilter.Comparison2.Value));
        }

        return filteredData;
    }

    /// <inheritdoc />
    public DataResult<TData> GetData(
        PropertyValueFilters<TData> propertyValueFilters, 
        OrderingWithProperty<TData> ordering, 
        List<object> searchAfter)
    {
        var filteredData = ApplySelectFilers(_data, propertyValueFilters.SelectFilters);
        filteredData = ApplyDateTimeFilters(filteredData, propertyValueFilters.DateTimeFilters);
        filteredData = ApplyNumberFilers(filteredData, propertyValueFilters.NumberFilters);
        filteredData = ApplyStringFilers(filteredData, propertyValueFilters.StringFilters);

        var sortedData = SortData(filteredData, ordering);

        return new DataResult<TData>
        {
            Data = sortedData.Take(_batchSize).ToList(),
            NumberOfResults = sortedData.Count(),
        };
    }

    /// <inheritdoc />
    public Task<DataResult<TData>> GetDataAsync(
        PropertyValueFilters<TData> propertyValueFilters, 
        OrderingWithProperty<TData> ordering, 
        List<object> searchAfter)
    {
        return Task.FromResult(GetData(propertyValueFilters, ordering, searchAfter));
    }

    /// <inheritdoc />
    public List<TData> GetDataById(IEnumerable<TData> ids, OrderingWithProperty<TData> ordering)
    {
        var idFilter = _idFilterFactory(ids);
        var data = _data.Where(d => idFilter(d));
        return SortData(data, ordering);
    }

    /// <inheritdoc />
    public Task<List<TData>> GetDataByIdAsync(IEnumerable<TData> ids, OrderingWithProperty<TData> ordering)
    {
        return Task.FromResult(GetDataById(ids, ordering));
    }
}