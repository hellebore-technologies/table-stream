﻿namespace HelleboreTech.TableStream.Core.Exceptions
{
    public class InvalidPropertyException : TableStreamFilterException
    {
        public InvalidPropertyException(string propertyName) : base(
            $"Invalid property {propertyName}. In {nameof(FilteredItem)} derived classes "
            + $"properties must either have a {nameof(AdditionalFieldAttribute)} or be referenced"
            + " as a filter property in the associated filter class.")
        {
        }
    }
}
