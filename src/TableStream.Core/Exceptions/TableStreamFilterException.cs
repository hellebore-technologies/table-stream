﻿using System;

namespace HelleboreTech.TableStream.Core.Exceptions
{
    public abstract class TableStreamFilterException : Exception
    {
        public TableStreamFilterException()
        {
        }

        public TableStreamFilterException(string msg) : base(msg)
        {
        }

        public TableStreamFilterException(string msg, Exception innerException)
            : base(msg, innerException)
        {
        }
    }
}
