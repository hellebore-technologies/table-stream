﻿using System.Collections.Generic;

namespace HelleboreTech.TableStream.Core.Exceptions
{
    public class SortAliasNotFoundException : TableStreamFilterException
    {
        public SortAliasNotFoundException(string fieldName, Dictionary<string, string> aliases) : base(
            $"Couldn't find alias for field {fieldName}.")
        {
            Aliases = aliases;
        }

        public Dictionary<string, string> Aliases { get; }
    }
}
