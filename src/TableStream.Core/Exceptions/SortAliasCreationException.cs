﻿using System;

namespace HelleboreTech.TableStream.Core.Exceptions
{
    public class SortAliasCreationException : TableStreamFilterException
    {
        public SortAliasCreationException(string aliasTarget, Type dataType) : base(
            $"Couldn't find property {aliasTarget} in {dataType}.")
        {
        }
    }
}
