﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace HelleboreTech.TableStream.Core
{
    /// <summary>
    /// Container for all the information related to the filter and data object.
    /// </summary>
    /// <typeparam name="TData">Type of data stored.</typeparam>
    public class PropertyValueFilters<TData> where TData : FilteredItem
    {
        private static readonly HashSet<Type> _allowedFilterType = new()
        {
            typeof(StringFilter),
            typeof(NumberFilter),
            typeof(SelectFilter),
            typeof(DateTimeFilter),
        };

        /// <summary>
        /// List of property information for the StringFilters.
        /// </summary>
        public List<PropertyValueFilter<TData, StringFilter>> StringFilters { get; set; }
        /// <summary>
        /// List of property information for the NumberFilters.
        /// </summary>
        public List<PropertyValueFilter<TData, NumberFilter>> NumberFilters { get; set; }
        /// <summary>
        /// List of property information for the SelectFilters.
        /// </summary>
        public List<PropertyValueFilter<TData, SelectFilter>> SelectFilters { get; set; }
        /// <summary>
        /// List of property information for the DateTimeFilters.
        /// </summary>
        public List<PropertyValueFilter<TData, DateTimeFilter>> DateTimeFilters { get; set; }

        internal static Dictionary<Type, List<PropertyInfo>> GetPropsByType<TFilter>()
        {
            return typeof(TFilter)
                .GetProperties()
                .Where(p => _allowedFilterType.Contains(p.PropertyType))
                .GroupBy(o => o.PropertyType)
                .ToDictionary(g => g.Key, g => g.ToList());
        }
    }

    /// <summary>
    /// Container for information related to a property of the data object.
    /// </summary>
    /// <typeparam name="TData">Type of data stored.</typeparam>
    /// <typeparam name="TFilter">Type of request.</typeparam>
    public class PropertyValueFilter<TData, TFilter> where TData : FilteredItem
    {
        /// <summary>
        /// Name of the data object's property.
        /// </summary>
        public string DataPropertyName { get; set; }
        /// <summary>
        /// Accessor to the property's data.
        /// </summary>
        public Func<TData, object> DataGetter { get; set; }
        /// <summary>
        /// Filter associated with the property.
        /// </summary>
        public TFilter ValueFilter { get; set; }
    }

    /// <summary>
    /// Ordering object with data property getter.
    /// </summary>
    /// <typeparam name="TData">Type of data stored.</typeparam>
    public class OrderingWithProperty<TData> where TData : FilteredItem
    {
        /// <summary>
        /// Name of the data object's property.
        /// </summary>
        public string DataPropertyName { get; set; }
        /// <summary>
        /// Accessor to the property's data.
        /// </summary>
        public Func<TData, object> DataGetter { get; set; }
        /// <summary>
        /// Sort order.
        /// </summary>
        public Order Order { get; set; }
    }
}
