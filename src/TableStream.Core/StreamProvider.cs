﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace HelleboreTech.TableStream.Core
{
    /// <summary>
    /// Provides functions to insert data into the storage.
    /// </summary>
    /// <typeparam name="TData">Type of data stored.</typeparam>
    public interface IStreamProvider<in TData> where TData : FilteredItem
    {
        /// <summary>
        /// Insert a document into the current index.
        /// </summary>
        /// <param name="item">Document to insert.</param>
        bool Insert(TData item);
        /// <summary>
        /// Insert a document into the current index asynchronously.
        /// </summary>
        /// <param name="item">Document to insert.</param>
        Task<bool> InsertAsync(TData item);
        /// <summary>
        /// Insert several documents into the current index.
        /// </summary>
        /// <param name="items">Documents to insert.</param>
        /// <returns>Number of documents inserted.</returns>
        int Insert(IEnumerable<TData> items);
        /// <summary>
        /// Insert several documents into the current index asynchronously.
        /// </summary>
        /// <param name="items">Documents to insert.</param>
        /// <returns>Number of documents inserted.</returns>
        Task<int> InsertAsync(IEnumerable<TData> items);
        /// <summary>
        /// Update a document.
        /// </summary>
        /// <param name="id">Id of the document.</param>
        /// <param name="item">Object containing the fields to update. Must be an anonymous object.</param>
        bool Update(string id, object item);
        /// <summary>
        /// Update a document asynchronously.
        /// </summary>
        /// <param name="id">Id of the document.</param>
        /// <param name="item">Object containing the fields to update. Must be an anonymous object.</param>
        Task<bool> UpdateAsync(string id, object item);
    }
}
