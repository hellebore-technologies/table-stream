﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HelleboreTech.TableStream.Core
{
    /// <summary>
    /// Result returned by a query.
    /// </summary>
    /// <typeparam name="TData">Type of data stored.</typeparam>
    public class DataResult<TData>
    {
        /// <summary>
        /// Set of data found matching the query.
        /// </summary>
        public List<TData> Data { get; set; }
        /// <summary>
        /// Number of data matching the query in the storage.
        /// </summary>
        public long NumberOfResults { get; set; }
        /// <summary>
        /// Objects used by the pagination to find the next set of results.
        /// </summary>
        public List<object> NextSearchAfter { get; set; }
    }

    /// <summary>
    /// Provides concrete implementation of functions used to query data.
    /// </summary>
    /// <typeparam name="TData">Type of data stored.</typeparam>
    public interface IFilteredItemProvider<TData> where TData : FilteredItem
    {
        /// <summary>
        /// Execute the filter query and return the data found.
        /// </summary>
        /// <param name="propertyValueFilters">Filter's value for each property.</param>
        /// <param name="ordering">Ordering parameters.</param>
        /// <param name="searchAfter">Parameters used for pagination.</param>
        /// <returns>Result object containing the list of data found and some metadata.</returns>
        DataResult<TData> GetData(
            PropertyValueFilters<TData> propertyValueFilters,
            OrderingWithProperty<TData> ordering,
            List<object> searchAfter);

        /// <summary>
        /// Execute the filter query asynchronously and return the data found.
        /// </summary>
        /// <param name="propertyValueFilters">Filter's value for each property.</param>
        /// <param name="ordering">Ordering parameters.</param>
        /// <param name="searchAfter">Parameters used for pagination.</param>
        /// <returns>Result object containing the list of data found and some metadata.</returns>
        Task<DataResult<TData>> GetDataAsync(
            PropertyValueFilters<TData> propertyValueFilters,
            OrderingWithProperty<TData> ordering,
            List<object> searchAfter);

        /// <summary>
        /// Get the data having the same ids as those given in parameters and return them in sorted list.
        /// </summary>
        /// <param name="ids">Collection of objects from which the id field will be read.</param>
        /// <param name="ordering">Ordering of the results.</param>
        /// <returns>Sorted list of data matching the ids given.</returns>
        List<TData> GetDataById(IEnumerable<TData> ids, OrderingWithProperty<TData> ordering);

        /// <summary>
        /// Get the data having the same ids as those given in parameters asynchronously and
        /// return them in sorted list.
        /// </summary>
        /// <param name="ids">Collection of objects from which the id field will be read.</param>
        /// <param name="ordering">Ordering of the results.</param>
        /// <returns>Sorted list of data matching the ids given.</returns>
        Task<List<TData>> GetDataByIdAsync(IEnumerable<TData> ids, OrderingWithProperty<TData> ordering);
    }

    /// <summary>
    /// Provides functions to query data using filters.
    /// </summary>
    /// <typeparam name="TData">Type of data stored.</typeparam>
    /// <typeparam name="TFilter">Type of request.</typeparam>
    public class FilteredItemProvider<TData, TFilter> where TData : FilteredItem
    {
        private readonly IFilteredItemProvider<TData> _dataProvider;
        private readonly FilterHelper<TData, TFilter> _filterHelper;

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        /// <param name="dataProvider">Concrete implementation of the provider.</param>
        public FilteredItemProvider(IFilteredItemProvider<TData> dataProvider)
        {
            _dataProvider = dataProvider;
            _filterHelper = new FilterHelper<TData, TFilter>();
        }

        /// <summary>
        /// Execute the filter query and return the data found.
        /// </summary>
        /// <param name="filter">Filter query.</param>
        /// <returns>Result object containing the list of data found and some metadata.</returns>
        public DataResult<TData> GetData(Filter<TFilter> filter)
        {
            var orderWithProp = _filterHelper.GetOrderingProperty(filter.Ordering);
            var propertyValueFilters = _filterHelper.GetFilters(filter.ValueFilters);

            var data = _dataProvider.GetData(
                propertyValueFilters,
                orderWithProp,
                filter.SearchAfter);

            return data;
        }

        /// <summary>
        /// Execute the filter query asynchronously and return the data found.
        /// </summary>
        /// <param name="filter">Filter query.</param>
        /// <returns>Result object containing the list of data found and some metadata.</returns>
        public async Task<DataResult<TData>> GetDataAsync(Filter<TFilter> filter)
        {
            var orderWithProp = _filterHelper.GetOrderingProperty(filter.Ordering);
            var propertyValueFilters = _filterHelper.GetFilters(filter.ValueFilters);

            var data = await _dataProvider.GetDataAsync(
                propertyValueFilters,
                orderWithProp,
                filter.SearchAfter);

            return data;
        }

        /// <summary>
        /// Get the data having the same ids as those given in parameters and return them in sorted list.
        /// </summary>
        /// <param name="ids">Collection of objects from which the id field will be read.</param>
        /// <param name="ordering">Ordering of the results.</param>
        /// <returns>Sorted list of data matching the ids given.</returns>
        public List<TData> GetDataByIds(IEnumerable<TData> ids, Ordering ordering)
        {
            var orderWithProp = _filterHelper.GetOrderingProperty(ordering);

            var data = _dataProvider.GetDataById(ids, orderWithProp);

            return data;
        }

        /// <summary>
        /// Get the data having the same ids as those given in parameters asynchronously and
        /// return them in sorted list.
        /// </summary>
        /// <param name="ids">Collection of objects from which the id field will be read.</param>
        /// <param name="ordering">Ordering of the results.</param>
        /// <returns>Sorted list of data matching the ids given.</returns>
        public async Task<List<TData>> GetDataByIdsAsync(IEnumerable<TData> ids, Ordering ordering)
        {
            var orderWithProp =_filterHelper.GetOrderingProperty(ordering);

            var data = await _dataProvider.GetDataByIdAsync(ids, orderWithProp);

            return data;
        }
    }
}
