﻿using System.Collections.Generic;

namespace HelleboreTech.TableStream.Core
{
    public enum Order
    {
        Ascending = 1,
        Descending = 2,
    }

    /// <summary>
    /// Define the order by which the table is sorted.
    /// </summary>
    public class Ordering
    {
        /// <summary>
        /// Column's id.
        /// </summary>
        public string FieldName { get; set; }
        /// <summary>
        /// Sort order.
        /// </summary>
        public Order Order { get; set; }
    }

    /// <summary>
    /// Define the filters and order that are applied to the table's data.
    /// </summary>
    /// <typeparam name="TValueFilters">Class containing the filters for each column.</typeparam>
    public class Filter<TValueFilters>
    {
        /// <summary>
        /// List of data ids used by search engine to retrieve next page.
        /// </summary>
        public List<object> SearchAfter { get; set; }
        /// <summary>
        /// Ordering of the data.
        /// </summary>
        public Ordering Ordering { get; set; }
        /// <summary>
        /// Filters
        /// </summary>
        public TValueFilters ValueFilters { get; set; }
    }
}
