﻿using System;
using System.Collections.Generic;

namespace HelleboreTech.TableStream.Core
{
    /// <summary>
    /// A class that provides functions to manage the "table" used to store the data.
    /// Here the "table" refers to the data structure used by the data provider to store
    /// the data in such a way that they can be easily and quickly queried.
    /// </summary>
    /// <typeparam name="TData">Type of data stored.</typeparam>
    /// <typeparam name="TFilter">Type of request.</typeparam>
    public abstract class DatabaseHandler<TData, TFilter> where TData : FilteredItem
    {
        protected readonly Tuple<List<ValueFilterProp>, List<AdditionalFieldProp>> _properties;

        protected DatabaseHandler()
        {
            _properties = FilterHelper<TData, TFilter>.GetProperties();
        }

        /// <summary>
        /// Creates the "table" used to store the data.
        /// </summary>
        public abstract void CreateTable();
        /// <summary>
        /// Removes the "table" used to store the data along with the data.
        /// </summary>
        public abstract void DeleteTable();
        /// <summary>
        /// Removes all the data contained in the "table".
        /// </summary>
        public abstract void TruncateTable();
    }
}
