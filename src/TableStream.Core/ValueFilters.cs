﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HelleboreTech.TableStream.Core
{
    public enum ComparisonType
    {
        Greater,
        GreaterEqual,
        Lesser,
        LesserEqual,
        Equal,
    }

    /// <summary>
    /// Filter for string values.
    /// </summary>
    public class StringFilter : IEquatable<StringFilter>
    {
        /// <summary>
        /// Whether the filter should be applied.
        /// </summary>
        public bool IsActive { get; set; }
        /// <summary>
        /// Whether to display only null values.
        /// </summary>
        public bool LooksForNullValues { get; set; }
        /// <summary>
        /// String to search for.
        /// </summary>
        public string Value { get; set; }
        /// <summary>
        /// Whether to anchor search at the beginning.
        /// </summary>
        public bool Begin { get; set; }
        /// <summary>
        /// Whether to anchor search at the end.
        /// </summary>
        public bool End { get; set; }

        /// <inheritdoc />
        public bool Equals(StringFilter other)
        {
            return other != null
                   && Value == other.Value
                   && IsActive == other.IsActive
                   && LooksForNullValues == other.LooksForNullValues
                   && Begin == other.Begin
                   && End == other.End;
        }
    }

    /// <summary>
    /// Filter for number values.
    /// </summary>
    public class NumberFilter : IEquatable<NumberFilter>
    {
        /// <summary>
        /// Whether the filter should be applied.
        /// </summary>
        public bool IsActive { get; set; }
        /// <summary>
        /// Whether to display only null values.
        /// </summary>
        public bool LooksForNullValues { get; set; }
        /// <summary>
        /// First value.
        /// </summary>
        public double? Value1 { get; set; }
        /// <summary>
        /// First comparison operation.
        /// </summary>
        public ComparisonType? Comparison1 { get; set; }
        /// <summary>
        /// Second value.
        /// </summary>
        public double? Value2 { get; set; }
        /// <summary>
        /// Second comparison operation.
        /// </summary>
        public ComparisonType? Comparison2 { get; set; }

        /// <inheritdoc />
        public bool Equals(NumberFilter other)
        {
            return other != null
                   && IsActive == other.IsActive
                   && LooksForNullValues == other.LooksForNullValues
                   && Value1 == other.Value1
                   && Comparison1 == other.Comparison1
                   && Value2 == other.Value2
                   && Comparison2 == other.Comparison2;
        }
    }

    /// <summary>
    /// Filter for enum values.
    /// </summary>
    public class SelectFilter : IEquatable<SelectFilter>
    {
        /// <summary>
        /// Whether the filter should be applied.
        /// </summary>
        public bool IsActive { get; set; }
        /// <summary>
        /// Selected values.
        /// </summary>
        public List<int> Values { get; set; }

        /// <inheritdoc />
        public bool Equals(SelectFilter other)
        {
            if (other == null
                || IsActive != other.IsActive
                || Values.Count != other.Values.Count)
                return false;

            return Values.All(other.Values.Contains);
        }
    }

    /// <summary>
    /// Filter for DateTime values.
    /// </summary>
    public class DateTimeFilter : IEquatable<DateTimeFilter>
    {
        /// <summary>
        /// Whether the filter should be applied.
        /// </summary>
        public bool IsActive { get; set; }
        /// <summary>
        /// Whether to display only null values.
        /// </summary>
        public bool LooksForNullValues { get; set; }
        /// <summary>
        /// First value.
        /// </summary>
        public DateTime? Value1 { get; set; }
        /// <summary>
        /// First comparison operation.
        /// </summary>
        public ComparisonType? Comparison1 { get; set; }
        /// <summary>
        /// Second value.
        /// </summary>
        public DateTime? Value2 { get; set; }
        /// <summary>
        /// Second comparison operation.
        /// </summary>
        public ComparisonType? Comparison2 { get; set; }

        /// <inheritdoc />
        public bool Equals(DateTimeFilter other)
        {
            return other != null
                   && IsActive == other.IsActive
                   && LooksForNullValues == other.LooksForNullValues
                   && Value1 == other.Value1
                   && Comparison1 == other.Comparison1
                   && Value2 == other.Value2
                   && Comparison2 == other.Comparison2;
        }
    }
}
