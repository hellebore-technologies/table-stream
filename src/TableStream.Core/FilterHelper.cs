﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using HelleboreTech.TableStream.Core.Exceptions;

namespace HelleboreTech.TableStream.Core
{
    /// <summary>
    /// Represents a searchable property of the data object.
    /// </summary>
    public class ValueFilterProp
    {
        /// <summary>
        /// Name of the data's property.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Type of the data's property.
        /// </summary>
        public Type Type { get; set; }
    }

    /// <summary>
    /// Represents a non-searchable property of the data object.
    /// </summary>
    public class AdditionalFieldProp
    {
        /// <summary>
        /// Name of the data's property.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Type of the data's property.
        /// </summary>
        public AdditionalFieldType Type { get; set; }
    }

    internal class FilterHelper<TData, TFilter> where TData : FilteredItem
    {
        private readonly List<ValueFilterGetter<DateTimeFilter>> _dateTimeFilterGetters = new();
        private readonly List<ValueFilterGetter<NumberFilter>> _numberFilterGetters = new();
        private readonly HashSet<string> _propertyNames = new();
        private readonly List<ValueFilterGetter<SelectFilter>> _selectFilterGetters = new();
        private readonly Dictionary<string, string> _sortAlias = new(StringComparer.InvariantCultureIgnoreCase);
        private readonly Dictionary<string, Func<TData, object>> _dataGetters;

        private readonly List<ValueFilterGetter<StringFilter>> _stringFilterGetters = new();

        public FilterHelper()
        {
            _dataGetters = typeof(TData)
                .GetProperties()
                .ToDictionary(
                    p => p.Name, 
                    p => (Func<TData, object>)p.GetValue);
            
            var propsByType = PropertyValueFilters<TData>.GetPropsByType<TFilter>();
            CreateGetters(propsByType, _stringFilterGetters);
            CreateGetters(propsByType, _numberFilterGetters);
            CreateGetters(propsByType, _selectFilterGetters);
            CreateGetters(propsByType, _dateTimeFilterGetters);
            LoadSortAlias();
            // call GetProperties to ensure that TData & TFilter are consistent
            GetProperties();
        }

        private void LoadSortAlias()
        {
            var attributeAlias = new Dictionary<string, string>();
            foreach (var propertyInfo in typeof(TData).GetProperties())
            {
                if (_propertyNames.Contains(propertyInfo.Name))
                    _sortAlias.Add(propertyInfo.Name, propertyInfo.Name);

                var sortFieldForAttribute = (SortFieldFor)propertyInfo
                    .GetCustomAttributes(
                        typeof(SortFieldFor),
                        false)
                    .SingleOrDefault();
                if (sortFieldForAttribute != null)
                    attributeAlias.Add(propertyInfo.Name, sortFieldForAttribute.PropertyName);
            }

            foreach (var attributeAliasValue in attributeAlias)
                if (_sortAlias.ContainsKey(attributeAliasValue.Value))
                    _sortAlias[attributeAliasValue.Value] = attributeAliasValue.Key;
                else
                    throw new SortAliasCreationException(attributeAliasValue.Value, typeof(TData));
        }

        private void CreateGetters<TFilterType>(
            Dictionary<Type, List<PropertyInfo>> propsByType,
            List<ValueFilterGetter<TFilterType>> getters)
        {
            if (propsByType.TryGetValue(typeof(TFilterType), out var filterProps))
            {
                getters.AddRange(
                    filterProps.Select(
                        filterProp =>
                            new ValueFilterGetter<TFilterType>
                            {
                                DataPropertyName = filterProp.Name,
                                Getter = (Func<TFilter, TFilterType>)Delegate.CreateDelegate(
                                    typeof(Func<TFilter, TFilterType>),
                                    filterProp.GetGetMethod()),
                            }
                    ));
                foreach (var valueFilterGetter in getters)
                    _propertyNames.Add(valueFilterGetter.DataPropertyName);
            }
        }

        public PropertyValueFilters<TData> GetFilters(TFilter filter)
        {
            return new PropertyValueFilters<TData>
            {
                StringFilters = _stringFilterGetters
                    .Select(
                        fg => new PropertyValueFilter<TData, StringFilter>
                        {
                            DataPropertyName = fg.DataPropertyName,
                            ValueFilter = fg.Getter(filter),
                            DataGetter = _dataGetters[fg.DataPropertyName],
                        })
                    .ToList(),
                NumberFilters = _numberFilterGetters
                    .Select(
                        fg => new PropertyValueFilter<TData, NumberFilter>
                        {
                            DataPropertyName = fg.DataPropertyName,
                            ValueFilter = fg.Getter(filter),
                            DataGetter = _dataGetters[fg.DataPropertyName],
                        })
                    .ToList(),
                SelectFilters = _selectFilterGetters
                    .Select(
                        fg => new PropertyValueFilter<TData, SelectFilter>
                        {
                            DataPropertyName = fg.DataPropertyName,
                            ValueFilter = fg.Getter(filter),
                            DataGetter = _dataGetters[fg.DataPropertyName],
                        })
                    .ToList(),
                DateTimeFilters = _dateTimeFilterGetters
                    .Select(
                        fg => new PropertyValueFilter<TData, DateTimeFilter>
                        {
                            DataPropertyName = fg.DataPropertyName,
                            ValueFilter = fg.Getter(filter),
                            DataGetter = _dataGetters[fg.DataPropertyName],
                        })
                    .ToList(),
            };
        }

        public OrderingWithProperty<TData> GetOrderingProperty(Ordering ordering)
        {
            if (ordering == null)
                return null;

            if (!_sortAlias.TryGetValue(ordering.FieldName, out var alias))
                throw new SortAliasNotFoundException(ordering.FieldName, _sortAlias);

            return new OrderingWithProperty<TData>
            {
                DataPropertyName = alias,
                DataGetter = _dataGetters[alias],
                Order = ordering.Order,
            };
        }

        public static Tuple<List<ValueFilterProp>, List<AdditionalFieldProp>> GetProperties()
        {
            var valueFilterPropsByType = PropertyValueFilters<TData>.GetPropsByType<TFilter>();
            var valueFilterPropsName = new HashSet<string>(
                valueFilterPropsByType.Values
                    .SelectMany(pl => pl.Select(p => p.Name)));

            var additionalProps = new List<AdditionalFieldProp>();
            foreach (var prop in typeof(TData)
                .GetProperties()
                .Where(p => !valueFilterPropsName.Contains(p.Name)))
            {
                var attribute = (AdditionalFieldAttribute)prop
                    .GetCustomAttributes(typeof(AdditionalFieldAttribute), false)
                    .SingleOrDefault();
                if (attribute == null)
                    throw new InvalidPropertyException(prop.Name);

                additionalProps.Add(new AdditionalFieldProp { Name = prop.Name, Type = attribute.Type });
            }

            var valueFilterProps = valueFilterPropsByType.Values
                .SelectMany(l => l)
                .Select(p => new ValueFilterProp { Name = p.Name, Type = p.PropertyType })
                .ToList();

            return Tuple.Create(valueFilterProps, additionalProps);
        }

        private class ValueFilterGetter<TFilterType>
        {
            public string DataPropertyName { get; set; }
            public Func<TFilter, TFilterType> Getter { get; set; }
        }
    }
}
