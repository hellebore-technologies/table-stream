﻿using System;

namespace HelleboreTech.TableStream.Core
{
    public enum AdditionalFieldType
    {
        Guid = 1,
        SmallInt,
        Int,
        Long,
        Float,
        Double,
        String,
        Date,
    }

    /// <summary>
    /// Indicates that the property should be stored but not searchable.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class AdditionalFieldAttribute : Attribute
    {
        /// <summary>
        /// Initialises the attribute.
        /// </summary>
        /// <param name="type">Type of the property</param>
        public AdditionalFieldAttribute(AdditionalFieldType type)
        {
            Type = type;
        }

        /// <summary>
        /// Type of the property.
        /// </summary>
        public AdditionalFieldType Type { get; }
    }

    /// <summary>
    /// Indicates that the property's value should be used instead of
    /// of the referenced property for sort.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class SortFieldFor : Attribute
    {
        /// <summary>
        /// Initialises the attribute.
        /// </summary>
        /// <param name="propertyName">Name of the property that should be replace by this one when sorting.</param>
        public SortFieldFor(string propertyName)
        {
            PropertyName = propertyName;
        }

        /// <summary>
        /// Name of the property to replace.
        /// </summary>
        public string PropertyName { get; }
    }

    /// <summary>
    /// A base class for searchable items.
    /// </summary>
    public abstract class FilteredItem
    {
        /// <summary>
        /// A random number used for pagination. Should be unique to avoid missing or duplicate result.
        /// </summary>
        [AdditionalField(AdditionalFieldType.Long)]
        public long TieBreakerId { get; set; }
    }
}
