# Table Stream
The Table Stream project provides a complete solution for building tables capable of handling
millions of rows with powerful filtering and sorting capabilities. It's composed of a TypeScript
package providing React components to build and display tables, and of several C# libraries to
enable server side filtering and sorting.

The key features of the project are:
- Declarative configuration
- Several column types:
  - string
  - number
  - datetime
  - enumeration
  - other
- Advanced filters, specific to each column types, with support for comparison operators
- Sorting by a given column
- Support for infinite loading
- Support for server side filtering and sorting using Elasticsearch
- Fully themeable / customizable

The React components library uses [react-window](https://github.com/bvaughn/react-window) to
efficiently display table with hundred of thousands of rows.


## Documentation
You can find the complete documentation [here](https://gitlab.com/hellebore-technologies/table-stream/-/blob/master/docs/ReadMe.md).

You can find the documentation on the various filters (syntax and behavior)
[here](https://gitlab.com/hellebore-technologies/table-stream/-/blob/master/docs/Filters.md).


## ChangeLog
Changes are tracked in the [ChangeLog](https://gitlab.com/hellebore-technologies/table-stream/-/blob/master/ChangeLog.md).


## License
Table Stream is available under the [MIT License](https://gitlab.com/hellebore-technologies/table-stream/-/blob/master/LICENSE).
