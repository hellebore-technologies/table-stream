# ChangeLog

## 0.2.0 - 2023-03-21
### Added
**C#: InMemory**
- Pack ReadMe
- Get data by ids for in memory data
- Filter for in memory data
- Sort for in memory data

### Changed
**C#: core**
- Pack ReadMe
- Properly set `DataGetter` property in `PropertyValueFilter` object
- Add `OrderingWithProperty` object to pass property getter for ordering to provider

**C#: Elasticsearch**
- Pack ReadMe
- Update FilteredItemProvider to use the new `OrderingWithProperty` object


## 0.1.0 - 2021-09-20
First public version.

### Added
**Ts: core**
- Main components with customization
- Filter for in memory data
- Sort for in memory data
- Hooks for Infinite Loading with and without refresh

**C#: core**
- Data models
- Interfaces for data providers

**C#: Elasticsearch**
- Implementation of data providers using Nest
- Support for rolling and static index
