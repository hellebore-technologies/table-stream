# Sorting

Sorting is only available on columns with this symbol in the upper right corner of the header.

You can change the sort order by double clicking on the column header. The order is  "Ascending",
"Descending". ``null`` values are always last.

You can only sort by one column at a time.
