# Filter

This document provide the complete information on how each type of filter works (syntax and
behavior).

Some notions are shared by several types of filter. These notions are:
- [Ranges](#ranges)
- [Null Selection](#null-selection)

The different types of filter available are:
- [String](#string)
- [Number](#number)
- [Datetime](#datetime)
- [Select](#select)


## Common Notion
### Ranges
In some filters, you can use ``>``, ``>=``, ``<``, ``<=``, and ``=`` to define a comparison. For
example, ``>= 1`` means all values above or equal to 1.

To define a range, you can combine two clauses with ``&``. For example, ``[1, 4[`` could be written
``>= 1 & <3``.

*Note*:
- Spaces are ignored between symbols and values. This means that ``>= 1`` and ``>=1`` are
  equivalent, but ``> = 1`` won``t work.
- No symbol is equivalent to ``=``


### Null Selection
In some filters, you can use ``//`` to search rows for which the value associated with the column is
null.


## Filters
### String
*Doesn't support ranges. Support null selection.*

Search is case insensitive and done in contain mode. This means that ``it`` would match ``it``,
``iteration``, ``written``, and ``bit``.

You can use ``^`` and ``$``, meaning respectively ``begin`` and ``end``, to anchor the search.

Here are some examples, assuming the possible values are ``it``, ``iteration``, ``written``, and
``bit``:
- ``^it`` would match ``it`` and ``iteration``
- ``it$`` would match ``it`` and ``bit``
- ``^it$`` would only match ``it``

You can escape the characters if needed using ``/``. The escaping only works for ``^`` at the
beginning and ``$`` at the end.

**Examples**:
- ``/^it`` will be read as ``^it`` without any anchoring
- ``it/$`` will be read as ``it$`` without any anchoring
- ``te/^st`` will be read as ``te/^st`` without any anchoring


### Number
*Support ranges and null selection.*

You should input numbers without any spaces inside them and using ``.`` as the decimal separator.

Number can also be negative.


### Datetime
*Support ranges and null selection.*

You can use iso 8601 datetime format. You can also use the following formats:
- for date
  - yyyy/mm/dd
  - yyyy/m/d
  - mm/dd
  - m/d
- for time
  - hh:mm:ss
  - h:m:s
  - hh:mm
  - h:m

With these formats date and time must be separated by a space.

When using incomplete dates (mm/dd or m/d formats) the year is autocompleted with the current year.

When using incomplete times (hh:mm or h:m formats) the seconds are set to 0.

If only a date is provided, the time is set to 00:00:00.

If only a time is provided, the date is set to the current date.

**Examples**: (assuming the current date is 2020-10-09 12:20:23)
- 2020-09-12T12:20:23 is left untouched as it``s a valid iso 8601 date
- 2020/09/12 is translated to 2020-09-12 00:00:00
- 3/1 is translated to 2020-03-01 00:00:00
- 12:3 is translated to 2020-10-09 12:03:00
- 5/09 15:23:26 is translated to 2020-05-09 15:23:26
- 2020/4/25 20:59 is translated to 2020-04-25 20:59:00


### Select
*Doesn't support ranges.*

Simply choose from the dropdown menu.

You can type in the box to search in the available options. You can use keyboard arrows to move
between options and select them by pressing the ``Enter`` key. You can close the menu by pressing
the ``Escape`` key.
